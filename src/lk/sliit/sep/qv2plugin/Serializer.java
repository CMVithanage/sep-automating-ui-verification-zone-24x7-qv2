package lk.sliit.sep.qv2plugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

@SuppressWarnings("restriction")
public class Serializer {

	public boolean validate() throws IOException, SAXException {

		boolean status = false;

		File file = new File("//Users//TwiZtor//Desktop//schema.xsd");

		Source xmlFile = new StreamSource(new File(
				"//Users//TwiZtor//Desktop//serialized1.xml"));

		SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(file);
		Validator validator = schema.newValidator();

		try {
			validator.validate(xmlFile);
			status = true;
		}

		catch (SAXException e) {
			status = false;
		}

		return status;

	}
	
	public File serialize(RuleElementSet ruleElementSet, String outputFilePath) throws FileNotFoundException{
		XStream xstream = new XStream();
		String xml = xstream.toXML(ruleElementSet);
		System.out.println(xml);
		
		File file = new File(outputFilePath);
		FileOutputStream fs = new FileOutputStream(file);
		xstream.toXML(ruleElementSet, fs);
		
		return file;
	}

//	public void serialize(List<RuleElement> elementList, String path) {
//
//		try {
//			File file = new File(path);
//			JAXBContext jaxbContext = JAXBContext
//					.newInstance(RuleElement.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//
//			// output pretty printed
//			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//			jaxbMarshaller.marshal(elementList, file);
//
//		} catch (JAXBException e) {
//			e.printStackTrace();
//		}
//
//	}

	public RuleElementSet deserialize() throws IOException, SAXException {

		if (validate()) {

			JFrame frame = new JFrame();

			String filename = File.separator + "tmp";
			JFileChooser fc = new JFileChooser(new File(filename));

			// Show open dialog; this method does not return until the dialog is
			// closed
			fc.showOpenDialog(frame);
			File selFile = fc.getSelectedFile();

			// Show save dialog; this method does not return until the dialog is
			// closed
			/*
			 * fc.showSaveDialog(frame); selFile = fc.getSelectedFile();
			 */

			// /////////////////

			try {

				// File file = new File(selFile);
				JAXBContext jaxbContext = JAXBContext
						.newInstance(RuleElementSet.class);

				Unmarshaller jaxbUnmarshaller = jaxbContext
						.createUnmarshaller();
				RuleElementSet r = (RuleElementSet) jaxbUnmarshaller
						.unmarshal(selFile);

				return r;

			} catch (JAXBException e) {
				e.printStackTrace();
				return null;
			}

		} else {
			System.out.println("XML file is NOT valid");
			return null;
		}

	}
	
	
	

}
