package lk.sliit.sep.qv2plugin.test;

import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import lk.sliit.sep.qv2plugin.RuleElement;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TwiZtor
 */
public class RuleMakerGUI extends javax.swing.JFrame {

	Vector<RuleElement> ruleElements;
	Vector <String> ruleElementTableHeader;
	Vector <String> ruleElementAttributesTableHeader;
	Vector <Vector<String>> ruleElementTableContent;
    Vector <Vector<String>> ruleElementAttributesTableContent;
	
    /**
     * Creates new form RuleMaker
     */
    public RuleMakerGUI() {
        initComponents();
        ruleElements = new Vector<RuleElement>();
        
        ruleElementTableContent = new Vector<Vector<String>>();
        ruleElementAttributesTableContent = new Vector<Vector<String>>();
        
        ruleElementTableHeader = new Vector<String>();
        ruleElementTableHeader.add("ID");
        ruleElementTableHeader.add("Rule Name");
        ruleElementTableHeader.add("Type");
        
        ruleElementAttributesTableHeader = new Vector<String>();
        ruleElementAttributesTableHeader.add("Attribute");
        ruleElementAttributesTableHeader.add("Value");
        
        initializeAttributeNameCombo();
    }
    
    private void loadTable(){
        ruleElementTable.removeAll();
        ruleElementTable.setModel(new javax.swing.table.DefaultTableModel(ruleElementTableContent, ruleElementTableHeader));
        jScrollPane1.setViewportView(ruleElementTable);
        
        ruleElementAttributesTable.removeAll();
        ruleElementAttributesTable.setModel(new javax.swing.table.DefaultTableModel(ruleElementAttributesTableContent, ruleElementAttributesTableHeader));
        jScrollPane2.setViewportView(ruleElementAttributesTable);
    }
    
    private void initializeAttributeNameCombo(){
    	attributeNameCombo.removeAllItems();
    	for(String attributeName : RuleElement.ATTRIBUTES_SORTED){
    		attributeNameCombo.addItem(attributeName);
    	}
    }
    
    private void initializeAttributeValueCombo(String attributeName){
    	attributeValueCombo.removeAllItems();
    	
    	if(attributeName.equalsIgnoreCase("BACKGROUNDATTACHMENT")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BACKGROUNDATTACHMENT_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BACKGROUNDCOLOR")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.COLORNAME_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BACKGROUNDPOSITION")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BACKGROUNDPOSITION_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BACKGROUNDREPEAT")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BACKGROUNDREPEAT_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BORDERCOLLAPSE")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BORDERCOLLAPSE_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BORDERCOLOR")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BORDERCOLOR_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BORDERSTYLE")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BORDERSTYLE_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BORDERWIDTH")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BORDERWIDTH_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("BOTTOM")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.BOTTOM_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("FONTFAMILY")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.FONTFAMILY_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("HEIGHT")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.HEIGHT_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	} else if(attributeName.equalsIgnoreCase("WIDTH")){
    		attributeValueCombo.addItem(" ");
    		for(String attributeValue:RuleElement.WIDTH_VALUES){
    			attributeValueCombo.addItem(attributeValue);
    		}
    	}
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ruleElementNameText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        ruleElementTypeText = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        ruleElementTable = new javax.swing.JTable();
        addRuleElementButton = new javax.swing.JButton();
        deleteRuleElementButton = new javax.swing.JButton();
        ruleElementIDText = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        ruleElementAttributesTable = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        attributeNameCombo = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        attributeValueCombo = new javax.swing.JComboBox();
        deleteAttributeButton = new javax.swing.JButton();
        addAttributeButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        continueButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel2.setText("RuleMaker");

        jLabel1.setText("Rule Name :");

        jLabel3.setText("Rule Type :");

        ruleElementTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "Rule Name", "Type"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        ruleElementTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ruleElementTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ruleElementTable);

        addRuleElementButton.setText("Add");
        addRuleElementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRuleElementButtonActionPerformed(evt);
            }
        });

        deleteRuleElementButton.setText("Delete");
        deleteRuleElementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRuleElementButtonActionPerformed(evt);
            }
        });

        jLabel6.setText("ID :");

        jLabel7.setText("Add Rule");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(addRuleElementButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteRuleElementButton)
                        .addGap(42, 42, 42))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ruleElementIDText, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ruleElementNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ruleElementTypeText, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 713, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(ruleElementNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(ruleElementTypeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(ruleElementIDText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteRuleElementButton)
                    .addComponent(addRuleElementButton))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        ruleElementAttributesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Attribute", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        ruleElementAttributesTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ruleElementAttributesTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(ruleElementAttributesTable);

        jLabel4.setText("Attribute :");

        attributeNameCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        attributeNameCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attributeNameComboActionPerformed(evt);
            }
        });

        jLabel5.setText("Value :");

        attributeValueCombo.setEditable(true);
        attributeValueCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        deleteAttributeButton.setText("Delete");
        deleteAttributeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteAttributeButtonActionPerformed(evt);
            }
        });

        addAttributeButton.setText("Add/Update");
        addAttributeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAttributeButtonActionPerformed(evt);
            }
        });

        jLabel8.setText("Add/Modify Attributes");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(attributeNameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(attributeValueCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addAttributeButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteAttributeButton))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8)))
                .addGap(34, 34, 34))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(111, 111, 111)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 538, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(attributeNameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(attributeValueCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteAttributeButton)
                    .addComponent(addAttributeButton))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        continueButton.setText("Continue");
        continueButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                continueButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(layout.createParallelGroup(Alignment.LEADING)
        					.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        					.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 753, Short.MAX_VALUE)
        					.addGroup(layout.createSequentialGroup()
        						.addGap(8)
        						.addComponent(jLabel2)
        						.addContainerGap(654, Short.MAX_VALUE)))
        				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
        					.addComponent(continueButton)
        					.addGap(15))))
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addGap(16)
        			.addComponent(jLabel2)
        			.addGap(18)
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(continueButton)
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().setLayout(layout);

        pack();
    }// </editor-fold>                        

    private void addAttributeButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                   
        // TODO add your handling code here:
    	String attrString = attributeNameCombo.getSelectedItem().toString();
    	int selectedRow = ruleElementTable.getSelectedRow();
    	RuleElement ruleElement = ruleElements.get(selectedRow);
    	if(attrString.equalsIgnoreCase(RuleElement.ATTRIBUTES_SORTED[1])){
    		ruleElement.setBackgroundAttachment(attributeValueCombo.getSelectedItem().toString());
    	} else if(attrString.equalsIgnoreCase(RuleElement.ATTRIBUTES_SORTED[2])){
    		ruleElement.setBackgroundColor(attributeValueCombo.getSelectedItem().toString());
    	} else if(attrString.equalsIgnoreCase(RuleElement.ATTRIBUTES_SORTED[3])){
    		ruleElement.setBackgroundImage(attributeValueCombo.getSelectedItem().toString());
    	} else if(attrString.equalsIgnoreCase(RuleElement.ATTRIBUTES_SORTED[4])){
    		ruleElement.setBackgroundPosition(attributeValueCombo.getSelectedItem().toString());
    	} else if(attrString.equalsIgnoreCase(RuleElement.ATTRIBUTES_SORTED[5])){
    		ruleElement.setBackgroundRepeat(attributeValueCombo.getSelectedItem().toString());
    	}
    	
    	Vector<String> elementAttribute = new Vector<String>();
    	elementAttribute.add(attrString);
    	elementAttribute.add(attributeValueCombo.getSelectedItem().toString());
    	ruleElementAttributesTableContent.add(elementAttribute);
    	
    	loadTable();
    }                                                  

    private void continueButtonActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
    	JFrame newFrame = new WebElementManagerGUI(ruleElements);
    	newFrame.setVisible(true);
    	this.dispose();
    }                                              

    private void addRuleElementButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                     
        // TODO add your handling code here:    	
    	int id = Integer.parseInt(ruleElementIDText.getText());
    	String nameString = ruleElementNameText.getText();
    	String typeString = ruleElementTypeText.getText();
    	
    	RuleElement ruleElement = new RuleElement(id, nameString, typeString);
    	ruleElements.add(ruleElement);
    	
    	Vector<String> element1 = new Vector<String>();
        element1.add(Integer.toString(id));
        element1.add(nameString);
        element1.add(typeString);
        ruleElementTableContent.add(element1);
        
        loadTable();
    }                                                    

    private void deleteRuleElementButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                        
        // TODO add your handling code here:
		if (ruleElementTable.getRowCount() > 0) {
			int selectedRow = ruleElementTable.getSelectedRow();
			ruleElementTableContent.remove(selectedRow);
			ruleElements.remove(selectedRow);
			loadTable();
		} else {
			JOptionPane.showMessageDialog(null, "Table is Empty",
					"Invalid Operation", JOptionPane.INFORMATION_MESSAGE);
		}
    }                                                       

    private void deleteAttributeButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                      
        // TODO add your handling code here:
    	if (ruleElementAttributesTable.getRowCount() > 0) {
			int selectedRow = ruleElementAttributesTable.getSelectedRow();
			ruleElementAttributesTableContent.remove(selectedRow);
			loadTable();
		} else {
			JOptionPane.showMessageDialog(null, "Table is Empty",
					"Invalid Operation", JOptionPane.INFORMATION_MESSAGE);
		}
    }                                                     

    private void attributeNameComboActionPerformed(java.awt.event.ActionEvent evt) {                                                   
        // TODO add your handling code here:
		try {
			if (!attributeNameCombo.getSelectedItem().equals(null)) {
				initializeAttributeValueCombo(attributeNameCombo
						.getSelectedItem().toString());
			}
		} catch (Exception e) {
			System.out.println(e);
		}
    }                                                  

    private void ruleElementTableMouseClicked(java.awt.event.MouseEvent evt) {                                              
        // TODO add your handling code here:
    	int selectedRow = ruleElementTable.getSelectedRow();
    	RuleElement ruleElement = ruleElements.get(selectedRow);
    	
    	Vector<String> attributeVector1 = new Vector<String>();
    	attributeVector1.add(RuleElement.ATTRIBUTES_SORTED[1]);
    	attributeVector1.add(ruleElement.getBackgroundAttachment());
    	ruleElementAttributesTableContent.add(attributeVector1);
    	
    	Vector<String> attributeVector2 = new Vector<String>();
    	attributeVector2.add(RuleElement.ATTRIBUTES_SORTED[2]);
    	attributeVector2.add(ruleElement.getBackgroundColor());
    	ruleElementAttributesTableContent.add(attributeVector2);
    	
    	Vector<String> attributeVector3 = new Vector<String>();
    	attributeVector3.add(RuleElement.ATTRIBUTES_SORTED[3]);
    	attributeVector3.add(ruleElement.getBackgroundImage());
    	ruleElementAttributesTableContent.add(attributeVector3);
    	
    	Vector<String> attributeVector4 = new Vector<String>();
    	attributeVector4.add(RuleElement.ATTRIBUTES_SORTED[4]);
    	attributeVector4.add(ruleElement.getBackgroundImage());
    	ruleElementAttributesTableContent.add(attributeVector4);
    	
    	Vector<String> attributeVector5 = new Vector<String>();
    	attributeVector5.add(RuleElement.ATTRIBUTES_SORTED[5]);
    	attributeVector5.add(ruleElement.getBackgroundImage());
    	ruleElementAttributesTableContent.add(attributeVector5);
    }                                             

    private void ruleElementAttributesTableMouseClicked(java.awt.event.MouseEvent evt) {                                                        
        // TODO add your handling code here:
    }                                                       

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RuleMakerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RuleMakerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RuleMakerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RuleMakerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RuleMakerGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton addAttributeButton;
    private javax.swing.JButton addRuleElementButton;
    private javax.swing.JComboBox attributeNameCombo;
    private javax.swing.JComboBox attributeValueCombo;
    private javax.swing.JButton continueButton;
    private javax.swing.JButton deleteAttributeButton;
    private javax.swing.JButton deleteRuleElementButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable ruleElementAttributesTable;
    private javax.swing.JTextField ruleElementIDText;
    private javax.swing.JTextField ruleElementNameText;
    private javax.swing.JTable ruleElementTable;
    private javax.swing.JTextField ruleElementTypeText;
    // End of variables declaration                   
}
