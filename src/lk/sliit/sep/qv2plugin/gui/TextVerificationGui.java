package lk.sliit.sep.qv2plugin.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import lk.sliit.sep.qv2plugin.verification.*;

public class TextVerificationGui extends javax.swing.JFrame implements ActionListener{
	
	private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField testFontAlignment;
    private javax.swing.JTextField textFontColor;
    private javax.swing.JTextField textFontFamily;
    private javax.swing.JTextField textFontSize;
    private javax.swing.JButton verifyTextButton;
    private javax.swing.JTextField xpathText;
	public TextVerificationGui(){
		

		  jLabel13 = new javax.swing.JLabel();
	      jScrollPane1 = new javax.swing.JScrollPane();
	      jPanel1 = new javax.swing.JPanel();
	      jLabel1 = new javax.swing.JLabel();
	      jLabel2 = new javax.swing.JLabel();
	      xpathText = new javax.swing.JTextField();
	      jLabel3 = new javax.swing.JLabel();
	      jLabel4 = new javax.swing.JLabel();
	      jLabel5 = new javax.swing.JLabel();
	      jLabel6 = new javax.swing.JLabel();
	      textFontFamily = new javax.swing.JTextField();
	      textFontSize = new javax.swing.JTextField();
	      testFontAlignment = new javax.swing.JTextField();
	      textFontColor = new javax.swing.JTextField();
	      verifyTextButton = new javax.swing.JButton();
	      jLabel7 = new javax.swing.JLabel();
	      jLabel8 = new javax.swing.JLabel();
	      jLabel9 = new javax.swing.JLabel();
	      jLabel10 = new javax.swing.JLabel();
	      jLabel11 = new javax.swing.JLabel();
	      jLabel12 = new javax.swing.JLabel();
	      jLabel14 = new javax.swing.JLabel();
	      jLabel15 = new javax.swing.JLabel();
	      jLabel16 = new javax.swing.JLabel();
	      jLabel17 = new javax.swing.JLabel();
	      jLabel18 = new javax.swing.JLabel();

	      jLabel13.setText("jLabel13");

	      

	      jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
	      jLabel1.setText("Verify Text");

	      jLabel2.setText("Enter XPath");

	      jLabel3.setText("Font family");

	      jLabel4.setText("Font size");

	      jLabel5.setText("Font alignment");

	      jLabel6.setText("Font color");

	      verifyTextButton.setText("Verify");

	      jLabel7.setText("Is bold");

	      jLabel8.setText("Is italic");

	      jLabel9.setText("Is underlined");

	      jLabel10.setText("");

	      jLabel11.setText("");

	      jLabel12.setText("");

	      jLabel14.setText("");

	      jLabel15.setText("");

	      jLabel16.setText("");

	      jLabel17.setText("");

	      jLabel18.setText("");

	      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
	      jPanel1.setLayout(jPanel1Layout);
	      jPanel1Layout.setHorizontalGroup(
	          jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	          .addGroup(jPanel1Layout.createSequentialGroup()
	              .addGap(29, 29, 29)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                  .addComponent(jLabel1)
	                  .addGroup(jPanel1Layout.createSequentialGroup()
	                      .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                          .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
	                              .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                              .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE))
	                          .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
	                      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                      .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                          .addGroup(jPanel1Layout.createSequentialGroup()
	                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                                  .addComponent(xpathText)
	                                  .addComponent(textFontFamily)
	                                  .addComponent(textFontSize)
	                                  .addComponent(testFontAlignment)
	                                  .addComponent(textFontColor, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE))
	                              .addGap(18, 18, 18)
	                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                                  .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
	                                      .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
	                                      .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                                      .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                                      .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                                  .addComponent(verifyTextButton)))
	                          .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
	                          .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
	              .addContainerGap(53, Short.MAX_VALUE))
	      );
	      jPanel1Layout.setVerticalGroup(
	          jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	          .addGroup(jPanel1Layout.createSequentialGroup()
	              .addGap(31, 31, 31)
	              .addComponent(jLabel1)
	              .addGap(18, 18, 18)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                  .addComponent(jLabel2)
	                  .addComponent(xpathText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                  .addComponent(jLabel10))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                  .addComponent(jLabel3)
	                  .addComponent(textFontFamily, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                  .addComponent(jLabel11))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                  .addComponent(jLabel4)
	                  .addComponent(textFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                  .addComponent(jLabel12))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                  .addComponent(jLabel5)
	                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                      .addComponent(testFontAlignment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                      .addComponent(jLabel14)))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                  .addComponent(jLabel6)
	                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                      .addComponent(textFontColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                      .addComponent(jLabel15)))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                  .addComponent(jLabel7)
	                  .addComponent(jLabel16))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                  .addComponent(jLabel8)
	                  .addComponent(jLabel17))
	              .addGap(26, 26, 26)
	              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                  .addComponent(jLabel9)
	                  .addComponent(jLabel18))
	              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	              .addComponent(verifyTextButton)
	              .addGap(117, 117, 117))
	      );

	      jScrollPane1.setViewportView(jPanel1);

	      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	      getContentPane().setLayout(layout);
	      layout.setHorizontalGroup(
	          layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	          .addGroup(layout.createSequentialGroup()
	              .addContainerGap()
	              .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 644, javax.swing.GroupLayout.PREFERRED_SIZE)
	              .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	      );
	      layout.setVerticalGroup(
	          layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	          .addGroup(layout.createSequentialGroup()
	              .addContainerGap()
	              .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
	              .addContainerGap())
	      );

	      pack();
	      
	      verifyTextButton.addActionListener(this);
		
	}
  // </editor-fold>                        

	public void actionPerformed(ActionEvent e) {
		
		WebDriver wb;
		System.setProperty("webdriver.chrome.driver", "E:\\chromedriver_win32\\chromedriver.exe");		
		wb = new ChromeDriver();
		wb.get("http://www.google.com");
		
//		Text text = new Text(xpathText.getText(), wb);
//		boolean fontFamily = text.verifyFont(textFontFamily.getText());
//		boolean fontSize = text.verifyFontSize(textFontSize.getText());
//		boolean fontAl= text.verifyFontAlignment(testFontAlignment.getText());
//		boolean fontColor = text.verifyFontColor(textFontColor.getText());	
//		boolean bold = text.verifyIsBold();
//		boolean italic = text.verifyIsItalic();
//		boolean underlined = text.verifyIsUnderlined();
		
//		if (fontFamily){
//			jLabel11.setText("Passed");
//		} else if (!fontFamily){
//			jLabel11.setText("Failed");
//		}
//
//		if (fontSize){
//			jLabel12.setText("Passed");
//		} else if(!fontSize){
//			jLabel12.setText("Failed");
//		}
//		
//		if (fontAl){
//			jLabel14.setText("Passed");
//		} else if(!fontSize){
//			jLabel14.setText("Failed");
//		}
//		
//		if (fontColor){
//			jLabel15.setText("Passed");
//		} else if(!fontColor){
//			jLabel15.setText("Failed");
//		}
//		
//		if (bold){
//			jLabel16.setText("Passed");
//		} else if(!bold){
//			jLabel16.setText("Failed");
//		}
//		
//		if (italic){
//			jLabel17.setText("Passed");
//		} else if(!italic){
//			jLabel17.setText("Failed");
//		}
//		
//		if (underlined){
//			jLabel18.setText("Passed");
//		} else if(!underlined){
//			jLabel18.setText("Failed");
//		}
		
	}

}
