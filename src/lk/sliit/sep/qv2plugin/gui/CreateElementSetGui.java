package lk.sliit.sep.qv2plugin.gui;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.thoughtworks.xstream.XStream;

import lk.sliit.sep.qv2plugin.*;

public class CreateElementSetGui  extends JFrame implements ActionListener {
		
		RuleElementSet rst;
		ArrayList <RuleElement> ruleElementList = new ArrayList<RuleElement>();
	 	private javax.swing.JButton addElementButton;
	    private javax.swing.JButton createRuleElemSet;
	    private javax.swing.JTextField elemAbsPosition;
	    private javax.swing.JTextField elemBkgColor;
	    private javax.swing.JTextField elemFgroundColor;
	    private javax.swing.JTextField elemFont;
	    private javax.swing.JTextField elemHeight;
	    private javax.swing.JTextField elemId;
	    private javax.swing.JTextField elemName;
	    private javax.swing.JTextField elemRelDistance;
	    private javax.swing.JTextField elemText;
	    private javax.swing.JTextField elemToolTipText;
	    private javax.swing.JTextField elemType;
	    private javax.swing.JTextField elemWidth;
	    private javax.swing.JLabel jLabel1;
	    private javax.swing.JLabel jLabel10;
	    private javax.swing.JLabel jLabel11;
	    private javax.swing.JLabel jLabel12;
	    private javax.swing.JLabel jLabel13;
	    private javax.swing.JLabel jLabel14;
	    private javax.swing.JLabel jLabel15;
	    private javax.swing.JLabel jLabel16;
	    private javax.swing.JLabel jLabel2;
	    private javax.swing.JLabel jLabel3;
	    private javax.swing.JLabel jLabel4;
	    private javax.swing.JLabel jLabel5;
	    private javax.swing.JLabel jLabel6;
	    private javax.swing.JLabel jLabel7;
	    private javax.swing.JLabel jLabel8;
	    private javax.swing.JLabel jLabel9;
	    private javax.swing.JPanel jPanel1;
	    private javax.swing.JScrollPane jScrollPane1;
	    private javax.swing.JButton serializeButton;
	    private javax.swing.JTextField setId;
	    private javax.swing.JTextField setName;
	
	public CreateElementSetGui(){
		
		 jScrollPane1 = new javax.swing.JScrollPane();
	        jPanel1 = new javax.swing.JPanel();
	        jLabel1 = new javax.swing.JLabel();
	        jLabel2 = new javax.swing.JLabel();
	        jLabel3 = new javax.swing.JLabel();
	        jLabel4 = new javax.swing.JLabel();
	        jLabel5 = new javax.swing.JLabel();
	        jLabel6 = new javax.swing.JLabel();
	        jLabel7 = new javax.swing.JLabel();
	        jLabel8 = new javax.swing.JLabel();
	        jLabel9 = new javax.swing.JLabel();
	        jLabel10 = new javax.swing.JLabel();
	        jLabel11 = new javax.swing.JLabel();
	        jLabel12 = new javax.swing.JLabel();
	        jLabel13 = new javax.swing.JLabel();
	        jLabel14 = new javax.swing.JLabel();
	        jLabel15 = new javax.swing.JLabel();
	        jLabel16 = new javax.swing.JLabel();
	        elemId = new javax.swing.JTextField();
	        elemName = new javax.swing.JTextField();
	        elemType = new javax.swing.JTextField();
	        elemHeight = new javax.swing.JTextField();
	        elemWidth = new javax.swing.JTextField();
	        elemBkgColor = new javax.swing.JTextField();
	        elemFgroundColor = new javax.swing.JTextField();
	        elemAbsPosition = new javax.swing.JTextField();
	        elemRelDistance = new javax.swing.JTextField();
	        elemFont = new javax.swing.JTextField();
	        elemToolTipText = new javax.swing.JTextField();
	        elemText = new javax.swing.JTextField();
	        addElementButton = new javax.swing.JButton();
	        createRuleElemSet = new javax.swing.JButton();
	        serializeButton = new javax.swing.JButton();
	        setId = new javax.swing.JTextField();
	        setName = new javax.swing.JTextField();

	        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
	        jLabel1.setText("Create Rule Element Set");

	        jLabel2.setText("Id:");

	        jLabel3.setText("Name");

	        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
	        jLabel4.setText("Rule Element");

	        jLabel5.setText("Id:");

	        jLabel6.setText("Name:");

	        jLabel7.setText("Type:");

	        jLabel8.setText("Height:");

	        jLabel9.setText("Width:");

	        jLabel10.setText("Background Color:");

	        jLabel11.setText("Foreground Color:");

	        jLabel12.setText("Absolute Position:");

	        jLabel13.setText("Relative Distance:");

	        jLabel14.setText("Font:");

	        jLabel15.setText("Tool Tip Text:");

	        jLabel16.setText("Text:");

	        addElementButton.setText("Add Element");

	        createRuleElemSet.setText("Create Rule Element Set");

	        serializeButton.setText("Serialize");

	        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
	        jPanel1.setLayout(jPanel1Layout);
	        jPanel1Layout.setHorizontalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addContainerGap()
	                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addGroup(jPanel1Layout.createSequentialGroup()
	                                .addGap(98, 98, 98)
	                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                                    .addComponent(jLabel5)
	                                    .addComponent(jLabel6)
	                                    .addComponent(jLabel7)
	                                    .addComponent(jLabel8)
	                                    .addComponent(jLabel9)
	                                    .addComponent(jLabel10)
	                                    .addComponent(jLabel11)
	                                    .addComponent(jLabel12)
	                                    .addComponent(jLabel13)
	                                    .addComponent(jLabel14)
	                                    .addComponent(jLabel15)
	                                    .addComponent(jLabel16)))
	                            .addGroup(jPanel1Layout.createSequentialGroup()
	                                .addGap(61, 61, 61)
	                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))))
	                        .addGap(76, 76, 76)
	                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                            .addComponent(elemId)
	                            .addComponent(elemName)
	                            .addComponent(elemType)
	                            .addComponent(elemHeight)
	                            .addComponent(elemWidth)
	                            .addComponent(elemBkgColor)
	                            .addComponent(elemFgroundColor)
	                            .addComponent(elemAbsPosition)
	                            .addComponent(elemRelDistance)
	                            .addComponent(elemFont)
	                            .addComponent(elemToolTipText)
	                            .addComponent(elemText)
	                            .addComponent(setId)
	                            .addComponent(setName, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE))))
	                .addContainerGap(114, Short.MAX_VALUE))
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
	                .addGap(0, 0, Short.MAX_VALUE)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addComponent(addElementButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(createRuleElemSet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(serializeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                .addGap(114, 114, 114))
	        );
	        jPanel1Layout.setVerticalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(34, 34, 34)
	                .addComponent(jLabel1)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jLabel2)
	                    .addComponent(setId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jLabel3)
	                    .addComponent(setName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addComponent(jLabel4)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jLabel5)
	                    .addComponent(elemId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(8, 8, 8)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel6)
	                    .addComponent(elemName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel7)
	                    .addComponent(elemType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel8)
	                    .addComponent(elemHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel9)
	                    .addComponent(elemWidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel10)
	                    .addComponent(elemBkgColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel11)
	                    .addComponent(elemFgroundColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel12)
	                    .addComponent(elemAbsPosition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel13)
	                    .addComponent(elemRelDistance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel14)
	                    .addComponent(elemFont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel15)
	                    .addComponent(elemToolTipText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel16)
	                    .addComponent(elemText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(19, 19, 19)
	                .addComponent(addElementButton)
	                .addGap(36, 36, 36)
	                .addComponent(createRuleElemSet)
	                .addGap(29, 29, 29)
	                .addComponent(serializeButton)
	                .addContainerGap(83, Short.MAX_VALUE))
	        );

	        jScrollPane1.setViewportView(jPanel1);

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	        getContentPane().setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane1)
	                .addContainerGap())
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );

	        pack();
	        addElementButton.addActionListener(this);
	        createRuleElemSet.addActionListener(this);
	        serializeButton.addActionListener(this);
	}
	
	public void createElementList(){
		Integer id = Integer.parseInt(elemId.getText());
		String name = elemName.getText();
		String type = elemType.getText();
		Double height = Double.parseDouble(elemHeight.getText());
		Double width = Double.parseDouble(elemWidth.getText());
		//Point absPosition = (Point) elemAbsPosition.getText();
		
		RuleElement element = new RuleElement();
		element.setId(id);
		element.setName(name);
		element.setType(type);
//		element.setHeight(height);
//		element.setWidth(width);
		
		ruleElementList.add(element);
		JOptionPane.showMessageDialog(null, "Element added!", "Element Creation", JOptionPane.INFORMATION_MESSAGE);
		elemId.setText(null);
		elemName.setText(null);
		elemType.setText(null);
		elemHeight.setText(null);
		elemWidth.setText(null);
		
	}
	
	public void createRuleElementSet(){
		Integer id = Integer.parseInt(setId.getText());
		String name = setName.getText();
		RuleElement[] ruleElementArray = new RuleElement[ruleElementList.size()];
		ruleElementArray = ruleElementList.toArray(ruleElementArray);
		
		rst = new RuleElementSet(id, name, ruleElementArray);
		JOptionPane.showMessageDialog(null, "Rule element set created!", "Rule element set creation", JOptionPane.INFORMATION_MESSAGE);
		setId.setText(null);
		setName.setText(null);
	}
	
	public void actionPerformed(ActionEvent e) {
	
		if (e.getSource()== addElementButton){
			createElementList();
			System.out.println("Done");
		} else if (e.getSource() == createRuleElemSet){
			createRuleElementSet();
			System.out.println("Done");
		} else{
			XStream xstream = new XStream();
			File file = new File("E://RuleElement.xml");
			FileOutputStream fs;
			try {
				fs = new FileOutputStream(file);
				xstream.toXML(rst, fs);
				fs.close();
			} catch (FileNotFoundException e1) {
				
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null,"Serialized!", "Serialization", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	

	
	
}
