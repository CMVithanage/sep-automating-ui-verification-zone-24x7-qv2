package lk.sliit.sep.qv2plugin.exceptions;

public class UndefinedException extends Exception{
	
	public UndefinedException(String msg){
		super(msg);
	}

}
