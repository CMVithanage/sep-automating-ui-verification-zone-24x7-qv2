package lk.sliit.sep.qv2plugin;

import java.awt.Point;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("restriction")
@XmlRootElement
public class RuleElement implements java.io.Serializable {
	//Identification Properties
	private int id; //Element Id
	private String name; //Element Name
	private String type; //Element Type

	//Background Properties
	private String backgroundAttachment; //Predefined Values
	private String backgroundColor;	//Valid color names, RGB values or hexadecimal notation
	private String backgroundImage; //URL of an Image
	private String backgroundPosition; //Coordinates or Predefined Position
	private String backgroundRepeat; //Predefined Values
	
	//Border Properties
	//Top
	private String borderTopColor; //Valid color names, RGB values or hexadecimal notation
	private String borderTopStyle; //Predefined Values
	private String borderTopWidth; //Predefined Values or length in pixels
	
	//Right
	private String borderRightColor; //Valid color names, RGB values or hexadecimal notation
	private String borderRightStyle; //Predefined Values
	private String borderRightWidth; //Predefined Values or length in pixels
	
	//Bottom
	private String borderBottomColor; //Valid color names, RGB values or hexadecimal notation
	private String borderBottomStyle; //Predefined Values
	private String borderBottomWidth; //Predefined Values or length in pixels
	
	//Left
	private String borderLeftColor; //Valid color names, RGB values or hexadecimal notation
	private String borderLeftStyle; //Predefined Values
	private String borderLeftWidth; //Predefined Values or length in pixels
	
	//Global (Use only if all attributes are true to Top, Right, Bottom, Left)
	private String borderColor; //Valid color names, RGB values or hexadecimal notation
	private String borderStyle; //Predefined Values
	private String borderWidth; //Predefined Values or length in pixels
	
	//Classification and Positioning Properties
	private String clear; //Predefined Values
	private String cursor; //URL and Predefined values
	private String display; //Predefined Values
	private String positionFloat; //Predefined Values
	private String visibility; //Predefined Values
	private String top; //Lengths, percentages and Predefined value - auto
	private String right; //Lengths, percentages and Predefined value - auto
	private String bottom; //Lengths, percentages and Predefined value - auto
	private String left; //Lengths, percentages and Predefined value - auto
	private String position; //Predefined Values
	private String clip; //Shapes or Predefined value - auto
	private String overflow; //Predefined Values
	private String verticalAlign; //Lengths, percentages and Predefined values
	private String zIndex; //Integer values and Predefined value - auto
	
	//Dimension Properties
	private String height; //Lengths, percentages and Predefined value - auto
	private String maxHeight; //Lengths, percentages and Predefined value - auto
	private String minHeight; //Lengths, percentages and Predefined value - auto
	private String width; //Lengths, percentages and Predefined value - auto
	private String maxWidth; //Lengths, percentages and Predefined value - auto
	private String minWidth; //Lengths, percentages and Predefined value - auto
	
	//Font Properties
	private String fontFamily; //Valid font family names
	private String fontSize; //Lengths, percentages and Predefined values
	private String fontSizeAdjust; //Numeric value (float) [Limited Support]
	private String fontStretch; //Predefined Values [Limited Support]
	private String fontStyle; //Predefined Values
	private String fontVariant; //Predefined Values
	private String fontWeight; //Predefined Values
	
	//Generated Content Properties
	private String content; //String values, URL values and Predefined value formats
	private String counterIncrement; //Integer values and Predefined value - none
	private String counterReset; //Integer values and Predefined value - none
	private String quotes; //String values and Predefined value - none
	
	//List Properties
	private String listStyleType; //Predefined Values
	private String listStylePosition; //Predefined Values
	private String listStyleImage; //URL values
	private String markerOffset; //Lengths and Predefined value - auto
	
	//Margin Properties
	private String marginTop; //Lengths, percentages and Predefined value - auto
	private String marginRight; //Lengths, percentages and Predefined value - auto
	private String marginBottom; //Lengths, percentages and Predefined value - auto
	private String marginLeft; //Lengths, percentages and Predefined value - auto
	
	//Outline Properties
	private String outlineColor; //Valid color names, RGB values or hexadecimal notation
	private String outlineStyle; //Predefined Values
	private String outlineWidth; //Lengths or Predefined value
	
	//Padding Properties
	private String paddingTop; //Lengths, percentages and Predefined value - auto
	private String paddingRight; //Lengths, percentages and Predefined value - auto
	private String paddingBottom; //Lengths, percentages and Predefined value - auto
	private String paddingLeft; //Lengths, percentages and Predefined value - auto
	
	//Page Properties
	private String marks; //Predefined Values
	private String orphans; //Integer Values
	private String page; //Identifiers
	private String pageBreakAfter; //Predefined Values
	private String pageBreakBefore; //Predefined Values
	private String pageBreakInside; //Predefined Values
	private String size; //Lengths and Predefined values
	private String windows; //Integer Values
	
	//Table Properties
	private String borderCollapse; //Predefined Values
	private String borderSpacing; //Lengths for the horizontal and vertical spacing, separated by a space
	private String captionSide; //Predefined Values
	private String emptyCells; //Predefined Values
	private String tableLayout; //Predefined Values
	
	//Text Properties
	private String color; //Valid color names, RGB values or hexadecimal notation
	private String direction; //Predefined Values
	private String lineHeight; //Numbers, Percentages, lengths and the Predefined value - normal
	private String letterSpacing; //A length (in addition to the default space) or the Predefined value - normal
	private String textAlign; //Predefined Values
	private String textDecoration; //Predefined Values
	private String textIndent; //Lengths and percentages
	private String textShadow; //A list containing a color followed by numeric values
	private String textTransform; //Predefined Values
	private String unicodeBidi; //Predefined Values
	private String whiteSpace; //Predefined Values
	private String wordSpacing; //A length (in addition to the default space) or the Predefined value - normal

	//HTML Properties
	private String toolTipText;
	private String text;
	
	//Local Positioning Properties
	private Point absolutePosition;
	private Map<Integer, Double> relativeDistance;
	
	//Attribute Strings List
	public static final String[] ATTRIBUTES = new String[] {
			"backgroundAttachment", 
			"backgroundColor", 
			"backgroundImage", 
			"backgroundPosition", 
			"backgroundRepeat", 
			"borderTopColor", 
			"borderTopStyle", 
			"borderTopWidth", 
			"borderRightColor", 
			"borderRightStyle", 
			"borderRightWidth", 
			"borderBottomColor", 
			"borderBottomStyle", 
			"borderBottomWidth", 
			"borderLeftColor", 
			"borderLeftStyle", 
			"borderLeftWidth", 
			"borderColor", 
			"borderStyle", 
			"borderWidth", 
			"clear", 
			"cursor", 
			"display", 
			"positionFloat", 
			"visibility", 
			"top", 
			"right", 
			"bottom", 
			"left", 
			"position", 
			"clip", 
			"overflow", 
			"verticalAlign", 
			"zIndex", 
			"height", 
			"maxHeight", 
			"minHeight", 
			"width", 
			"maxWidth", 
			"minWidth", 
			"fontFamily", 
			"fontSize", 
			"fontSizeAdjust", 
			"fontStretch", 
			"fontStyle", 
			"fontVariant", 
			"fontWeight", 
			"content", 
			"counterIncrement", 
			"counterReset", 
			"quotes", 
			"listStyleType", 
			"listStylePosition", 
			"listStyleImage", 
			"markerOffset", 
			"marginTop", 
			"marginRight", 
			"marginBottom", 
			"marginLeft", 
			"outlineColor", 
			"outlineStyle", 
			"outlineWidth", 
			"paddingTop", 
			"paddingRight", 
			"paddingBottom", 
			"paddingLeft", 
			"marks", 
			"orphans", 
			"page", 
			"pageBreakAfter", 
			"pageBreakBefore", 
			"pageBreakInside", 
			"size", 
			"windows", 
			"borderCollapse", 
			"borderSpacing", 
			"captionSide", 
			"emptyCells", 
			"tableLayout", 
			"color", 
			"direction", 
			"lineHeight", 
			"letterSpacing", 
			"textAlign", 
			"textDecoration", 
			"textIndent", 
			"textShadow", 
			"textTransform", 
			"unicodeBidi", 
			"whiteSpace", 
			"wordSpacing", 
			"toolTipText", 
			"text", 
			"absolutePosition", 
			"relativeDistance"
	};
	
	public static final String[] ATTRIBUTES_SORTED = new String[] {
			"absolutePosition", 
			"backgroundAttachment", 
			"backgroundColor", 
			"backgroundImage", 
			"backgroundPosition", 
			"backgroundRepeat", 
			"borderBottomColor", 
			"borderBottomStyle", 
			"borderBottomWidth", 
			"borderCollapse", 
			"borderColor", 
			"borderLeftColor", 
			"borderLeftStyle", 
			"borderLeftWidth", 
			"borderRightColor", 
			"borderRightStyle", 
			"borderRightWidth", 
			"borderSpacing", 
			"borderStyle", 
			"borderTopColor", 
			"borderTopStyle", 
			"borderTopWidth", 
			"borderWidth", 
			"bottom", 
			"captionSide", 
			"clear", 
			"clip", 
			"color", 
			"content", 
			"counterIncrement", 
			"counterReset", 
			"cursor", 
			"direction", 
			"display", 
			"emptyCells", 
			"fontFamily", 
			"fontSize", 
			"fontSizeAdjust", 
			"fontStretch", 
			"fontStyle", 
			"fontVariant", 
			"fontWeight", 
			"height", 
			"left", 
			"letterSpacing", 
			"lineHeight", 
			"listStyleImage", 
			"listStylePosition", 
			"listStyleType", 
			"marginBottom", 
			"marginLeft", 
			"marginRight", 
			"marginTop", 
			"markerOffset", 
			"marks", 
			"maxHeight", 
			"maxWidth", 
			"minHeight", 
			"minWidth", 
			"orphans", 
			"outlineColor", 
			"outlineStyle", 
			"outlineWidth", 
			"overflow", 
			"paddingBottom", 
			"paddingLeft", 
			"paddingRight", 
			"paddingTop", 
			"page", 
			"pageBreakAfter", 
			"pageBreakBefore", 
			"pageBreakInside", 
			"position", 
			"positionFloat", 
			"quotes", 
			"relativeDistance", 
			"right", 
			"size", 
			"tableLayout", 
			"text", 
			"textAlign", 
			"textDecoration", 
			"textIndent", 
			"textShadow", 
			"textTransform", 
			"toolTipText", 
			"top", 
			"unicodeBidi", 
			"verticalAlign", 
			"visibility", 
			"whiteSpace", 
			"width", 
			"windows", 
			"wordSpacing", 
			"zIndex", 
	};

	//Attribute Predefined Values List
	public static final String[] BACKGROUNDATTACHMENT_VALUES = new String[] {
		"fixed",
		"scroll"
	};

	public static final String[] BACKGROUNDPOSITION_VALUES = new String[] {
		"top left",
		"top center",
		"top right",
		"center left",
		"center center",
		"center right",
		"bottom left",
		"bottom center",
		"bottom right"
	};

	public static final String[] BACKGROUNDREPEAT_VALUES = new String[] {
		"repeat",
		"repeat-x",
		"repeat-y",
		"no-repeat"
	};

	public static final String[] BORDERCOLOR_VALUES = new String[] {
		"transparent"
	};

	public static final String[] BORDERSTYLE_VALUES = new String[] {
		"none",
		"hidden",
		"dotted",
		"dashed",
		"solid",
		"double",
		"groove",
		"ridge",
		"inset",
		"outset"
	};

	public static final String[] BORDERWIDTH_VALUES = new String[] {
		"thin",
		"medium",
		"thick"
	};

	public static final String[] CLEAR_VALUES = new String[] {
		"left",
		"right",
		"both",
		"none"
	};

	public static final String[] CURSOR_VALUES = new String[] {
		"auto",
		"crosshair",
		"default",
		"pointer",
		"move",
		"e-resize",
		"ne-resize",
		"nw-resize",
		"n-resize",
		"se-resize",
		"sw-resize",
		"s-resize",
		"w-resize",
		"text",
		"wait",
		"help"
	};

	public static final String[] DISPLAY_VALUES = new String[] {
		"none",
		"inline",
		"block",
		"list-item",
		"run-in",
		"compact",
		"marker",
		"table",
		"inline-table",
		"table-row-group",
		"table-header-group",
		"table-footer-group",
		"table-row",
		"table-column-group",
		"table-column",
		"table-cell",
		"table-caption"
	};

	public static final String[] POSITIONFLOAT_VALUES = new String[] {
		"left",
		"right",
		"none"
	};

	public static final String[] VISIBILITY_VALUES = new String[] {
		"visible",
		"hidden",
		"collapse"
	};

	public static final String[] TOP_VALUES = new String[] {
		"auto"
	};

	public static final String[] RIGHT_VALUES = new String[] {
		"auto"
	};

	public static final String[] BOTTOM_VALUES = new String[] {
		"auto"
	};

	public static final String[] LEFT_VALUES = new String[] {
		"auto"
	};

	public static final String[] POSITION_VALUES = new String[] {
		"static",
		"relative",
		"absolute",
		"fixed"
	};

	public static final String[] CLIP_VALUES = new String[] {
		"auto"
	};

	public static final String[] OVERFLOW_VALUES = new String[] {
		"visible",
		"hidden",
		"scroll",
		"auto"
	};

	public static final String[] VERTICALALIGN_VALUES = new String[] {
		"baseline",
		"sub",
		"super",
		"top",
		"text-top",
		"middle",
		"bottom",
		"text-bottom"
	};

	public static final String[] ZINDEX_VALUES = new String[] {
		"auto"
	};

	public static final String[] HEIGHT_VALUES = new String[] {
		"auto"
	};

	public static final String[] MAXHEIGHT_VALUES = new String[] {
		"auto"
	};

	public static final String[] MINHEIGHT_VALUES = new String[] {
		"auto"
	};

	public static final String[] WIDTH_VALUES = new String[] {
		"auto"
	};

	public static final String[] MAXWIDTH_VALUES = new String[] {
		"auto"
	};

	public static final String[] MINWIDTH_VALUES = new String[] {
		"auto"
	};

	public static final String[] FONTFAMILY_VALUES = new String[] {
		"Antiqua",
		"Arial",
		"Blackletter",
		"Calibri",
		"Comic Sans",
		"Courier",
		"Cursive",
		"Decorative",
		"Fantasy",
		"Fraktur",
		"Frosty",
		"Garamond",
		"Georgia",
		"Helvetica",
		"Impact",
		"Minion",
		"Modern",
		"Monospace",
		"Palatino",
		"Roman",
		"Sans-serif",
		"Serif",
		"Script",
		"Swiss",
		"Times",
		"Times New Roman",
		"Verdana"
	};

	public static final String[] FONTSIZE_VALUES = new String[] {
		"xx-small",
		"x-small",
		"small",
		"medium",
		"large",
		"x-large",
		"xx-large",
		"smaller",
		"larger"
	};

	public static final String[] FONTSTRETCH_VALUES = new String[] {
		"normal",
		"wider",
		"narrower",
		"ultra-condensed",
		"extra-condensed",
		"condensed",
		"semi-condensed",
		"semi-expanded",
		"expanded",
		"extra-expanded",
		"ultra-expanded"
	};

	public static final String[] FONTSTYLE_VALUES = new String[] {
		"normal",
		"italic",
		"oblique"
	};

	public static final String[] FONTVARIANT_VALUES = new String[] {
		"normal",
		"small-caps"
	};

	public static final String[] FONTWEIGHT_VALUES = new String[] {
		"normal",
		"bold",
		"bolder",
		"lighter",
		"100",
		"200",
		"300",
		"400",
		"500",
		"600",
		"700",
		"800",
		"900"
	};

	public static final String[] CONTENT_VALUES = new String[] {
		//Value Formats
		"counter(name)",
		"counter(name, list-style-type)",
		"counters(name, string)",
		"counters(name, string, list-style-type)",
		"attr(X)",
		"open-quote",
		"close-quote",
		"no-open-quote",
		"no-close-quote"
	};

	public static final String[] COUNTERINCREMENT_VALUES = new String[] {
		"none"
	};

	public static final String[] COUNTERRESET_VALUES = new String[] {
		"none"
	};

	public static final String[] QUOTES_VALUES = new String[] {
		"none"
	};

	public static final String[] LISTSTYLETYPE_VALUES = new String[] {
		"disc",
		"circle",
		"square",
		"decimal",
		"decimal-leading-zero",
		"lower-roman",
		"upper-roman",
		"lower-alpha",
		"upper-alpha",
		"lower-greek",
		"lower-latin",
		"upper-latin",
		"hebrew",
		"armenian",
		"georgian",
		"cjk-ideographic",
		"hiragana",
		"katakana",
		"hiragana-iroha",
		"katakana-iroha"
	};

	public static final String[] LISTSTYLEPOSITION_VALUES = new String[] {
		"inside",
		"outside"
	};

	public static final String[] MARKEROFFSET_VALUES = new String[] {
		"auto"
	};

	public static final String[] MARGINTOP_VALUES = new String[] {
		"auto"
	};

	public static final String[] MARGINRIGHT_VALUES = new String[] {
		"auto"
	};

	public static final String[] MARGINBOTTOM_VALUES = new String[] {
		"auto"
	};

	public static final String[] MARGINLEFT_VALUES = new String[] {
		"auto"
	};

	public static final String[] COLORNAME_VALUES = new String[] {
		"AliceBlue", 
		"AntiqueWhite", 
		"Aqua", 
		"Aquamarine", 
		"Azure", 
		"Beige", 
		"Bisque", 
		"Black", 
		"BlanchedAlmond", 
		"Blue", 
		"BlueViolet", 
		"Brown", 
		"BurlyWood", 
		"CadetBlue", 
		"Chartreuse", 
		"Chocolate", 
		"Coral", 
		"CornflowerBlue", 
		"Cornsilk", 
		"Crimson", 
		"Cyan", 
		"DarkBlue", 
		"DarkCyan", 
		"DarkGoldenRod", 
		"DarkGray", 
		"DarkGreen", 
		"DarkKhaki", 
		"DarkMagenta", 
		"DarkOliveGreen", 
		"DarkOrange", 
		"DarkOrchid", 
		"DarkRed", 
		"DarkSalmon", 
		"DarkSeaGreen", 
		"DarkSlateBlue", 
		"DarkSlateGray", 
		"DarkTurquoise", 
		"DarkViolet", 
		"DeepPink", 
		"DeepSkyBlue", 
		"DimGray", 
		"DodgerBlue", 
		"FireBrick", 
		"FloralWhite", 
		"ForestGreen", 
		"Fuchsia", 
		"Gainsboro", 
		"GhostWhite", 
		"Gold", 
		"GoldenRod", 
		"Gray", 
		"Green", 
		"GreenYellow", 
		"HoneyDew", 
		"HotPink", 
		"IndianRed ", 
		"Indigo ", 
		"Ivory", 
		"Khaki", 
		"Lavender", 
		"LavenderBlush", 
		"LawnGreen", 
		"LemonChiffon", 
		"LightBlue", 
		"LightCoral", 
		"LightCyan", 
		"LightGoldenRodYellow", 
		"LightGray", 
		"LightGreen", 
		"LightPink", 
		"LightSalmon", 
		"LightSeaGreen", 
		"LightSkyBlue", 
		"LightSlateGray", 
		"LightSteelBlue", 
		"LightYellow", 
		"Lime", 
		"LimeGreen", 
		"Linen", 
		"Magenta", 
		"Maroon", 
		"MediumAquaMarine", 
		"MediumBlue", 
		"MediumOrchid", 
		"MediumPurple", 
		"MediumSeaGreen", 
		"MediumSlateBlue", 
		"MediumSpringGreen", 
		"MediumTurquoise", 
		"MediumVioletRed", 
		"MidnightBlue", 
		"MintCream", 
		"MistyRose", 
		"Moccasin", 
		"NavajoWhite", 
		"Navy", 
		"OldLace", 
		"Olive", 
		"OliveDrab", 
		"Orange", 
		"OrangeRed", 
		"Orchid", 
		"PaleGoldenRod", 
		"PaleGreen", 
		"PaleTurquoise", 
		"PaleVioletRed", 
		"PapayaWhip", 
		"PeachPuff", 
		"Peru", 
		"Pink", 
		"Plum", 
		"PowderBlue", 
		"Purple", 
		"Red", 
		"RosyBrown", 
		"RoyalBlue", 
		"SaddleBrown", 
		"Salmon", 
		"SandyBrown", 
		"SeaGreen", 
		"SeaShell", 
		"Sienna", 
		"Silver", 
		"SkyBlue", 
		"SlateBlue", 
		"SlateGray", 
		"Snow", 
		"SpringGreen", 
		"SteelBlue", 
		"Tan", 
		"Teal", 
		"Thistle", 
		"Tomato", 
		"Turquoise", 
		"Violet", 
		"Wheat", 
		"White", 
		"WhiteSmoke", 
		"Yellow", 
		"YellowGreen"
	};

	public static final String[] COLORHEX_VALUES = new String[] {
		"#F0F8FF", //AliceBlue
		"#FAEBD7", //AntiqueWhite
		"#00FFFF", //Aqua
		"#7FFFD4", //Aquamarine
		"#F0FFFF", //Azure
		"#F5F5DC", //Beige
		"#FFE4C4", //Bisque
		"#000000", //Black
		"#FFEBCD", //BlanchedAlmond
		"#0000FF", //Blue
		"#8A2BE2", //BlueViolet
		"#A52A2A", //Brown
		"#DEB887", //BurlyWood
		"#5F9EA0", //CadetBlue
		"#7FFF00", //Chartreuse
		"#D2691E", //Chocolate
		"#FF7F50", //Coral
		"#6495ED", //CornflowerBlue
		"#FFF8DC", //Cornsilk
		"#DC143C", //Crimson
		"#00FFFF", //Cyan
		"#00008B", //DarkBlue
		"#008B8B", //DarkCyan
		"#B8860B", //DarkGoldenRod
		"#A9A9A9", //DarkGray
		"#006400", //DarkGreen
		"#BDB76B", //DarkKhaki
		"#8B008B", //DarkMagenta
		"#556B2F", //DarkOliveGreen
		"#FF8C00", //DarkOrange
		"#9932CC", //DarkOrchid
		"#8B0000", //DarkRed
		"#E9967A", //DarkSalmon
		"#8FBC8F", //DarkSeaGreen
		"#483D8B", //DarkSlateBlue
		"#2F4F4F", //DarkSlateGray
		"#00CED1", //DarkTurquoise
		"#9400D3", //DarkViolet
		"#FF1493", //DeepPink
		"#00BFFF", //DeepSkyBlue
		"#696969", //DimGray
		"#1E90FF", //DodgerBlue
		"#B22222", //FireBrick
		"#FFFAF0", //FloralWhite
		"#228B22", //ForestGreen
		"#FF00FF", //Fuchsia
		"#DCDCDC", //Gainsboro
		"#F8F8FF", //GhostWhite
		"#FFD700", //Gold
		"#DAA520", //GoldenRod
		"#808080", //Gray
		"#008000", //Green
		"#ADFF2F", //GreenYellow
		"#F0FFF0", //HoneyDew
		"#FF69B4", //HotPink
		"#CD5C5C", //IndianRed 
		"#4B0082", //Indigo 
		"#FFFFF0", //Ivory
		"#F0E68C", //Khaki
		"#E6E6FA", //Lavender
		"#FFF0F5", //LavenderBlush
		"#7CFC00", //LawnGreen
		"#FFFACD", //LemonChiffon
		"#ADD8E6", //LightBlue
		"#F08080", //LightCoral
		"#E0FFFF", //LightCyan
		"#FAFAD2", //LightGoldenRodYellow
		"#D3D3D3", //LightGray
		"#90EE90", //LightGreen
		"#FFB6C1", //LightPink
		"#FFA07A", //LightSalmon
		"#20B2AA", //LightSeaGreen
		"#87CEFA", //LightSkyBlue
		"#778899", //LightSlateGray
		"#B0C4DE", //LightSteelBlue
		"#FFFFE0", //LightYellow
		"#00FF00", //Lime
		"#32CD32", //LimeGreen
		"#FAF0E6", //Linen
		"#FF00FF", //Magenta
		"#800000", //Maroon
		"#66CDAA", //MediumAquaMarine
		"#0000CD", //MediumBlue
		"#BA55D3", //MediumOrchid
		"#9370DB", //MediumPurple
		"#3CB371", //MediumSeaGreen
		"#7B68EE", //MediumSlateBlue
		"#00FA9A", //MediumSpringGreen
		"#48D1CC", //MediumTurquoise
		"#C71585", //MediumVioletRed
		"#191970", //MidnightBlue
		"#F5FFFA", //MintCream
		"#FFE4E1", //MistyRose
		"#FFE4B5", //Moccasin
		"#FFDEAD", //NavajoWhite
		"#000080", //Navy
		"#FDF5E6", //OldLace
		"#808000", //Olive
		"#6B8E23", //OliveDrab
		"#FFA500", //Orange
		"#FF4500", //OrangeRed
		"#DA70D6", //Orchid
		"#EEE8AA", //PaleGoldenRod
		"#98FB98", //PaleGreen
		"#AFEEEE", //PaleTurquoise
		"#DB7093", //PaleVioletRed
		"#FFEFD5", //PapayaWhip
		"#FFDAB9", //PeachPuff
		"#CD853F", //Peru
		"#FFC0CB", //Pink
		"#DDA0DD", //Plum
		"#B0E0E6", //PowderBlue
		"#800080", //Purple
		"#FF0000", //Red
		"#BC8F8F", //RosyBrown
		"#4169E1", //RoyalBlue
		"#8B4513", //SaddleBrown
		"#FA8072", //Salmon
		"#F4A460", //SandyBrown
		"#2E8B57", //SeaGreen
		"#FFF5EE", //SeaShell
		"#A0522D", //Sienna
		"#C0C0C0", //Silver
		"#87CEEB", //SkyBlue
		"#6A5ACD", //SlateBlue
		"#708090", //SlateGray
		"#FFFAFA", //Snow
		"#00FF7F", //SpringGreen
		"#4682B4", //SteelBlue
		"#D2B48C", //Tan
		"#008080", //Teal
		"#D8BFD8", //Thistle
		"#FF6347", //Tomato
		"#40E0D0", //Turquoise
		"#EE82EE", //Violet
		"#F5DEB3", //Wheat
		"#FFFFFF", //White
		"#F5F5F5", //WhiteSmoke
		"#FFFF00", //Yellow
		"#9ACD32" //YellowGreen
	};

	public static final String[] OUTLINESTYLE_VALUES = new String[] {
		"none",
		"dotted",
		"dashed",
		"solid",
		"double",
		"groove",
		"ridge",
		"inset",
		"outset"
	};

	public static final String[] OUTLINEWIDTH_VALUES = new String[] {
		"thin",
		"medium",
		"thick"
	};

	public static final String[] PADDINGTOP_VALUES = new String[] {
		"auto"
	};

	public static final String[] PADDINGRIGHT_VALUES = new String[] {
		"auto"
	};

	public static final String[] PADDINGBOTTOM_VALUES = new String[] {
		"auto"
	};

	public static final String[] PADDINGLEFT_VALUES = new String[] {
		"auto"
	};

	public static final String[] MARKS_VALUES = new String[] {
		"crop",
		"cross"
	};

	public static final String[] PAGEBREAKAFTER_VALUES = new String[] {
		"auto",
		"always",
		"avoid",
		"left",
		"right"
	};

	public static final String[] PAGEBREAKBEFORE_VALUES = new String[] {
		"auto",
		"always",
		"avoid",
		"left",
		"right"
	};

	public static final String[] PAGEBREAKINSIDE_VALUES = new String[] {
		"auto",
		"avoid"
	};

	public static final String[] SIZE_VALUES = new String[] {
		"auto",
		"landscape",
		"potrait"
	};

	public static final String[] BORDERCOLLAPSE_VALUES = new String[] {
		"collapse",
		"separate"
	};

	public static final String[] CAPTIONSIDE_VALUES = new String[] {
		"top",
		"bottom",
		"left",
		"right"
	};

	public static final String[] EMPTYCELLS_VALUES = new String[] {
		"show",
		"hide"
	};

	public static final String[] TABLELAYOUT_VALUES = new String[] {
		"auto",
		"fixed"
	};

	public static final String[] COLOR_VALUES = new String[] {
		"aqua",
		"black",
		"blue ",
		"fuchsia",
		"gray",
		"green",
		"lime",
		"maroon",
		"navy",
		"olive",
		"purple",
		"red",
		"silver",
		"teal",
		"white",
		"yellow"
	};

	public static final String[] DIRECTION_VALUES = new String[] {
		"ltr",
		"rtl"
	};

	public static final String[] LINEHEIGHT_VALUES = new String[] {
		"normal"
	};

	public static final String[] LETTERSPACING_VALUES = new String[] {
		"normal"
	};

	public static final String[] TEXTALIGN_VALUES = new String[] {
		"left",
		"right",
		"center",
		"justify"
	};

	public static final String[] TEXTDECORATION_VALUES = new String[] {
		"none",
		"underline",
		"overline",
		"line-through",
		"blink"
	};

	public static final String[] TEXTTRANSFORM_VALUES = new String[] {
		"none",
		"capitalize",
		"uppercase",
		"lowercase"
	};

	public static final String[] UNICODEBIDI_VALUES = new String[] {
		"normal",
		"embed",
		"bidi-override"
	};

	public static final String[] WHITESPACE_VALUES = new String[] {
		"normal",
		"pre",
		"nowrap"
	};

	public static final String[] WORDSPACING_VALUES = new String[] {
		"normal"
	};




	public Double getRelativeDistanceByElementId(int id) {
		Double distance = 0.0;
		Integer elementId = id;
		if (!this.relativeDistance.isEmpty()) {
			Map<Integer, Double> distanceMap = this.relativeDistance;
			Iterator<Entry<Integer, Double>> iterator = distanceMap.entrySet()
					.iterator();
			while (iterator.hasNext()) {
				Map.Entry<Integer, Double> pairs = (Map.Entry<Integer, Double>) iterator
						.next();
				if (elementId == pairs.getKey()) {
					distance = pairs.getValue();
					return distance;
				}
			}
		} else {
			//this.relativeDistance is empty
		}
		// Iterator has not found a match for the given element
		return distance;
	}

	public RuleElement(int id, String name, String type,
			String backgroundAttachment, String backgroundColor,
			String backgroundImage, String backgroundPosition,
			String backgroundRepeat, String borderTopColor,
			String borderTopStyle, String borderTopWidth,
			String borderRightColor, String borderRightStyle,
			String borderRightWidth, String borderBottomColor,
			String borderBottomStyle, String borderBottomWidth,
			String borderLeftColor, String borderLeftStyle,
			String borderLeftWidth, String borderColor, String borderStyle,
			String borderWidth, String clear, String cursor, String display,
			String positionFloat, String visibility, String top, String right,
			String bottom, String left, String position, String clip,
			String overflow, String verticalAlign, String zIndex,
			String height, String maxHeight, String minHeight, String width,
			String maxWidth, String minWidth, String fontFamily,
			String fontSize, String fontSizeAdjust, String fontStretch,
			String fontStyle, String fontVariant, String fontWeight,
			String content, String counterIncrement, String counterReset,
			String quotes, String listStyleType, String listStylePosition,
			String listStyleImage, String markerOffset, String marginTop,
			String marginRight, String marginBottom, String marginLeft,
			String outlineColor, String outlineStyle, String outlineWidth,
			String paddingTop, String paddingRight, String paddingBottom,
			String paddingLeft, String marks, String orphans, String page,
			String pageBreakAfter, String pageBreakBefore,
			String pageBreakInside, String size, String windows,
			String borderCollapse, String borderSpacing, String captionSide,
			String emptyCells, String tableLayout, String color,
			String direction, String lineHeight, String letterSpacing,
			String textAlign, String textDecoration, String textIndent,
			String textShadow, String textTransform, String unicodeBidi,
			String whiteSpace, String wordSpacing, String toolTipText,
			String text, Point absolutePosition,
			Map<Integer, Double> relativeDistance) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.backgroundAttachment = backgroundAttachment;
		this.backgroundColor = backgroundColor;
		this.backgroundImage = backgroundImage;
		this.backgroundPosition = backgroundPosition;
		this.backgroundRepeat = backgroundRepeat;
		this.borderTopColor = borderTopColor;
		this.borderTopStyle = borderTopStyle;
		this.borderTopWidth = borderTopWidth;
		this.borderRightColor = borderRightColor;
		this.borderRightStyle = borderRightStyle;
		this.borderRightWidth = borderRightWidth;
		this.borderBottomColor = borderBottomColor;
		this.borderBottomStyle = borderBottomStyle;
		this.borderBottomWidth = borderBottomWidth;
		this.borderLeftColor = borderLeftColor;
		this.borderLeftStyle = borderLeftStyle;
		this.borderLeftWidth = borderLeftWidth;
		this.borderColor = borderColor;
		this.borderStyle = borderStyle;
		this.borderWidth = borderWidth;
		this.clear = clear;
		this.cursor = cursor;
		this.display = display;
		this.positionFloat = positionFloat;
		this.visibility = visibility;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.left = left;
		this.position = position;
		this.clip = clip;
		this.overflow = overflow;
		this.verticalAlign = verticalAlign;
		this.zIndex = zIndex;
		this.height = height;
		this.maxHeight = maxHeight;
		this.minHeight = minHeight;
		this.width = width;
		this.maxWidth = maxWidth;
		this.minWidth = minWidth;
		this.fontFamily = fontFamily;
		this.fontSize = fontSize;
		this.fontSizeAdjust = fontSizeAdjust;
		this.fontStretch = fontStretch;
		this.fontStyle = fontStyle;
		this.fontVariant = fontVariant;
		this.fontWeight = fontWeight;
		this.content = content;
		this.counterIncrement = counterIncrement;
		this.counterReset = counterReset;
		this.quotes = quotes;
		this.listStyleType = listStyleType;
		this.listStylePosition = listStylePosition;
		this.listStyleImage = listStyleImage;
		this.markerOffset = markerOffset;
		this.marginTop = marginTop;
		this.marginRight = marginRight;
		this.marginBottom = marginBottom;
		this.marginLeft = marginLeft;
		this.outlineColor = outlineColor;
		this.outlineStyle = outlineStyle;
		this.outlineWidth = outlineWidth;
		this.paddingTop = paddingTop;
		this.paddingRight = paddingRight;
		this.paddingBottom = paddingBottom;
		this.paddingLeft = paddingLeft;
		this.marks = marks;
		this.orphans = orphans;
		this.page = page;
		this.pageBreakAfter = pageBreakAfter;
		this.pageBreakBefore = pageBreakBefore;
		this.pageBreakInside = pageBreakInside;
		this.size = size;
		this.windows = windows;
		this.borderCollapse = borderCollapse;
		this.borderSpacing = borderSpacing;
		this.captionSide = captionSide;
		this.emptyCells = emptyCells;
		this.tableLayout = tableLayout;
		this.color = color;
		this.direction = direction;
		this.lineHeight = lineHeight;
		this.letterSpacing = letterSpacing;
		this.textAlign = textAlign;
		this.textDecoration = textDecoration;
		this.textIndent = textIndent;
		this.textShadow = textShadow;
		this.textTransform = textTransform;
		this.unicodeBidi = unicodeBidi;
		this.whiteSpace = whiteSpace;
		this.wordSpacing = wordSpacing;
		this.toolTipText = toolTipText;
		this.text = text;
		this.absolutePosition = absolutePosition;
		this.relativeDistance = relativeDistance;
	}
	
	public RuleElement() {
		super();
		this.id = -1;
		this.name = "";
		this.type = "";
		this.backgroundAttachment = "";
		this.backgroundColor = "";
		this.backgroundImage = "";
		this.backgroundPosition = "";
		this.backgroundRepeat = "";
		this.borderTopColor = "";
		this.borderTopStyle = "";
		this.borderTopWidth = "";
		this.borderRightColor = "";
		this.borderRightStyle = "";
		this.borderRightWidth = "";
		this.borderBottomColor = "";
		this.borderBottomStyle = "";
		this.borderBottomWidth = "";
		this.borderLeftColor = "";
		this.borderLeftStyle = "";
		this.borderLeftWidth = "";
		this.borderColor = "";
		this.borderStyle = "";
		this.borderWidth = "";
		this.clear = "";
		this.cursor = "";
		this.display = "";
		this.positionFloat = "";
		this.visibility = "";
		this.top = "";
		this.right = "";
		this.bottom = "";
		this.left = "";
		this.position = "";
		this.clip = "";
		this.overflow = "";
		this.verticalAlign = "";
		this.zIndex = "";
		this.height = "";
		this.maxHeight = "";
		this.minHeight = "";
		this.width = "";
		this.maxWidth = "";
		this.minWidth = "";
		this.fontFamily = "";
		this.fontSize = "";
		this.fontSizeAdjust = "";
		this.fontStretch = "";
		this.fontStyle = "";
		this.fontVariant = "";
		this.fontWeight = "";
		this.content = "";
		this.counterIncrement = "";
		this.counterReset = "";
		this.quotes = "";
		this.listStyleType = "";
		this.listStylePosition = "";
		this.listStyleImage = "";
		this.markerOffset = "";
		this.marginTop = "";
		this.marginRight = "";
		this.marginBottom = "";
		this.marginLeft = "";
		this.outlineColor = "";
		this.outlineStyle = "";
		this.outlineWidth = "";
		this.paddingTop = "";
		this.paddingRight = "";
		this.paddingBottom = "";
		this.paddingLeft = "";
		this.marks = "";
		this.orphans = "";
		this.page = "";
		this.pageBreakAfter = "";
		this.pageBreakBefore = "";
		this.pageBreakInside = "";
		this.size = "";
		this.windows = "";
		this.borderCollapse = "";
		this.borderSpacing = "";
		this.captionSide = "";
		this.emptyCells = "";
		this.tableLayout = "";
		this.color = "";
		this.direction = "";
		this.lineHeight = "";
		this.letterSpacing = "";
		this.textAlign = "";
		this.textDecoration = "";
		this.textIndent = "";
		this.textShadow = "";
		this.textTransform = "";
		this.unicodeBidi = "";
		this.whiteSpace = "";
		this.wordSpacing = "";
		this.toolTipText = "";
		this.text = "";
		this.absolutePosition = null;
		this.relativeDistance = null;
	}
	
	public RuleElement(int id, String name, String type) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.backgroundAttachment = "";
		this.backgroundColor = "";
		this.backgroundImage = "";
		this.backgroundPosition = "";
		this.backgroundRepeat = "";
		this.borderTopColor = "";
		this.borderTopStyle = "";
		this.borderTopWidth = "";
		this.borderRightColor = "";
		this.borderRightStyle = "";
		this.borderRightWidth = "";
		this.borderBottomColor = "";
		this.borderBottomStyle = "";
		this.borderBottomWidth = "";
		this.borderLeftColor = "";
		this.borderLeftStyle = "";
		this.borderLeftWidth = "";
		this.borderColor = "";
		this.borderStyle = "";
		this.borderWidth = "";
		this.clear = "";
		this.cursor = "";
		this.display = "";
		this.positionFloat = "";
		this.visibility = "";
		this.top = "";
		this.right = "";
		this.bottom = "";
		this.left = "";
		this.position = "";
		this.clip = "";
		this.overflow = "";
		this.verticalAlign = "";
		this.zIndex = "";
		this.height = "";
		this.maxHeight = "";
		this.minHeight = "";
		this.width = "";
		this.maxWidth = "";
		this.minWidth = "";
		this.fontFamily = "";
		this.fontSize = "";
		this.fontSizeAdjust = "";
		this.fontStretch = "";
		this.fontStyle = "";
		this.fontVariant = "";
		this.fontWeight = "";
		this.content = "";
		this.counterIncrement = "";
		this.counterReset = "";
		this.quotes = "";
		this.listStyleType = "";
		this.listStylePosition = "";
		this.listStyleImage = "";
		this.markerOffset = "";
		this.marginTop = "";
		this.marginRight = "";
		this.marginBottom = "";
		this.marginLeft = "";
		this.outlineColor = "";
		this.outlineStyle = "";
		this.outlineWidth = "";
		this.paddingTop = "";
		this.paddingRight = "";
		this.paddingBottom = "";
		this.paddingLeft = "";
		this.marks = "";
		this.orphans = "";
		this.page = "";
		this.pageBreakAfter = "";
		this.pageBreakBefore = "";
		this.pageBreakInside = "";
		this.size = "";
		this.windows = "";
		this.borderCollapse = "";
		this.borderSpacing = "";
		this.captionSide = "";
		this.emptyCells = "";
		this.tableLayout = "";
		this.color = "";
		this.direction = "";
		this.lineHeight = "";
		this.letterSpacing = "";
		this.textAlign = "";
		this.textDecoration = "";
		this.textIndent = "";
		this.textShadow = "";
		this.textTransform = "";
		this.unicodeBidi = "";
		this.whiteSpace = "";
		this.wordSpacing = "";
		this.toolTipText = "";
		this.text = "";
		this.absolutePosition = null;
		this.relativeDistance = null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBackgroundAttachment() {
		return backgroundAttachment;
	}

	public void setBackgroundAttachment(String backgroundAttachment) {
		this.backgroundAttachment = backgroundAttachment;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public String getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public String getBackgroundPosition() {
		return backgroundPosition;
	}

	public void setBackgroundPosition(String backgroundPosition) {
		this.backgroundPosition = backgroundPosition;
	}

	public String getBackgroundRepeat() {
		return backgroundRepeat;
	}

	public void setBackgroundRepeat(String backgroundRepeat) {
		this.backgroundRepeat = backgroundRepeat;
	}

	public String getBorderTopColor() {
		return borderTopColor;
	}

	public void setBorderTopColor(String borderTopColor) {
		this.borderTopColor = borderTopColor;
	}

	public String getBorderTopStyle() {
		return borderTopStyle;
	}

	public void setBorderTopStyle(String borderTopStyle) {
		this.borderTopStyle = borderTopStyle;
	}

	public String getBorderTopWidth() {
		return borderTopWidth;
	}

	public void setBorderTopWidth(String borderTopWidth) {
		this.borderTopWidth = borderTopWidth;
	}

	public String getBorderRightColor() {
		return borderRightColor;
	}

	public void setBorderRightColor(String borderRightColor) {
		this.borderRightColor = borderRightColor;
	}

	public String getBorderRightStyle() {
		return borderRightStyle;
	}

	public void setBorderRightStyle(String borderRightStyle) {
		this.borderRightStyle = borderRightStyle;
	}

	public String getBorderRightWidth() {
		return borderRightWidth;
	}

	public void setBorderRightWidth(String borderRightWidth) {
		this.borderRightWidth = borderRightWidth;
	}

	public String getBorderBottomColor() {
		return borderBottomColor;
	}

	public void setBorderBottomColor(String borderBottomColor) {
		this.borderBottomColor = borderBottomColor;
	}

	public String getBorderBottomStyle() {
		return borderBottomStyle;
	}

	public void setBorderBottomStyle(String borderBottomStyle) {
		this.borderBottomStyle = borderBottomStyle;
	}

	public String getBorderBottomWidth() {
		return borderBottomWidth;
	}

	public void setBorderBottomWidth(String borderBottomWidth) {
		this.borderBottomWidth = borderBottomWidth;
	}

	public String getBorderLeftColor() {
		return borderLeftColor;
	}

	public void setBorderLeftColor(String borderLeftColor) {
		this.borderLeftColor = borderLeftColor;
	}

	public String getBorderLeftStyle() {
		return borderLeftStyle;
	}

	public void setBorderLeftStyle(String borderLeftStyle) {
		this.borderLeftStyle = borderLeftStyle;
	}

	public String getBorderLeftWidth() {
		return borderLeftWidth;
	}

	public void setBorderLeftWidth(String borderLeftWidth) {
		this.borderLeftWidth = borderLeftWidth;
	}

	public String getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}

	public String getBorderStyle() {
		return borderStyle;
	}

	public void setBorderStyle(String borderStyle) {
		this.borderStyle = borderStyle;
	}

	public String getBorderWidth() {
		return borderWidth;
	}

	public void setBorderWidth(String borderWidth) {
		this.borderWidth = borderWidth;
	}

	public String getClear() {
		return clear;
	}

	public void setClear(String clear) {
		this.clear = clear;
	}

	public String getCursor() {
		return cursor;
	}

	public void setCursor(String cursor) {
		this.cursor = cursor;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getPositionFloat() {
		return positionFloat;
	}

	public void setPositionFloat(String positionFloat) {
		this.positionFloat = positionFloat;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getTop() {
		return top;
	}

	public void setTop(String top) {
		this.top = top;
	}

	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public String getBottom() {
		return bottom;
	}

	public void setBottom(String bottom) {
		this.bottom = bottom;
	}

	public String getLeft() {
		return left;
	}

	public void setLeft(String left) {
		this.left = left;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getClip() {
		return clip;
	}

	public void setClip(String clip) {
		this.clip = clip;
	}

	public String getOverflow() {
		return overflow;
	}

	public void setOverflow(String overflow) {
		this.overflow = overflow;
	}

	public String getVerticalAlign() {
		return verticalAlign;
	}

	public void setVerticalAlign(String verticalAlign) {
		this.verticalAlign = verticalAlign;
	}

	public String getzIndex() {
		return zIndex;
	}

	public void setzIndex(String zIndex) {
		this.zIndex = zIndex;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(String maxHeight) {
		this.maxHeight = maxHeight;
	}

	public String getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(String minHeight) {
		this.minHeight = minHeight;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getMaxWidth() {
		return maxWidth;
	}

	public void setMaxWidth(String maxWidth) {
		this.maxWidth = maxWidth;
	}

	public String getMinWidth() {
		return minWidth;
	}

	public void setMinWidth(String minWidth) {
		this.minWidth = minWidth;
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public String getFontSize() {
		return fontSize;
	}

	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}

	public String getFontSizeAdjust() {
		return fontSizeAdjust;
	}

	public void setFontSizeAdjust(String fontSizeAdjust) {
		this.fontSizeAdjust = fontSizeAdjust;
	}

	public String getFontStretch() {
		return fontStretch;
	}

	public void setFontStretch(String fontStretch) {
		this.fontStretch = fontStretch;
	}

	public String getFontStyle() {
		return fontStyle;
	}

	public void setFontStyle(String fontStyle) {
		this.fontStyle = fontStyle;
	}

	public String getFontVariant() {
		return fontVariant;
	}

	public void setFontVariant(String fontVariant) {
		this.fontVariant = fontVariant;
	}

	public String getFontWeight() {
		return fontWeight;
	}

	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCounterIncrement() {
		return counterIncrement;
	}

	public void setCounterIncrement(String counterIncrement) {
		this.counterIncrement = counterIncrement;
	}

	public String getCounterReset() {
		return counterReset;
	}

	public void setCounterReset(String counterReset) {
		this.counterReset = counterReset;
	}

	public String getQuotes() {
		return quotes;
	}

	public void setQuotes(String quotes) {
		this.quotes = quotes;
	}

	public String getListStyleType() {
		return listStyleType;
	}

	public void setListStyleType(String listStyleType) {
		this.listStyleType = listStyleType;
	}

	public String getListStylePosition() {
		return listStylePosition;
	}

	public void setListStylePosition(String listStylePosition) {
		this.listStylePosition = listStylePosition;
	}

	public String getListStyleImage() {
		return listStyleImage;
	}

	public void setListStyleImage(String listStyleImage) {
		this.listStyleImage = listStyleImage;
	}

	public String getMarkerOffset() {
		return markerOffset;
	}

	public void setMarkerOffset(String markerOffset) {
		this.markerOffset = markerOffset;
	}

	public String getMarginTop() {
		return marginTop;
	}

	public void setMarginTop(String marginTop) {
		this.marginTop = marginTop;
	}

	public String getMarginRight() {
		return marginRight;
	}

	public void setMarginRight(String marginRight) {
		this.marginRight = marginRight;
	}

	public String getMarginBottom() {
		return marginBottom;
	}

	public void setMarginBottom(String marginBottom) {
		this.marginBottom = marginBottom;
	}

	public String getMarginLeft() {
		return marginLeft;
	}

	public void setMarginLeft(String marginLeft) {
		this.marginLeft = marginLeft;
	}

	public String getOutlineColor() {
		return outlineColor;
	}

	public void setOutlineColor(String outlineColor) {
		this.outlineColor = outlineColor;
	}

	public String getOutlineStyle() {
		return outlineStyle;
	}

	public void setOutlineStyle(String outlineStyle) {
		this.outlineStyle = outlineStyle;
	}

	public String getOutlineWidth() {
		return outlineWidth;
	}

	public void setOutlineWidth(String outlineWidth) {
		this.outlineWidth = outlineWidth;
	}

	public String getPaddingTop() {
		return paddingTop;
	}

	public void setPaddingTop(String paddingTop) {
		this.paddingTop = paddingTop;
	}

	public String getPaddingRight() {
		return paddingRight;
	}

	public void setPaddingRight(String paddingRight) {
		this.paddingRight = paddingRight;
	}

	public String getPaddingBottom() {
		return paddingBottom;
	}

	public void setPaddingBottom(String paddingBottom) {
		this.paddingBottom = paddingBottom;
	}

	public String getPaddingLeft() {
		return paddingLeft;
	}

	public void setPaddingLeft(String paddingLeft) {
		this.paddingLeft = paddingLeft;
	}

	public String getMarks() {
		return marks;
	}

	public void setMarks(String marks) {
		this.marks = marks;
	}

	public String getOrphans() {
		return orphans;
	}

	public void setOrphans(String orphans) {
		this.orphans = orphans;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getPageBreakAfter() {
		return pageBreakAfter;
	}

	public void setPageBreakAfter(String pageBreakAfter) {
		this.pageBreakAfter = pageBreakAfter;
	}

	public String getPageBreakBefore() {
		return pageBreakBefore;
	}

	public void setPageBreakBefore(String pageBreakBefore) {
		this.pageBreakBefore = pageBreakBefore;
	}

	public String getPageBreakInside() {
		return pageBreakInside;
	}

	public void setPageBreakInside(String pageBreakInside) {
		this.pageBreakInside = pageBreakInside;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getWindows() {
		return windows;
	}

	public void setWindows(String windows) {
		this.windows = windows;
	}

	public String getBorderCollapse() {
		return borderCollapse;
	}

	public void setBorderCollapse(String borderCollapse) {
		this.borderCollapse = borderCollapse;
	}

	public String getBorderSpacing() {
		return borderSpacing;
	}

	public void setBorderSpacing(String borderSpacing) {
		this.borderSpacing = borderSpacing;
	}

	public String getCaptionSide() {
		return captionSide;
	}

	public void setCaptionSide(String captionSide) {
		this.captionSide = captionSide;
	}

	public String getEmptyCells() {
		return emptyCells;
	}

	public void setEmptyCells(String emptyCells) {
		this.emptyCells = emptyCells;
	}

	public String getTableLayout() {
		return tableLayout;
	}

	public void setTableLayout(String tableLayout) {
		this.tableLayout = tableLayout;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getLineHeight() {
		return lineHeight;
	}

	public void setLineHeight(String lineHeight) {
		this.lineHeight = lineHeight;
	}

	public String getLetterSpacing() {
		return letterSpacing;
	}

	public void setLetterSpacing(String letterSpacing) {
		this.letterSpacing = letterSpacing;
	}

	public String getTextAlign() {
		return textAlign;
	}

	public void setTextAlign(String textAlign) {
		this.textAlign = textAlign;
	}

	public String getTextDecoration() {
		return textDecoration;
	}

	public void setTextDecoration(String textDecoration) {
		this.textDecoration = textDecoration;
	}

	public String getTextIndent() {
		return textIndent;
	}

	public void setTextIndent(String textIndent) {
		this.textIndent = textIndent;
	}

	public String getTextShadow() {
		return textShadow;
	}

	public void setTextShadow(String textShadow) {
		this.textShadow = textShadow;
	}

	public String getTextTransform() {
		return textTransform;
	}

	public void setTextTransform(String textTransform) {
		this.textTransform = textTransform;
	}

	public String getUnicodeBidi() {
		return unicodeBidi;
	}

	public void setUnicodeBidi(String unicodeBidi) {
		this.unicodeBidi = unicodeBidi;
	}

	public String getWhiteSpace() {
		return whiteSpace;
	}

	public void setWhiteSpace(String whiteSpace) {
		this.whiteSpace = whiteSpace;
	}

	public String getWordSpacing() {
		return wordSpacing;
	}

	public void setWordSpacing(String wordSpacing) {
		this.wordSpacing = wordSpacing;
	}

	public String getToolTipText() {
		return toolTipText;
	}

	public void setToolTipText(String toolTipText) {
		this.toolTipText = toolTipText;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Point getAbsolutePosition() {
		return absolutePosition;
	}

	public void setAbsolutePosition(Point absolutePosition) {
		this.absolutePosition = absolutePosition;
	}

	public Map<Integer, Double> getRelativeDistance() {
		return relativeDistance;
	}

	public void setRelativeDistance(Map<Integer, Double> relativeDistance) {
		this.relativeDistance = relativeDistance;
	}
	
	
}
