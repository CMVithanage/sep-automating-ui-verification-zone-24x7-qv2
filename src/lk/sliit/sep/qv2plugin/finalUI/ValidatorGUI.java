package lk.sliit.sep.qv2plugin.finalUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.executionEngine.MultiElementExecutor;
import lk.sliit.sep.qv2plugin.utilities.OSValidator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import de.javasoft.plaf.synthetica.SyntheticaBlueLightLookAndFeel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TwiZtor
 */
public class ValidatorGUI extends javax.swing.JFrame {

	private Vector<RuleElement> ruleElements;
	private Vector<Vector<String>> selectedWebElements;
	
	private WebDriver driver;
	private String url;
	
	private WebElementManagerGUI webElementsManGui;
	
	private Vector<String> ruleWebElementsTableHeader;
	private Vector<String> selectedWebElementsTableHeader;
	private Vector<Vector<String>> ruleWebElementsTableContent;
	private Vector<Vector<String>> selectedWebElementsTableContent;
	
	private HashMap<RuleElement, Vector<Vector<String>>> elementMap;
	
    /**
     * Creates new form ValidatorGUI
     */
    public ValidatorGUI() {
		try {
			UIManager.setLookAndFeel(new SyntheticaBlueLightLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
		}
		initComponents();
		
		this.ruleElements = new Vector<RuleElement>();
		this.selectedWebElements = new Vector<Vector<String>>();
		
		this.webElementsManGui = null;
		
		this.elementMap = new HashMap<RuleElement, Vector<Vector<String>>>();
		
		//Set Selected Element table header to Type, Name, XPath
		this.selectedWebElementsTableContent = new Vector<Vector<String>>();
		
		this.selectedWebElementsTableHeader = new Vector<String>();
		this.selectedWebElementsTableHeader.add("Type");
		this.selectedWebElementsTableHeader.add("Name");
		this.selectedWebElementsTableHeader.add("XPath");
		
		this.ruleWebElementsTableContent = new Vector<Vector<String>>();
		
		this.ruleWebElementsTableHeader = new Vector<String>();
		this.ruleWebElementsTableHeader.add("Type");
		this.ruleWebElementsTableHeader.add("Name");
		this.ruleWebElementsTableHeader.add("XPath");
		
		/*
		 * Test Inputs
		 */
		
		// Test Inputs Initialization
		RuleElement re1 = new RuleElement(123, "abc", "button");
		re1.setBackgroundAttachment("fixed");
		re1.setBackgroundColor("black");
		RuleElement re2 = new RuleElement(123, "def", "input");
		re1.setBackgroundAttachment("scroll");
		re1.setBackgroundColor("white");
		Vector<RuleElement> rev = new Vector<RuleElement>();
		rev.add(re1);
		rev.add(re2);
		this.ruleElements = rev;

		Vector<String> sel1 = new Vector<String>();
		sel1.add("button");
		sel1.add("button1");
		sel1.add("xPath");
		sel1.add("2");
		sel1.add("weqfdg");
		Vector<String> sel2 = new Vector<String>();
		sel2.add("input");
		sel2.add("input1");
		sel2.add("xPath");
		sel2.add("6");
		sel2.add("rhjefgh");
		Vector<Vector<String>> selvec = new Vector<Vector<String>>();
		selvec.add(sel1);
		selvec.add(sel2);
		this.selectedWebElements = selvec;
		
		//Test Driver Initialization
		this.url = "http://www.google.com";
        String driverPath = "";        
        
        //Validate OS
		try {
			if (OSValidator.isMac()) {
				driverPath = new File("./Drivers/OSXDriver/chromedriver")
						.getCanonicalPath(); // For OSX
			} else if (OSValidator.isWindows()) {
				driverPath = new File("./Drivers/WinDriver/chromedriver.exe")
						.getCanonicalPath(); // For Windows
			} else {
				JOptionPane.showMessageDialog(null,
						"Selenium Webdriver loading failed!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		System.setProperty("webdriver.chrome.driver", driverPath);
    	this.driver = new ChromeDriver();
    	this.driver.get(url);
    	
    	/*
    	 * End of Test Inputs
    	 */
		
		loadTable();
		refreshTables();
		initializeRuleElementCombo();
		
		ruleNameCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				//Fire Event
				refreshTables();
			}
		});
		
		
    }
    
    public ValidatorGUI(Vector<RuleElement> ruleElements, Vector<Vector<String>> selectedWebElements, WebDriver driver, String url, WebElementManagerGUI webGui) {
		try {
			UIManager.setLookAndFeel(new SyntheticaBlueLightLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
		}
		initComponents();
		
		this.ruleElements = ruleElements;
		this.selectedWebElements = selectedWebElements;
		
		this.driver = driver;
		this.url = url;
		
		this.webElementsManGui = webGui;
		
		this.elementMap = new HashMap<RuleElement, Vector<Vector<String>>>();

		//Set Selected Element table header to Type, Name, XPath
		this.selectedWebElementsTableContent = new Vector<Vector<String>>();
		
		this.selectedWebElementsTableHeader = new Vector<String>();
		this.selectedWebElementsTableHeader.add("Type");
		this.selectedWebElementsTableHeader.add("Name");
		this.selectedWebElementsTableHeader.add("XPath");
		
		this.ruleWebElementsTableContent = new Vector<Vector<String>>();
		
		this.ruleWebElementsTableHeader = new Vector<String>();
		this.ruleWebElementsTableHeader.add("Type");
		this.ruleWebElementsTableHeader.add("Name");
		this.ruleWebElementsTableHeader.add("XPath");

		loadTable();
		refreshTables();
		initializeRuleElementCombo();
		
		ruleNameCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				//Fire Event
				refreshTables();
			}
		});
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Validator_label = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        selectedWebElementsTable = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        ruleElementWebElementTable = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        ruleNameCombo = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        addWebElementToRuleElementButton = new javax.swing.JButton();
        deleteWebElementFromRuleElementButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        validateButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Validator_label.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        Validator_label.setText("Validator");

        jPanel11.setBackground(new java.awt.Color(250, 249, 249));
        jPanel11.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        selectedWebElementsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "Rule Name", "Type"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        selectedWebElementsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                selectedWebElementsTableMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(selectedWebElementsTable);

        jPanel12.setBackground(new java.awt.Color(250, 249, 249));
        jPanel12.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));

        ruleElementWebElementTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Attribute", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        ruleElementWebElementTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ruleElementWebElementTableMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(ruleElementWebElementTable);

        jLabel14.setText("Rule :");

        ruleNameCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ruleNameCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ruleNameComboActionPerformed(evt);
            }
        });

        jLabel16.setText("Add XPaths to Rule Elements");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ruleNameCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel16)
                        .addGap(0, 129, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(ruleNameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );

        addWebElementToRuleElementButton.setText("Add");
        addWebElementToRuleElementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addWebElementToRuleElementButtonActionPerformed(evt);
            }
        });

        deleteWebElementFromRuleElementButton.setText("Delete");
        deleteWebElementFromRuleElementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteWebElementFromRuleElementButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Selected Elements");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(addWebElementToRuleElementButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(deleteWebElementFromRuleElementButton)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(addWebElementToRuleElementButton)
                        .addGap(18, 18, 18)
                        .addComponent(deleteWebElementFromRuleElementButton))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        validateButton.setText("Validate");
        validateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateButtonActionPerformed(evt);
            }
        });
        
        backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		backButtonActionPerformed(e);
        	}
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(Validator_label))
        		.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        			.addGroup(jPanel1Layout.createSequentialGroup()
        				.addComponent(backButton, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
        				.addPreferredGap(ComponentPlacement.RELATED)
        				.addComponent(validateButton))
        			.addComponent(jPanel11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(Validator_label)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addComponent(jPanel11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(validateButton)
        				.addComponent(backButton))
        			.addContainerGap())
        );
        jPanel1.setLayout(jPanel1Layout);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>
    
    /*
     * Modified Code
     */
    
    
    //Custom Methods
    private void loadTable(){
    	ruleElementWebElementTable.removeAll();
    	ruleElementWebElementTable.setModel(new javax.swing.table.DefaultTableModel(
    			ruleWebElementsTableContent, ruleWebElementsTableHeader));
		jScrollPane5.setViewportView(ruleElementWebElementTable);

		selectedWebElementsTable.removeAll();
		selectedWebElementsTable
				.setModel(new javax.swing.table.DefaultTableModel(
						selectedWebElementsTableContent,
						selectedWebElementsTableHeader));
		jScrollPane4.setViewportView(selectedWebElementsTable);
    }
    
    private void refreshTables(){
    	if (!this.elementMap.isEmpty()) {
			HashMap<RuleElement, Vector<Vector<String>>> map = this.elementMap;
			int selectedRuleElementIndex = ruleNameCombo.getSelectedIndex();
			RuleElement re = this.ruleElements.get(selectedRuleElementIndex);
			Vector<Vector<String>> ruleWebElementVector = map.get(re);//throws exception
			this.ruleWebElementsTableContent.removeAllElements();
			if (ruleWebElementVector != null) {
				for (Vector<String> el : ruleWebElementVector) {
					Vector<String> tel = new Vector<String>();
					tel.add(el.get(0));
					tel.add(el.get(1));
					tel.add(el.get(2));
					this.ruleWebElementsTableContent.add(tel);
				}
			}
		}
    	
		if (!this.selectedWebElements.isEmpty()) {
			Vector<Vector<String>> sel = this.selectedWebElements;
			this.selectedWebElementsTableContent.removeAllElements();
			for (Vector<String> els : sel) {
				Vector<String> tels = new Vector<String>();
				tels.add(els.get(0));
				tels.add(els.get(1));
				tels.add(els.get(2));
				this.selectedWebElementsTableContent.add(tels);
			}
		} else {
			JOptionPane.showMessageDialog(null, "Please select at least one Web Element to Validate...");
		}
		loadTable();
    }
    
    private void initializeRuleElementCombo(){
    	Vector<RuleElement> rules = this.ruleElements;    	
    	if(rules.isEmpty()){
    		JOptionPane.showMessageDialog(this, "Loading RuleElements failed: No RuleElements defined!");
    		return;
    	}
    	
    	ruleNameCombo.removeAllItems();    	
    	for(RuleElement rule : rules){
    		ruleNameCombo.addItem(rule.getName());
    	}
    }

    //Action Handler Methods
    private void selectedWebElementsTableMouseClicked(java.awt.event.MouseEvent evt) {                                                      
        // TODO add your handling code here:
    }                                                     

    private void ruleElementWebElementTableMouseClicked(java.awt.event.MouseEvent evt) {                                                        
        // TODO add your handling code here:
    }                                                       

    private void ruleNameComboActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void validateButtonActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
    	HashMap<String, String> resultMap = new HashMap<String, String>();
    	HashMap<RuleElement, Vector<Vector<String>>> ruleWebElementMap = this.elementMap;
    	HashMap<RuleElement, Vector<String>> formattedElementMap = new HashMap<RuleElement, Vector<String>>();
    	
    	if (ruleWebElementMap == null || ruleWebElementMap.isEmpty()||ruleWebElementMap.equals("")){
    		JOptionPane.showMessageDialog(this, "Select Rule to Be Validated!");
    	}
    	Iterator it = ruleWebElementMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            
            RuleElement ruleElement = (RuleElement) pair.getKey();            
            if (ruleElement != null) {
				Vector<Vector<String>> webElementVector = (Vector<Vector<String>>) pair
						.getValue();
				if (webElementVector != null) {
					for (Vector<String> webElement : webElementVector) {
						if (formattedElementMap.get(ruleElement) != null) {
							Vector<String> xPathVector = formattedElementMap
									.get(ruleElement);
							xPathVector.add(webElement.get(2));
							formattedElementMap.put(ruleElement, xPathVector);
						} else {
							Vector<String> xPathVector = new Vector<String>();
							xPathVector.add(webElement.get(2));
							formattedElementMap.put(ruleElement, xPathVector);
						}
					}
				} else {
//					JOptionPane.showMessageDialog(this, "Validation Failed: No WebElements to validate!");
				}
			} else {
//				JOptionPane.showMessageDialog(this, "Validation Failed: No RuleElements to validate!");
			}
			it.remove(); 
        }
        
        resultMap = MultiElementExecutor.excecute(formattedElementMap, this.driver, this.url);
        if(!resultMap.isEmpty()){
        	
//        	System.out.println("Hell Yeah!");
        	
        	ReportGUI rgui = new ReportGUI(resultMap, driver, this);
    		rgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		rgui.setLocationRelativeTo(null);
    		rgui.setVisible(true);
    		
        	this.setVisible(false);
        	this.dispose();
        }
    }
    
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt){
    	int response = JOptionPane.showConfirmDialog(
    		    this,
    		    "Would you like to go back? All your current changes will be lost.",
    		    "Alert",
    		    JOptionPane.YES_NO_OPTION);
		if (response == 0) {
			webElementsManGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			webElementsManGui.setLocationRelativeTo(null);
			webElementsManGui.validate();
			webElementsManGui.repaint();
			webElementsManGui.setVisible(true);
			this.dispose();
		}
    }

    private void addWebElementToRuleElementButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                                 
        // TODO add your handling code here:
    	int selectedElementIndex = selectedWebElementsTable.getSelectedRow();
    	Vector<String> selectedElement = this.selectedWebElements.get(selectedElementIndex);
    	
    	int ruleElementIndex = ruleNameCombo.getSelectedIndex();
    	RuleElement ruleElement = this.ruleElements.get(ruleElementIndex);
    	
//    	selectedElement.add(Integer.toString(selectedElementIndex));
    	
    	if (this.elementMap.containsKey(ruleElement)) {
			Vector<Vector<String>> elementVector = this.elementMap
					.get(ruleElement);
			if (elementVector != null) {
				elementVector.add(selectedElement);
			} else {
				elementVector = new Vector<Vector<String>>();
				elementVector.add(selectedElement);
			}
			this.elementMap.put(ruleElement, elementVector);
		} else {
			Vector<Vector<String>> elementVector = new Vector<Vector<String>>();
			elementVector.add(selectedElement);
			this.elementMap.put(ruleElement, elementVector);
		}
		refreshTables();
    }                                                                

    private void deleteWebElementFromRuleElementButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                                      
        // TODO add your handling code here:
    	int ruleWebElementIndex = ruleElementWebElementTable.getSelectedRow();
    	
    	int ruleElementIndex = ruleNameCombo.getSelectedIndex();
    	RuleElement ruleElement = this.ruleElements.get(ruleElementIndex);
    	
    	Vector<Vector<String>> ruleWebElementVector = this.elementMap.get(ruleElement);
//    	Vector<String> selectedElement = ruleWebElementVector.get(ruleWebElementIndex);
    	ruleWebElementVector.remove(ruleWebElementIndex);
    	this.elementMap.put(ruleElement, ruleWebElementVector);
    	
//    	int selectedIndex = Integer.parseInt(selectedElement.lastElement());
//    	selectedElement.remove(5);
    	
//    	this.selectedWebElements.add(selectedIndex, selectedElement);
    	
    	refreshTables();
    }                                                                     

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ValidatorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ValidatorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ValidatorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ValidatorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	ValidatorGUI vgui = new ValidatorGUI();
        		vgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        		vgui.setLocationRelativeTo(null);
        		vgui.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JLabel Validator_label;
    private javax.swing.JButton addWebElementToRuleElementButton;
    private javax.swing.JButton deleteWebElementFromRuleElementButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable ruleElementWebElementTable;
    private javax.swing.JComboBox ruleNameCombo;
    private javax.swing.JTable selectedWebElementsTable;
    private javax.swing.JButton validateButton;
    private JButton backButton;
    // End of variables declaration                   
}
