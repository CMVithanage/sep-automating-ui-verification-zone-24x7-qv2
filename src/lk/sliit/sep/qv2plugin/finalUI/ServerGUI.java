/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.sliit.sep.qv2plugin.finalUI;

import javax.swing.UIManager;

import de.javasoft.plaf.synthetica.SyntheticaBlueLightLookAndFeel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 *
 * @author TwiZtor
 */
public class ServerGUI extends javax.swing.JFrame {

    /**
     * Creates new form ServerUI
     */
    public ServerGUI() {
    	try {
			UIManager.setLookAndFeel(new SyntheticaBlueLightLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
		}
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnServerStart = new javax.swing.JButton();
        lblServerStatus = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnServerStart.setText("Start Server");

        lblServerStatus.setForeground(new java.awt.Color(255, 0, 0));
        lblServerStatus.setText("Server is not running");

        jLabel3.setText("interact with the AutoUIV engine using the AutoUIV web application");

        jLabel2.setText("AutoUIV Server enables you to ");

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel1.setText("AutoUIV Server");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 473, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(7, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().setLayout(layout);
        GroupLayout gl_jPanel1 = new GroupLayout(jPanel1);
        gl_jPanel1.setHorizontalGroup(
        	gl_jPanel1.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_jPanel1.createSequentialGroup()
        			.addGap(19)
        			.addComponent(jLabel1)
        			.addContainerGap(192, Short.MAX_VALUE))
        		.addGroup(Alignment.TRAILING, gl_jPanel1.createSequentialGroup()
        			.addGap(36)
        			.addGroup(gl_jPanel1.createParallelGroup(Alignment.LEADING)
        				.addGroup(gl_jPanel1.createParallelGroup(Alignment.LEADING)
        					.addGroup(gl_jPanel1.createSequentialGroup()
        						.addGap(113)
        						.addComponent(jLabel2))
        					.addComponent(jLabel3))
        				.addGroup(gl_jPanel1.createSequentialGroup()
        					.addGap(139)
        					.addGroup(gl_jPanel1.createParallelGroup(Alignment.LEADING)
        						.addComponent(lblServerStatus)
        						.addGroup(gl_jPanel1.createSequentialGroup()
        							.addGap(8)
        							.addComponent(btnServerStart)))))
        			.addContainerGap(12, Short.MAX_VALUE))
        );
        gl_jPanel1.setVerticalGroup(
        	gl_jPanel1.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_jPanel1.createSequentialGroup()
        			.addGap(16)
        			.addComponent(jLabel1)
        			.addGap(46)
        			.addComponent(jLabel2)
        			.addGap(6)
        			.addComponent(jLabel3)
        			.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
        			.addComponent(lblServerStatus)
        			.addGap(44)
        			.addComponent(btnServerStart)
        			.addGap(13))
        );
        jPanel1.setLayout(gl_jPanel1);

        pack();
    }// </editor-fold>                        

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ServerGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblServerStatus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton btnServerStart;
    // End of variables declaration                   
}
