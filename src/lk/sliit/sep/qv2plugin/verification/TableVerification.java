package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class TableVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public TableVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		elementName = ruleElement.getName();
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	/*
	 * table properties
	 */

	public String verifyTableBorderCollapse(/*String borderCollapse*/) throws UndefinedException{

		String borderCollapse = ruleElement.getBorderCollapse().trim().toLowerCase();
		//String correct = false;

		String cssBorderCollapse = element.getCssValue("border-collapse");
		
		if (cssBorderCollapse == null){
			throw new UndefinedException("Table Border Collapse Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssBorderCollapse.equals(borderCollapse)) {
			return passed;
		}

		return failed;
	}

	public String verifyTableBorderSpacingOneVal(/*String borderSpacing*/) throws UndefinedException{

		String borderSpacing = ruleElement.getBorderSpacing().trim().toLowerCase();
		//String correct = false;

		String cssBorderSpacing = element.getCssValue("border-spacing");
		
		if (cssBorderSpacing == null){
			throw new UndefinedException("Table Border Spacing Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssBorderSpacing.equals(borderSpacing)) {
			return passed;
		}

		return failed;
	}

	public String verifyTableBorderSpacingTwoVal(String borderSpacingH,
			String borderSpacingV) {			//CHECK

		//String correct = false;

		String[] cssBorderSpacing = element.getCssValue("border-spacing")
				.split(" ");

		if (cssBorderSpacing[0].equals(borderSpacingH)
				|| cssBorderSpacing[1].equals(borderSpacingV)) {
			return passed;
		}

		return failed;
	}

	public String verifyTableCaption(/*String tableCaption*/) throws UndefinedException{

		String tableCaption = ruleElement.getCaptionSide().trim();
		//String correct = false;

		String cssTableCaption = element.getCssValue("caption-side");
		

		if (cssTableCaption == null){
			throw new UndefinedException("Table Caption Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTableCaption.equals(tableCaption)) {
			return passed;
		}

		return failed;
	}

	public String verifyTableEmptyCells(/*String emptyCells*/) throws UndefinedException{

		String emptyCells = ruleElement.getEmptyCells().trim().toLowerCase();
		//String correct = false;

		String cssTableEmptyCells = element.getCssValue("empty-cells");
		
		if (cssTableEmptyCells == null){
			throw new UndefinedException("Table Empty Cells Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTableEmptyCells.equals(emptyCells)) {
			return passed;
		}

		return failed;
	}

	public String verifyTableLayout(/*String layout*/) throws UndefinedException{

		String layout = ruleElement.getTableLayout().trim().toLowerCase();
		//String correct = false;

		String cssTableLayout = element.getCssValue("table-layout");
		
		if (cssTableLayout == null){
			throw new UndefinedException("Table Layout Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTableLayout.equals(layout)) {
			return passed;
		}

		return failed;
	}

	/*
	 * end of table properties
	 */

}
