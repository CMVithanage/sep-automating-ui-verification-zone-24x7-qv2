package lk.sliit.sep.qv2plugin.verification;

import java.awt.Color;
import java.math.BigDecimal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.*;

public class VerifyElement {
	
	public WebElement element;
	public WebDriver driver;
	public RuleElement ruleElement;
	public String xpath;
	
	public VerifyElement(String xpath, WebDriver driver, RuleElement ruleElement){	
		this.xpath = xpath;
		this.driver = driver;
		this.ruleElement = ruleElement;
		element = driver.findElement(By.xpath(xpath));
		
	}

	public boolean verifyShadow(String shadow){
		
		boolean correct = false;
		
		String cssBoxShadow = element.getCssValue("box-shadow");
		
				
		if (cssBoxShadow.equals(shadow)){
			correct = true;
		}
		
		return correct;
	}
	public String hexToRgba(String hex){
		
		Color color = Color.decode(hex);
		
		String colorString  = color.toString();
		String[] colArray = colorString.split(",");
		
		String r = colArray[0].replaceAll("[^0-9]", "").concat(", ");
		String g = colArray[1].replaceAll("[^0-9]", "").concat(", ");
		String b = colArray[2].replaceAll("[^0-9]", "").concat(", 1)");
		
		String finalColor = "rgba(".concat(r).concat(g).concat(b);
			
		return finalColor;
	}
	
	public String getNumber(String stringVal){
		
		stringVal = stringVal.trim().replaceAll("[^0-9]", " ").trim();
				
		if (stringVal.indexOf(" ")>0){
			stringVal = stringVal.replaceAll(" ", ".");
		}
		
		return stringVal;
	}
	
	public String calculateDeviation(String definedVal, String cssVal, String elementName){
		String deviation="";
		if (definedVal.indexOf(".")>0 || cssVal.indexOf(".")>0){
			BigDecimal doubleNum1 = new BigDecimal(definedVal);
			BigDecimal doubleNum2 = new BigDecimal(cssVal);
			
			BigDecimal doubleDeviation = doubleNum1.subtract(doubleNum2);
			
			deviation = "Deviation of: "+doubleDeviation.toString();
		} else{
			
			Integer intNum1 = Integer.parseInt(definedVal);
			Integer intNum2 = Integer.parseInt(cssVal);
			
			Integer intDeviation = intNum1.intValue()-intNum2.intValue();
			
			deviation = "Verification Unsuccessful Deviation of "+intDeviation.toString()+" :"+xpath+": "+elementName;
			System.out.println(deviation);
		}
		
		return deviation;
	}
	public String prepareBackgroundCssImgUrl(String cssUrl, String driverUrl){
		
		if (cssUrl.startsWith("http://") || cssUrl.startsWith("https://")){
			return cssUrl;
		} else if (cssUrl.startsWith(".")){
			
			int index = cssUrl.lastIndexOf("/");
			String dots = cssUrl.substring(0, index).replace("/", "");
			int lastDot = dots.lastIndexOf(".");
			int numberOfDots = lastDot+1;
			
			int numberOfFolders = numberOfDots/2;
						
			cssUrl = cssUrl.substring(index+1);
			
			int driveUrlLastSlash = driverUrl.lastIndexOf("/")+1;
			
			driverUrl = driverUrl.substring(0, driveUrlLastSlash).replace("http://", "");
						
			String[] arr = driverUrl.split("/");
			
			String partialUrl = arr[numberOfFolders-1].concat("/").concat(cssUrl);
			
			
			String firstPartialUrl = "http://"+arr[0].concat("/");
			System.out.println(partialUrl);
			System.out.println(firstPartialUrl);
			for (int i=1; i<numberOfFolders-1; i++){
				
				if (firstPartialUrl.contains(arr[i])){
					continue;
				}
				else{
				firstPartialUrl = firstPartialUrl.concat(arr[i]).concat("/");
				}
			}
			
			
			String finalUrl = firstPartialUrl.concat(partialUrl);
			
			
			return finalUrl;
			
		} else{
			int index = driverUrl.lastIndexOf("/")+1;
			
			driverUrl = driverUrl.substring(0, index);
			
			String finalUrl = driverUrl.concat(cssUrl);
			
			return finalUrl;
			
		}
	}
		

}
