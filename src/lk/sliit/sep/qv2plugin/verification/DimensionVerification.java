package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.DeviationException;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;


public class DimensionVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public DimensionVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		elementName = ruleElement.getName();
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	public String verifyHeight(/*String height*/) throws UndefinedException, DeviationException {

		String height = ruleElement.getHeight().trim().toLowerCase();
		String msg="";
		////String correct = false;

		String cssHeight = element.getCssValue("height");
		
		if (cssHeight == null){
			throw new UndefinedException("Height Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssHeight.equals(height)) {
			
			return passed;
			
		} else {
			
			if (!cssHeight.equals(null)){
				
				String cssVal = getNumber(cssHeight);
				String defVal = getNumber(height);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
				
			}
			throw new DeviationException(msg);
		}

		//return failed;

	}

	public String verifyWidth(/*String width*/) throws UndefinedException, DeviationException  {

		String width = ruleElement.getWidth().trim().toLowerCase();
		String msg="";
		////String correct = false;

		String cssWidth = element.getCssValue("width");

		if (cssWidth == null){
			throw new UndefinedException("Width Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssWidth.equals(width)) {
			
			return passed;
			
		} else {
			
			if (!cssWidth.equals(null)){
				
				String cssVal = getNumber(cssWidth);
				String defVal = getNumber(width);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
				
			}
			
			throw new DeviationException(msg);
		}

		//return correct;
	}

	public String verifyMinHeight(/*String minHeight*/) throws UndefinedException, DeviationException  {

		String minHeight = ruleElement.getMinHeight().trim().toLowerCase();
		String msg = "";
		////String correct = false;

		String cssMinHeight = element.getAttribute("min-height");
		
		if (cssMinHeight == null){
			throw new UndefinedException("Minimum Height Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssMinHeight.equals(minHeight)) {
			
			return passed;
			
		} else {

			if(!cssMinHeight.equals(null)){
				
				String cssVal = getNumber(cssMinHeight);
				String defVal = getNumber(minHeight);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else {
				
				return failed;
				
			}
			throw new DeviationException(msg);
		}

		//return correct;
	}

	public String verifyMaxHeight(/*String maxHeight*/) throws UndefinedException, DeviationException  {

		String maxHeight = ruleElement.getMaxHeight().trim().toLowerCase();
		String msg="";
		////String correct = false;

		String cssMaxHeight = element.getAttribute("max-height");
		
		if (cssMaxHeight == null){
			throw new UndefinedException("Maximum Height Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssMaxHeight.equals(maxHeight)) {
			
			return passed;
			
		} else {
			
			if (!cssMaxHeight.equals(null)){
				
				String cssVal = getNumber(cssMaxHeight);
				String defVal = getNumber(maxHeight);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
				
			}
			throw new DeviationException(msg);
		}

		//return correct;
	}
	
	public String verifyMinWidth() throws UndefinedException, DeviationException {
		
		String minWidth = ruleElement.getMinWidth().trim().toLowerCase();
		String msg="";
		//String correct = false;
		
		String cssMinWidth = element.getAttribute("min-width");
		
		if (cssMinWidth == null){
			throw new UndefinedException("Minimum Width Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssMinWidth.equals(minWidth)){
			
			return passed;
			
		} else {
			;
			if (!cssMinWidth.equals(null)){
				
				String cssVal = getNumber(cssMinWidth);
				String defVal = getNumber(minWidth);
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else {
				
				return failed;
				
			}
			
			throw new DeviationException(msg);
		}
		
		//return correct;
	}
	
	public String verifyMaxWidth() throws UndefinedException, DeviationException {
		
		String maxWidth = ruleElement.getMaxWidth().trim().toLowerCase();
		String msg = "";
		//String correct = false;
		
		String cssMaxWidth = element.getAttribute("max-width");
		
		if (cssMaxWidth == null){
			throw new UndefinedException("Maximum width Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssMaxWidth.equals(maxWidth)){
			
			return passed;
			
		} else{
			
			if (!cssMaxWidth.equals(null)){
				
				String cssVal = getNumber(cssMaxWidth);
				String defVal = getNumber(maxWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else {
				
				return failed;
				
			}
			
			throw new DeviationException(msg);
		}
		
		//return correct;
	}
	/*
	 * end of dimesion properties
	 */

}
