package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class BackgroundVerification extends VerifyElement{
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;
	
	public BackgroundVerification(String xpath, WebDriver driver, RuleElement ruleElement){
		super(xpath, driver, ruleElement);	
		this.xpath = xpath;
		elementName = ruleElement.getName();
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
		
		
	}
	
	
	/*
	 * Declares how and/or if a background image repeats.
	 */
	
	public String verifyBackgroundColor(/*String color*/) throws UndefinedException{
		
		String color = ruleElement.getBackgroundColor().trim().toLowerCase();
		String properColor = hexToRgba(color);
		//String correct = false;
		
		String cssBackgroundColor1 = element.getCssValue("background-color");
		
		String cssBackgroundColor2 = element.getCssValue("background");
		
		if (cssBackgroundColor1 == null && cssBackgroundColor2 == null)
		{
			throw new UndefinedException("Background Color Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssBackgroundColor2 != null){
			cssBackgroundColor2 = cssBackgroundColor2.split(" ")[0];
		}
		
		if (cssBackgroundColor1.equals(properColor) || cssBackgroundColor2.equals(properColor)){
			return passed;
		}
	
		return failed;
		
	}
	
	public String verifyBackgroundImage() throws UndefinedException{
		
		String originalImgPath = ruleElement.getBackgroundImage().trim();
		
		String backgroundImgResource1 = element.getCssValue("background-image").replaceFirst("url", "").replace(")", "").replace("(", "");
		String backgroundImgResource2 = element.getCssValue("background").split(" ")[1].replaceFirst("url", "").replace(")", "").replace("(", "");
		String currentWebUrl = driver.getCurrentUrl();
		
		String properUrl="";
		
		if (backgroundImgResource1 == null && backgroundImgResource2==null)
		{
			throw new UndefinedException("Background Image Undefined in Element "+": "+elementName);
		}
				
		if(!backgroundImgResource1.equals(null)){
			
			properUrl=prepareBackgroundCssImgUrl(backgroundImgResource1, currentWebUrl);
			
		} else if (!backgroundImgResource2.equals(null)){
			
			properUrl = prepareBackgroundCssImgUrl(backgroundImgResource2, currentWebUrl);
			
		} else{
			
			return failed;
		}
		
		System.out.println(properUrl);
		 
		ImageVerification img = new ImageVerification();
		
		boolean correct = img.processAndCompareImage(originalImgPath, properUrl);
		
		if (correct){
			return passed;
		} else{
			return failed;
		}
		
		
	}
	/*
	 * Declares the attachment of a background image (to scroll with the page content or be in a fixed position).
	 */
	
	public String verifyBackgrounAttachment(/*String attachment*/) throws UndefinedException{
		
		String attachment = ruleElement.getBackgroundAttachment().trim().toLowerCase();
		//String correct = false;
		
		String cssBackgroundAttachment1 = element.getCssValue("background-attachment");
		String cssBackgroundAttachment2 = element.getCssValue("background");
		
		if (cssBackgroundAttachment1 == null && cssBackgroundAttachment2 == null)
		{
			throw new UndefinedException("Bankground Attachment Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBackgroundAttachment2 != null && cssBackgroundAttachment2.split(" ").length>3){
			cssBackgroundAttachment2 = cssBackgroundAttachment2.split(" ")[3];
		}
		
		if (cssBackgroundAttachment1.equals(attachment) || cssBackgroundAttachment2.equals(attachment)){
			return passed;
		}
		
		return failed;
	}
	
	/*
	 * Declares the position of a background image.
	 */
	
	public String verifyBackgroundPosition(/*String position*/) throws UndefinedException{
		
		String position = ruleElement.getBackgroundPosition().trim().toLowerCase();
		//String correct = false;
		
		String cssBackgroundPosition1 = element.getCssValue("background-position");
		String cssBackgroundPositon2 = element.getCssValue("background").split(" ")[4];
		
		if (cssBackgroundPosition1 == null && cssBackgroundPositon2 == null)
		{
			throw new UndefinedException("Background Position Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBackgroundPositon2 != null && cssBackgroundPositon2.split(" ").length>4){
			cssBackgroundPositon2 = cssBackgroundPositon2.split(" ")[4];
		}
		
		if (cssBackgroundPosition1.equals(position) || cssBackgroundPositon2.equals(position)){
			return passed;
		}
		
		return failed;
	}
	
	/*
	 * Declares how and/or if a background image repeats.
	 */
	
	public String verifyBackgroundRepeat(/*String repeat*/) throws UndefinedException{
		
		String repeat = ruleElement.getBackgroundRepeat().trim().toLowerCase();
		//String correct = false;
		
		String cssBackgroundRepeat1 = element.getCssValue("background-repeat");
		String cssBackgroundRepeat2 = element.getCssValue("background");
		
		if (cssBackgroundRepeat1 == null && cssBackgroundRepeat2 == null)
		{
			throw new UndefinedException("Background Repeat Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBackgroundRepeat2 != null && cssBackgroundRepeat2.split(" ").length >3){
			cssBackgroundRepeat2 = cssBackgroundRepeat2.split(" ")[2];
		}
		
		if (cssBackgroundRepeat1.equals(repeat) || cssBackgroundRepeat2.equals(repeat)){
			return passed;
		}
		
		return failed;
	}
	
	/*
	 * End of background verification
	 */


}
