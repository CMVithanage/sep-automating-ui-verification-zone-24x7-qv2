package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class OutlineVerification extends VerifyElement {

	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public OutlineVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		elementName = ruleElement.getName();
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	/*
	 * outline props
	 */

	public String verifyOutlineColor(/*String outlineColor*/) throws UndefinedException{

		String outlineColor = ruleElement.getOutlineColor().trim().toLowerCase();
		//String correct = false;

		String properColor = hexToRgba(outlineColor);
		String cssOutlineColor1 = element.getCssValue("outline-color");
		String cssOutlineColor2 = element.getCssValue("outline");
		
		if (cssOutlineColor1 == null && cssOutlineColor2==null){
			throw new UndefinedException("Ouline Color Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssOutlineColor2 != null){
			cssOutlineColor2 = cssOutlineColor2.split(" ")[0];
		}

		if (cssOutlineColor1.equals(properColor)
				|| cssOutlineColor2.equals(properColor)) {
			return passed;
		}

		return failed;

	}

	public String verifyOutlineStyle(/*String outlineStyle*/) throws UndefinedException{

		String outlineStyle = ruleElement.getOutlineStyle().trim().toLowerCase();
		//String correct = false;

		String cssOutlineStyle1 = element.getCssValue("outline-style");
		String cssOutlineStyle2 = element.getCssValue("outline");
		
		if (cssOutlineStyle1 == null && cssOutlineStyle2==null){
			throw new UndefinedException("Ouline Style Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssOutlineStyle2 != null && cssOutlineStyle2.split(" ").length>1){
			cssOutlineStyle2 = cssOutlineStyle2.split(" ")[1];
		}

		if (cssOutlineStyle1.equals(outlineStyle)
				|| cssOutlineStyle2.equals(outlineStyle)) {
			return passed;
		}

		return failed;
	}

	public String verifyOutlineWidth(/*String outlineWidth*/) throws UndefinedException{

		String outlineWidth = ruleElement.getOutlineWidth().trim().toLowerCase();
		//String correct = false;

		String cssOutlineWidth1 = element.getCssValue("outline-width");
		String cssOutlineWidth2 = element.getCssValue("outline");
		
		if (cssOutlineWidth1 == null && cssOutlineWidth2==null){
			throw new UndefinedException("Ouline Width Undefined in Element: "+xpath+": "+elementName);
		}
		
		if(cssOutlineWidth2 != null && cssOutlineWidth2.split(" ").length>2){
			cssOutlineWidth2 = cssOutlineWidth2.split(" ")[2];
		}

		if (cssOutlineWidth1.equals(outlineWidth)
				|| cssOutlineWidth2.equals(outlineWidth)) {
			return passed;
		}

		return failed;
	}

	/*
	 * end of outline props
	 */
}
