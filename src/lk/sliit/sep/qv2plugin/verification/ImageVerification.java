package lk.sliit.sep.qv2plugin.verification;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;

public class ImageVerification {
	
	public boolean processAndCompareImage(String originalFilePath, String urlPath){
		boolean matchFlag = true;
		try {
			URL url = new URL(urlPath);
			File input = new File(originalFilePath);
			//File output = new File(downloadFilePath);
			
			BufferedImage bufImgInput = ImageIO.read(input);
			DataBuffer dataBuffInput = bufImgInput.getData().getDataBuffer();
			int sizeInput = dataBuffInput.getSize();
			
			BufferedImage bufImgOutput = ImageIO.read(url);
			DataBuffer dataBuffOutput = bufImgOutput.getData().getDataBuffer();
			int sizeOutput = dataBuffOutput.getSize();
			
			
			
			if (sizeInput==sizeOutput){
				
				for (int j=0; j<sizeInput; j++){
					if (dataBuffInput.getElem(j)!=dataBuffOutput.getElem(j)){
						matchFlag = false;
						break;
					}
				}
				
			} else{
				matchFlag=false;
			}
			
			System.out.println("And the images matches...?"+matchFlag);
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return matchFlag;
		
	}

}
