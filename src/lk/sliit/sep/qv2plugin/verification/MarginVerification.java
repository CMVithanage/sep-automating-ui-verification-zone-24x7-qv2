package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class MarginVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public MarginVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	/*
	 * margin properties
	 */

	public String verifyMarginTop(/*String marginTop*/) throws UndefinedException {

		String marginTop = ruleElement.getMarginTop().trim().toLowerCase();
		//String correct = false;

		String cssMarginTop1 = element.getCssValue("margin-top");
		String cssMarginTop2 = element.getCssValue("margin");
		
		if (cssMarginTop1 == null && cssMarginTop2==null){
			throw new UndefinedException("Margin Top Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssMarginTop2 != null){
			cssMarginTop2 = cssMarginTop2.split(" ")[0];
		}

		if (cssMarginTop1.equals(marginTop) || cssMarginTop2.equals(marginTop)) {
			return passed;
		}

		return failed;
	}

	public String verifyMarginRight(/*String marginRight*/) throws UndefinedException{

		String marginRight = ruleElement.getMarginRight().trim().toLowerCase();
		//String correct = false;

		String cssMarginRight1 = element.getCssValue("margin-right");
		String cssMarginRight2 = element.getCssValue("margin");
		
		if (cssMarginRight1 == null && cssMarginRight2==null){
			throw new UndefinedException("Margin Right Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssMarginRight2 != null && cssMarginRight2.split(" ").length>1){
			cssMarginRight2 = cssMarginRight2.split(" ")[1];
		}

		if (cssMarginRight1.equals(marginRight)
				|| cssMarginRight2.equals(marginRight)) {
			return passed;
		}

		return failed;
	}

	public String verifyMarginBottom(/*String marginBottom*/) throws UndefinedException{

		String marginBottom = ruleElement.getMarginBottom().trim().toLowerCase();
		//String correct = false;

		String cssMarginBottom1 = element.getCssValue("margin-bottom");
		String cssMarginBottom2 = element.getCssValue("margin");
		
		if (cssMarginBottom1 == null && cssMarginBottom2==null){
			throw new UndefinedException("Margin Bottom Undefined in Element: "+xpath+": "+elementName);
		}
		
		
		if (cssMarginBottom2 != null && cssMarginBottom2.split(" ").length>2){
			cssMarginBottom2 = cssMarginBottom2.split(" ")[2];
		}

		if (cssMarginBottom1.equals(marginBottom)
				|| cssMarginBottom2.equals(marginBottom)) {
			return passed;
		}

		return failed;
	}

	public String verifyMarginLeft(/*String marginLeft*/) throws UndefinedException{

		String marginLeft = ruleElement.getMarginLeft().trim().toLowerCase();
		//String correct = false;

		String cssMarginLeft1 = element.getCssValue("margin-left");
		String cssMarginLeft2 = element.getCssValue("margin");
		
		if (cssMarginLeft1 == null && cssMarginLeft2==null){
			throw new UndefinedException("Margin Left Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssMarginLeft2!=null && cssMarginLeft2.split(" ").length>3){
			cssMarginLeft2 = cssMarginLeft2.split(" ")[3];
		}

		if (cssMarginLeft1.equals(marginLeft)
				|| cssMarginLeft2.equals(marginLeft)) {
			return passed;
		}

		return failed;
	}

	/*
	 * end of margin props
	 */

}
