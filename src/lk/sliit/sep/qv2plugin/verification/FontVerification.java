package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.DeviationException;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class FontVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public FontVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		elementName = ruleElement.getName();
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	public String verifyFont(/*String font*/)  throws UndefinedException{

		String font = ruleElement.getFontFamily().trim().toLowerCase();	//check
		//String correct = false;

		String fontFamily1 = element.getCssValue("font-family").replaceAll(",", " ");
		String fontFamily2 = element.getCssValue("font").replaceAll(",", " ");
		
		if (fontFamily1 == null && fontFamily2 == null){
			throw new UndefinedException("Font Undefined in Element: "+xpath+": "+elementName);
		}
		if (fontFamily2 != null && fontFamily2.split(" ").length>5){
			fontFamily2 = fontFamily2.split(" ")[5];
		}

		if (fontFamily1.contains(font) || fontFamily2.contains(font)) {
			return passed;
		}

		return failed;
	}

	public String verifyFontStyle(/*String style*/) throws UndefinedException{

		String style = ruleElement.getFontStyle().trim().toLowerCase();
		//String correct = false;

		String fontStyle1 = element.getCssValue("font-style");
		String fontStyle2 = element.getCssValue("font").replaceAll(",", " ");
		
		if (fontStyle1 == null && fontStyle2 == null ){
			throw new UndefinedException("Font Style Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (fontStyle2 != null){
			fontStyle2 = fontStyle2.split(" ")[0];
		}

		if (fontStyle1.equals(style) || fontStyle2.equals(style)) {

			return passed;
		}

		return failed;
	}

	public String verifyFontWeight(/*String weight*/) throws UndefinedException{

		String weight = ruleElement.getFontWeight().trim().toLowerCase();
		//String correct = false;

		String fontWeight1 = element.getCssValue("font-weight");
		String fontWeight2 = element.getCssValue("font");
		
		if (fontWeight1 == null && fontWeight2 == null ){
			throw new UndefinedException("Font Weight Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (fontWeight2 != null && fontWeight2.split(" ").length>2){
			fontWeight2 = fontWeight2.split(" ")[2];
		}

		if (fontWeight1.equals(weight) || fontWeight2.equals(weight)) {
			return passed;
		}

		return failed;
	}

	public String verifyFontSize(/*String size*/)throws UndefinedException, DeviationException {

		String size = ruleElement.getFontSize().trim().toLowerCase();
		String msg="";
		//String correct = false;
		

		String cssFontSize1 = element.getCssValue("font-size");
		String cssFontSize2 = element.getCssValue("font");
		
		if (cssFontSize1 == null && cssFontSize2 == null ){
			throw new UndefinedException("Font Size Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssFontSize2 != null && cssFontSize2.split(" ").length>3){
			cssFontSize2 = cssFontSize2.split(" ")[3];
		}
		System.out.println("CSS: "+cssFontSize1);
		if (cssFontSize1.equals(size) || cssFontSize2.equals(size)) {
			
			return passed;
			
		} else {
			
			if (!cssFontSize1.equals(null)){
				
				String cssVal = getNumber(cssFontSize1);
				String defVal = getNumber(size);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else if (!cssFontSize2.equals(null)){
				
				String cssVal = getNumber(cssFontSize2);
				String defVal = getNumber(size);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
				
			}
			
			throw new DeviationException(msg);
		}

		//return correct;
	}

	public String verifyFontVariant(/*String fontVariant*/) throws UndefinedException {

		String fontVariant = ruleElement.getFontVariant().trim().toLowerCase();
		//String correct = false;

		String cssFontVariant1 = element.getCssValue("font-variant");
		String cssFontVariant2 = element.getCssValue("font");
		
		if (cssFontVariant1 == null && cssFontVariant2 == null ){
			throw new UndefinedException("Font Variant Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssFontVariant2 != null && cssFontVariant2.split(" ").length>1){
			cssFontVariant2 = cssFontVariant2.split(" ")[1];
		}

		if (cssFontVariant1.equals(fontVariant)
				|| cssFontVariant2.equals(fontVariant)) {
			return passed;
		}

		return failed;
	}

	/*
	 * end of font
	 */
}
