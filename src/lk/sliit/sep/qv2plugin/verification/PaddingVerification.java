package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class PaddingVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public PaddingVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		passed = "Verification successful: "+xpath+": "+elementName;
		failed = "Verification unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	public String verifyPadding(/*String padding*/) throws UndefinedException{

		String paddingTop = ruleElement.getPaddingTop().trim().toLowerCase();
		String paddingRight = ruleElement.getPaddingRight().trim().toLowerCase();
		String paddingBottom = ruleElement.getPaddingBottom().trim().toLowerCase();
		String paddingLeft = ruleElement.getPaddingLeft().trim().toLowerCase();
		
		//String correct = false;

		String[] cssPadding = element.getCssValue("padding").split(" ");
		
		if (cssPadding == null){
			throw new UndefinedException("Padding Undefined in Element: "+xpath+": "+elementName);
		}
		if (cssPadding!=null && cssPadding[0].equals(paddingTop) && cssPadding[1].equals(paddingRight) 
				&& cssPadding[2].equals(paddingBottom) && cssPadding[3].equals(paddingLeft)) {
			return passed;
		}

		return failed;
	}

	public String verifyPaddingBottom(/*String paddingBottom*/)  throws UndefinedException{

		String paddingBottom = ruleElement.getPaddingBottom().trim().toLowerCase();
		//String correct = false;

		String cssPaddingBottom1 = element.getCssValue("padding-bottom");
		String cssPaddingBottom2 = element.getCssValue("padding");
		
		if (cssPaddingBottom1 == null && cssPaddingBottom2 == null){
			throw new UndefinedException("Padding Bottom Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssPaddingBottom2 != null && cssPaddingBottom2.split(" ").length>2){
			cssPaddingBottom2 = cssPaddingBottom2.split(" ")[2];
		}

		if (cssPaddingBottom1.equals(paddingBottom) || cssPaddingBottom2.equals(paddingBottom)) {
			return passed;
		}

		return failed;
	}

	public String verifyPaddingLeft(/*String paddingLeft*/)  throws UndefinedException{

		String paddingLeft = ruleElement.getPaddingLeft().trim().toLowerCase();
		//String correct = false;

		String cssPaddingLeft1 = element.getCssValue("padding-left");
		String cssPaddingLeft2 = element.getCssValue("padding");
		
		if (cssPaddingLeft1 == null && cssPaddingLeft2 == null){
			throw new UndefinedException("Padding Left Undefined in Element: "+xpath+": "+elementName);
		}
		
		if(cssPaddingLeft2 != null && cssPaddingLeft2.split(" ").length>3){
			cssPaddingLeft2 = cssPaddingLeft2.split(" ")[3];
		}

		if (cssPaddingLeft1.equals(paddingLeft) || cssPaddingLeft2.equals(paddingLeft)) {
			return passed;
		}

		return failed;
	}

	public String verifyPaddingRight(/*String paddingRight*/)  throws UndefinedException{

		String paddingRight = ruleElement.getPaddingRight().trim().toLowerCase();
		//String correct = false;

		String cssPaddingRight1 = element.getCssValue("padding-right");
		String cssPaddingRight2 = element.getCssValue("padding");
		
		if (cssPaddingRight1 == null && cssPaddingRight2 == null){
			throw new UndefinedException("Padding Right Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssPaddingRight2 != null && cssPaddingRight2.split(" ").length>1){
			cssPaddingRight2 = cssPaddingRight2.split(" ")[1];
		}

		if (cssPaddingRight1.equals(paddingRight) || cssPaddingRight2.equals(paddingRight)) {
			return passed;
		}

		return failed;
	}

	public String verifyPaddingTop(/*String paddingTop*/)  throws UndefinedException{

		String paddingTop = ruleElement.getPaddingBottom().trim().toLowerCase();
		//String correct = false;

		String cssPaddingTop1 = element.getCssValue("padding-top");
		String cssPaddingTop2 = element.getCssValue("padding");
		
		if (cssPaddingTop1 == null && cssPaddingTop2 == null){
			throw new UndefinedException("Padding Top Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssPaddingTop2 != null){
			cssPaddingTop2 = cssPaddingTop2.split(" ")[0];
		}

		if (cssPaddingTop1.equals(paddingTop) || cssPaddingTop2.equals(paddingTop)) {
			return passed;
		}

		return failed;

	}
	/*
	 * end of padding
	 */

}
