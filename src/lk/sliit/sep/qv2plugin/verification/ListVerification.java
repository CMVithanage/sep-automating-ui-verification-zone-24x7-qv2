package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class ListVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;
	
	public ListVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	public String verifyListStyleType(/*String listStyleType*/) throws UndefinedException{

		String listStyleType = ruleElement.getListStyleType().trim().toLowerCase();
		//String correct = false;

		String cssListStyleType1 = element.getCssValue("list-style-type");
		String cssListStyleType2 = element.getCssValue("list-style");
		
		if (cssListStyleType1 == null && cssListStyleType2 == null ){
			throw new UndefinedException("List Style Undefined in Element: "+xpath+": "+elementName);
		}
		
		if (cssListStyleType2 != null){
			cssListStyleType2 = cssListStyleType2.split(" ")[0];
		}

		if (cssListStyleType1.equals(listStyleType)
				|| cssListStyleType2.equals(listStyleType)) {
			return passed;
		}

		return failed;

	}

	/*
	 * Declares the position of the list marker.
	 */
	public String listStylePosition(/*String listStylePosition*/) throws UndefinedException{

		String listStylePosition = ruleElement.getListStylePosition().trim().toLowerCase();
		//String correct = false;

		String cssListStylePosition1 = element
				.getCssValue("list-style-position");
		String cssListStylePosition2 = element.getCssValue("list-style");
		
		if (cssListStylePosition1 == null && cssListStylePosition2 == null ){
			throw new UndefinedException("List Style Position Undefined in Element: "+xpath);
		}
		
		if (cssListStylePosition2 != null && cssListStylePosition2.split(" ").length>1){
			cssListStylePosition2 = cssListStylePosition2.split(" ")[1];
		}

		if (cssListStylePosition1.equals(listStylePosition)
				|| cssListStylePosition2.equals(listStylePosition)) {
			return passed;
		}

		return failed;
	}

	/*
	 * Declares the marker offset for elements with a value of marker set for
	 * the display property.
	 */

	public String verifyMarkerOffset(/*String markerOffSet*/) throws UndefinedException{

		String markerOffSet = ruleElement.getMarkerOffset().trim().toLowerCase();
		//String correct = false;

		String cssMarkerOffset = element.getCssValue("marker-offset");
		
		if (cssMarkerOffset == null){
			throw new UndefinedException("Marker Offset Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssMarkerOffset.equals(markerOffSet)) {
			return passed;
		}

		return failed;

	}
	/*
	 * end of list properties
	 */

}
