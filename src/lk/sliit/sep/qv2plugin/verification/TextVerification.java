package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class TextVerification extends VerifyElement {
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;

	public TextVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		passed = "Verification successful: "+xpath+": "+elementName;
		failed = "Verification unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}

	public String verifyColor(/*String color*/) throws UndefinedException{ 

		String color = ruleElement.getColor().trim().toLowerCase();
		String properColor = hexToRgba(color).trim().toLowerCase();
		//String correct = false;

		String cssColor = element.getCssValue("color");
		
		if (cssColor == null){
			throw new UndefinedException("Color Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssColor.equals(properColor)) {
			return passed;
		}

		return failed;

	}

	public String verifyTextDirection(/*String direction*/) throws UndefinedException{

		String direction = ruleElement.getDirection().trim().toLowerCase();
		//String correct = false;

		String cssTextDirection = element.getCssValue("direction");
		
		if (cssTextDirection == null){
			throw new UndefinedException("Direction Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextDirection.equals(direction)) {
			return passed;
		}

		return failed;
	}

	public String verifyLineHeight(/*String lineHeight*/) throws UndefinedException{

		String lineHeight = ruleElement.getLineHeight().trim();
		//String correct = false;

		String cssLineHeight = element.getCssValue("line-height");
		
		if (cssLineHeight == null){
			throw new UndefinedException("Line Height Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssLineHeight.equals(lineHeight)) {
			return passed;
		}

		return failed;
	}

	public String verifyLetterSpacing(/*String letterSpacing*/) throws UndefinedException{

		String letterSpacing = ruleElement.getLetterSpacing().trim();
		//String correct = false;

		String cssLetterSpacing = element.getCssValue("letter-spacing");
		
		if (cssLetterSpacing == null){
			throw new UndefinedException("Letter Spacing Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssLetterSpacing.equals(letterSpacing)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextAlign(/*String textAlign*/) throws UndefinedException{

		String textAlign = ruleElement.getTextAlign().trim(); 
		//String correct = false;

		String cssTextAlign = element.getCssValue("text-align");
		
		if (cssTextAlign == null){
			throw new UndefinedException("Text Align Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextAlign.equals(textAlign)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextDecoration(/*String textDecoration*/) throws UndefinedException{

		String textDecoration = ruleElement.getTextDecoration().trim();
		//String correct = false;

		String cssTextDecoration = element.getCssValue("text-decoration");
		
		if (cssTextDecoration == null){
			throw new UndefinedException("Text Decoration Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextDecoration.contains(textDecoration)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextAutospace(String autoSpace) throws UndefinedException{

		//String autoSpace = ruleElement.gette
		//String correct = false;

		String cssTextAutospace = element.getCssValue("text-autospace");
		
		if (cssTextAutospace == null){
			throw new UndefinedException("Text Auto Space Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextAutospace.equals(autoSpace)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextIndent(/*String indent*/) throws UndefinedException{

		String indent = ruleElement.getTextIndent().trim();
		//String correct = false;

		String cssTextIndent = element.getCssValue("text-indent");
		
		if (cssTextIndent == null){
			throw new UndefinedException("Text Indent Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextIndent.equals(indent)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextOverflow(String overflow) throws UndefinedException {

		//String correct = false;

		String cssTextOverflow = element.getCssValue("text-overflow");
		
		if (cssTextOverflow == null){
			throw new UndefinedException("Text Overflow Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextOverflow.equals(overflow)) {
			return passed;
		}

		return failed;

	}

	public String verifyTextSpacing(/*String spacing*/) throws UndefinedException{

		String spacing = ruleElement.getWordSpacing().trim();
		//String correct = false;

		String cssTextSpacing = element.getCssValue("word-spacing");
		
		if (cssTextSpacing == null){
			throw new UndefinedException("Text Spacing Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextSpacing.equals(spacing)) {
			return passed;
		}

		return failed;
	}

	public String verifyWordWrap(String wordWrap) throws UndefinedException{

		//String correct = false;

		String cssWordWrap = element.getCssValue("word-wrap");
		
		if (cssWordWrap == null){
			throw new UndefinedException("Word Wrap Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssWordWrap.equals(wordWrap)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextShadow(String color, String vDistance,
			String hDistance, String blur) {			//has to be created

		//String correct = false;

		String properColor = hexToRgba(color);

		String[] cssTextShadowArray = element.getCssValue("text-shadow").split(
				" ");

		if (properColor.equals(cssTextShadowArray[0])
				&& vDistance.equals(cssTextShadowArray[1])
				&& hDistance.equals(cssTextShadowArray[2])
				&& blur.equals(cssTextShadowArray[3])) {
			return passed;
		}

		return failed;
	}

	public String verifyTextTransform(/*String textTransform*/) throws UndefinedException{

		String textTransform = ruleElement.getTextTransform().trim();
		//String correct = false;

		String cssTextTransform = element.getCssValue("text-transform");
		
		if (cssTextTransform == null){
			throw new UndefinedException("Text Transform Undefined in Element: "+xpath+": "+elementName);
		}


		if (cssTextTransform.equals(textTransform)) {
			return passed;
		}

		return failed;
	}

	public String verifyTextWhiteSpace(/*String textWhiteSpace*/) throws UndefinedException{

		String textWhiteSpace = ruleElement.getWhiteSpace().trim();
		//String correct = false;

		String cssTextWhiteSpace = element.getCssValue("white-space");
		
		if (cssTextWhiteSpace == null){
			throw new UndefinedException("Whitespace Undefined in Element: "+xpath+": "+elementName);
		}

		if (cssTextWhiteSpace.equals(textWhiteSpace)) {
			return passed;
		}

		return failed;
	}

	/*
	 * end of text
	 */

}
