package lk.sliit.sep.qv2plugin.verification;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.exceptions.DeviationException;
import lk.sliit.sep.qv2plugin.exceptions.UndefinedException;

import org.openqa.selenium.WebDriver;

public class BorderVerification extends VerifyElement{
	
	private String xpath;
	private String passed;
	private String failed;
	private String elementName;
	
	public BorderVerification(String xpath, WebDriver driver,
			RuleElement ruleElement) {
		super(xpath, driver, ruleElement);
		elementName = ruleElement.getName();
		passed = "Verification Successful: "+xpath+": "+elementName;
		failed = "Verification Unsuccessful: "+xpath+": "+elementName;
		this.xpath = xpath;
	}
	
	public String verifyWholeBorderColor(/*String color*/) throws UndefinedException{	//only if 1 border color
		
		String color = ruleElement.getBorderColor().trim().toLowerCase();
		String properColor = hexToRgba(color);
		//String correct = false;
		
		String cssBorderColor1 = element.getCssValue("border-color");
		String cssBorderColor2 = element.getCssValue("border");
		
		if (cssBorderColor1 == null && cssBorderColor2==null)
		{
			throw new UndefinedException("Border Color Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderColor2 != null && cssBorderColor2.split(" ").length >2){
			cssBorderColor2 =  cssBorderColor2.split(" ")[2];
		}
		
		if (cssBorderColor1.equals(properColor) || cssBorderColor2.equals(properColor)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyTopBorderColor(/*String color*/) throws UndefinedException{
		
		String color = ruleElement.getBorderTopColor().trim().toLowerCase();
		String properColor = hexToRgba(color);
		//String correct = false;
		
		String cssBorderColor1 = element.getCssValue("border-top-color");
		String cssBorderColor2 = element.getCssValue("border-top");
		
		if (cssBorderColor1 == null && cssBorderColor2==null)
		{
			throw new UndefinedException("Top Border Color Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderColor2 != null && cssBorderColor2.split(" ").length>2){
			cssBorderColor2 = cssBorderColor2.split(" ")[2];
		}
		
		if (cssBorderColor1.equals(properColor) || cssBorderColor2.equals(properColor)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyLeftBorderColor(/*String color*/) throws UndefinedException{
		
		String color = ruleElement.getBorderLeftColor().trim().toLowerCase();
		String properColor = hexToRgba(color);
		//String correct = false;
		
		String cssBorderColor1 = element.getCssValue("border-left-color");
		String cssBorderColor2 = element.getCssValue("border-left");
		
		if (cssBorderColor1 == null && cssBorderColor2 == null)
		{
			throw new UndefinedException("Left Border Color Undefined in Element"+xpath+": "+elementName);
		}
		
		if (cssBorderColor2 != null && cssBorderColor2.split(" ").length>2){
			cssBorderColor2=cssBorderColor2.split(" ")[2];
		}
		if (cssBorderColor1.equals(properColor) || cssBorderColor2.equals(properColor)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyRightBorderColor(/*String color*/) throws UndefinedException{
		
		String color = ruleElement.getBorderRightColor().trim().toLowerCase();
		String properColor = hexToRgba(color);
		//String correct = false;
		
		String cssBorderColor1 = element.getCssValue("border-right-color");
		String cssBorderColor2 = element.getCssValue("border-right");
		
		if (cssBorderColor1 == null && cssBorderColor2 == null)
		{
			throw new UndefinedException("Right Border Color Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderColor2 != null && cssBorderColor2.split(" ").length >2){
			cssBorderColor2 = cssBorderColor2.split(" ")[2];
		}
		
		if (cssBorderColor1.equals(properColor) || cssBorderColor2.equals(properColor)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyBottomBorderColor(/*String color*/) throws UndefinedException{
		
		String color = ruleElement.getBorderBottomColor().trim().toLowerCase();
		String properColor = hexToRgba(color);
		//String correct = false;
		
		String cssBorderColor1 = element.getCssValue("border-bottom-color");
		String cssBorderColor2 = element.getCssValue("border-bottom");
		
		if (cssBorderColor1 == null && cssBorderColor2 == null)
		{
			throw new UndefinedException("Bottom Border Color Undefined in Element "+xpath+": "+elementName);
		}
		
		if(cssBorderColor2 != null && cssBorderColor2.split(" ").length>2){
			cssBorderColor2 = cssBorderColor2.split(" ")[2];
		}
		
		if (cssBorderColor1.equals(properColor) || cssBorderColor2.equals(properColor)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyWholeBorderWidth(/*String borderWidth*/) throws UndefinedException, DeviationException{	//if only one border defined
		
		String borderWidth = ruleElement.getBorderWidth().trim().toLowerCase();
		//String correct = false;
		String msg="";
		
		String cssBorderWidth1 = element.getCssValue("border-width");
		String cssBorderWidth2 = element.getCssValue("border");
		
		if (cssBorderWidth1 == null && cssBorderWidth2 == null)
		{
			throw new UndefinedException("Border Width Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderWidth2 != null){
			cssBorderWidth2 = cssBorderWidth2.split(" ")[0];
		}
		
		if (cssBorderWidth1.equals(borderWidth) || cssBorderWidth2.equals(borderWidth)){
			return passed;
		} else{
			
			if (!cssBorderWidth1.equals(null)){
				String cssBorder = getNumber(cssBorderWidth1);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssBorder, elementName);
				
			} else if (!cssBorderWidth2.equals(null)){
				
				String cssBorder = getNumber(cssBorderWidth2);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssBorder, elementName);
				
			} else{
				
				return failed;
			}
			
			throw new DeviationException(msg);
		}
		
		//return failed;
	}
	
	public String verifyTopBorderWidth(/*String borderWidth*/) throws UndefinedException, DeviationException{
		
		String borderWidth = ruleElement.getBorderTopWidth().trim().toLowerCase();
		//String correct = false;
		String msg="";
		
		String cssBorderWidth1 = element.getCssValue("border-top-width");
		String cssBorderWidth2 = element.getCssValue("border-top");
		
		if (cssBorderWidth1 == null && cssBorderWidth2 == null)
		{
			throw new UndefinedException("Top Border Width Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderWidth2 != null){
			cssBorderWidth2 = cssBorderWidth2.split(" ")[0];
		}
						
		if (cssBorderWidth1.equals(borderWidth) || cssBorderWidth2.equals(borderWidth)){
			return passed;
		} else{
			
			if (!cssBorderWidth1.equals(null)){				
				String defVal = getNumber(borderWidth);
				String cssVal = getNumber(cssBorderWidth1);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else if (!cssBorderWidth2.equals(null)){
				
				String defVal = getNumber(borderWidth);
				String cssVal = getNumber(cssBorderWidth2);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
				
			}

			throw new DeviationException(msg);
		}
		
		//return correct;
	}
	
	public String verifyRightBorderWidth(/*String borderWidth*/) throws UndefinedException, DeviationException{
		
		String borderWidth = ruleElement.getBorderRightWidth().trim().toLowerCase();
		//String correct = false;
		String msg="";
		
		String cssBorderWidth1 = element.getCssValue("border-right-width");
		String cssBorderWidth2 = element.getCssValue("border-right");
		
		if (cssBorderWidth1 == null && cssBorderWidth2 == null)
		{
			throw new UndefinedException("Right Border Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderWidth2 != null){
			cssBorderWidth2 = cssBorderWidth2.split(" ")[0];
		}
		
		if (cssBorderWidth1.equals(borderWidth) || cssBorderWidth2.equals(borderWidth)){
			return passed;
			
		} else{
			
			if (!cssBorderWidth1.equals(null)){
				
				String cssVal = getNumber(cssBorderWidth1);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else if (!cssBorderWidth2.equals(null)){
				
				String cssVal = getNumber(cssBorderWidth2);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
			}
			
			throw new DeviationException(msg);
		}
		
		//return correct;
	}
	
	public String verifyLeftBorderWidth(/*String borderWidth*/) throws UndefinedException, DeviationException{
		
		String borderWidth = ruleElement.getBorderLeftWidth().trim().toLowerCase();
		String msg = "";
		//String correct = false;
		
		String cssBorderWidth1 = element.getCssValue("border-left-width");
		String cssBorderWidth2 = element.getCssValue("border-left");
		
		if (cssBorderWidth1 == null && cssBorderWidth2 == null)
		{
			throw new UndefinedException("Left Border Width Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderWidth2 != null){
			cssBorderWidth2 = cssBorderWidth2.split(" ")[0];
		}
		
		if (cssBorderWidth1.equals(borderWidth) || cssBorderWidth2.equals(borderWidth)){
			return passed;
			
		} else{
			
			if(!cssBorderWidth1.equals(null)){
				
				String cssVal = getNumber(cssBorderWidth1);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else if (!cssBorderWidth2.equals(null)){
				
				String cssVal = getNumber(cssBorderWidth2);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else{
				
				return failed;
			}

			throw new DeviationException(msg);
		}
		
		//return correct;
	}
	
	public String verifyBottomBorderWidth(/*String borderWidth*/) throws UndefinedException, DeviationException{
		
		String borderWidth = ruleElement.getBorderBottomWidth().trim().toLowerCase();
		String msg="";
		//String correct = false;
		
		String cssBorderWidth1 = element.getCssValue("border-bottom-width");
		String cssBorderWidth2 = element.getCssValue("border-bottom");
		
		if (cssBorderWidth1 == null && cssBorderWidth2 == null)
		{
			throw new UndefinedException("Bottom Border Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderWidth2 != null){
			cssBorderWidth2 = cssBorderWidth2.split(" ")[0];
		}
				
		if (cssBorderWidth1.equals(borderWidth) || cssBorderWidth2.equals(borderWidth)){
			return passed;
			
		} else{
			
			if (!cssBorderWidth1.equals(null)){
				
				String cssVal = getNumber(cssBorderWidth1);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else if(!cssBorderWidth2.equals(null)){
				
				String cssVal = getNumber(cssBorderWidth2);
				String defVal = getNumber(borderWidth);
				
				msg = calculateDeviation(defVal, cssVal, elementName);
				
			} else {
				
				return failed;
				
			}
			
			throw new DeviationException(msg);
		}
		
//		return correct;
	}
	
	/*
	 * Declares the style of the top border.
	 */
	
public String verifyWholeBorderStyle(/*String style*/) throws UndefinedException{
		
		String style = ruleElement.getBorderStyle().trim().toLowerCase();
		//String correct = false;
		
		String cssBorderStyle1 = element.getCssValue("border-style");
		String cssBorderStyle2 = element.getCssValue("border");
		
		if (cssBorderStyle1 == null && cssBorderStyle2 == null)
		{
			throw new UndefinedException("Border Style Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderStyle2 != null && cssBorderStyle2.split(" ").length > 1){
			cssBorderStyle2 = cssBorderStyle2.split(" ")[1];
		}
		
		if (cssBorderStyle1.equals(style) || cssBorderStyle2.equals(style)){
			return passed;
		}
		
		return failed;
		
	}
	
	public String verifyTopBorderStyle(/*String style*/) throws UndefinedException{
		
		String style = ruleElement.getBorderTopStyle().trim().toLowerCase();
		//String correct = false;
		
		String cssBorderStyle1 = element.getCssValue("border-top-style");
		String cssBorderStyle2 = element.getCssValue("border-top");
		
		if (cssBorderStyle1 == null && cssBorderStyle2 == null)
		{
			throw new UndefinedException("Top Border Style Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderStyle2 != null && cssBorderStyle2.split(" ").length > 1){
			cssBorderStyle2 = cssBorderStyle2.split(" ")[1];
		}
		
		if (cssBorderStyle1.equals(style) || cssBorderStyle2.equals(style)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyRightBorderStyle(/*String style*/) throws UndefinedException{
		
		String style = ruleElement.getBorderRightStyle().trim().toLowerCase();
		//String correct = false;
		
		String cssBorderStyle1 = element.getCssValue("border-right-style");
		String cssBorderStyle2 = element.getCssValue("border-right");
		
		if (cssBorderStyle1 == null && cssBorderStyle2 == null)
		{
			throw new UndefinedException("Right Border Style Undefined in Element "+xpath+": "+elementName);
		}
		
		if (cssBorderStyle2 != null && cssBorderStyle2.split(" ").length > 1){
			cssBorderStyle2 = cssBorderStyle2.split(" ")[1];
		}
		
		if (cssBorderStyle1.equals(style) || cssBorderStyle2.equals(style)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyLeftBorderStyle(/*String style*/) throws UndefinedException{
		
		String style = ruleElement.getBorderLeftStyle().trim().toLowerCase();
		//String correct = false;
		
		String cssBorderStyle1 = element.getCssValue("border-left-style");
		String cssBorderStyle2 = element.getCssValue("border-left");
		
		if (cssBorderStyle1 == null && cssBorderStyle2 == null)
		{
			throw new UndefinedException("Left Border Style Undefined in Element "+xpath+": "+elementName);
		}

		if (cssBorderStyle2 != null && cssBorderStyle2.split(" ").length > 1){
			cssBorderStyle2 = cssBorderStyle2.split(" ")[1];
		}
		
		if (cssBorderStyle1.equals(style) || cssBorderStyle2.equals(style)){
			return passed;
		}
		
		return failed;
	}
	
	public String verifyBottomBorderStyle(/*String style*/) throws UndefinedException{
		
		String style = ruleElement.getBorderBottomStyle().trim().toLowerCase();
		//String correct = false;
		
		String cssBorderStyle1 = element.getCssValue("border-bottom-style");
		String cssBorderStyle2 = element.getCssValue("border-bottom");
		
		if (cssBorderStyle1 == null && cssBorderStyle2 == null)
		{
			throw new UndefinedException("Bottom Border Style Undefined in Element "+xpath+": "+elementName);
		}

		if (cssBorderStyle2 != null && cssBorderStyle2.split(" ").length > 1){
			cssBorderStyle2 = cssBorderStyle2.split(" ")[1];
		}
		
		if (cssBorderStyle1.equals(style) || cssBorderStyle2.equals(style)){
			return passed;
		}
		
		return failed;
	}
	
	/*
	 * end of border 
	 */

}
