package lk.sliit.sep.qv2plugin.database;

import java.util.HashMap;

public class ValidatedReuslt implements java.io.Serializable {
	
	
	private String type = null;
	private int id = -1;
	private HashMap<String, String> resultMap = new HashMap<String, String>();
	private String url=null;
	
	public ValidatedReuslt(int id,String type,String url,HashMap<String, String> resultMap){
		
		setId(id);
		setType(type);
		setUrl(url);
		setResultMap(resultMap);
		
	}
	
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public HashMap<String, String> getResultMap() {
		return resultMap;
	}
	public void setResultMap(HashMap<String, String> resultMap) {
		this.resultMap = resultMap;
	}
	
	
	
	
	
	

}
