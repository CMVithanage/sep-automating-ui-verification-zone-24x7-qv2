package lk.sliit.sep.qv2plugin.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import javax.swing.JOptionPane;


public class DB {

    private static Connection c;

    public static Connection getConnection() throws Exception {
        if (c == null) {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/qv2", "root", "root");
            return c;

        } else {

            return c;			
        }

    }

    
    public static void msg(String msg) {
        JOptionPane.showMessageDialog(null, msg);
    }
}
