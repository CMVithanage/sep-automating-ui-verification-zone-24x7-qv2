package lk.sliit.sep.qv2plugin.database;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.JOptionPane;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.sun.jna.platform.win32.WinBase.SYSTEMTIME;

import lk.sliit.sep.qv2plugin.RuleElement;


public class SaveObject {
	DB db = new DB();
	private Vector<RuleElement> ruleElements;
	
    public Object javaObject=null;
    
   

	

    public Object getJavaObject() {
        return javaObject;
    }


    public void setJavaObject(Object javaObject) {
        this.javaObject = javaObject;
    }


    public  boolean saveRuleElement(int Id,String name, String type,RuleElement ruleElement) 
    {
    	 boolean status = true;
        try{
        	
        Connection conn= db.getConnection() ;/// get connection string;
        PreparedStatement ps=null;
        String sql=null;
        
        byte[] data = serialize(ruleElement);

        sql="insert into rules (ID,Name,type,value) values('"+Id+"','"+name+"','"+type+"',?)";
     
         ps=conn.prepareStatement(sql);
        ps.setObject(1, data);
        ps.executeUpdate();
        }
        
        catch (MySQLIntegrityConstraintViolationException e) {
			JOptionPane.showMessageDialog(null,"The ID already exists!");
			status = false;
		} 
        catch(Exception e)
        {
            e.printStackTrace();
            status = false;
        }
        
        return status;
    }

    public  boolean saveValidatedResult(ValidatedReuslt vr) 
    {
    	 boolean status = true;
        try{
        	
        Connection conn= db.getConnection() ;/// get connection string;
        PreparedStatement ps=null;
        String sql=null;
        
       // Date date=(Date) Calendar.getInstance().getTime();//get the system date
       // System.out.println(date);
       // java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        
       // JOptionPane.showMessageDialog(null, vr.getId());
        /////System.currentTimeMillis()
        java.util.Date date = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(date.getTime()); 
        
        
        
        sql="insert into validateresults (RuleId,RuleType,Url,Date,Value) values('"+vr.getId()+"','"+vr.getType()+"','"+vr.getUrl()+"',now(),?)";
       
        byte[] data = serialize(vr);
         ps=conn.prepareStatement(sql);
     //    ps.setDate(1, new java.sql.Date(date);
        ps.setObject(1, data);
        ps.executeUpdate();
        
        }
        catch(Exception e)
        {
            e.printStackTrace();
            status = false;
        }
        
        return status;
    }

    
    

    public Vector<RuleElement> getObject() throws Exception
    {
    	ruleElements = new Vector<RuleElement>();
       
        Connection conn= db.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        String sql=null;

        sql="select * from rules";

        ps=conn.prepareStatement(sql);

        rs=ps.executeQuery();
        
        while(rs.next())
        {
            ByteArrayInputStream bais;

            ObjectInputStream ins;

            try {

            bais = new ByteArrayInputStream(rs.getBytes("Value"));

            ins = new ObjectInputStream(bais);

        

            	RuleElement mc = (RuleElement)ins.readObject();
            	// System.out.println("background color : "+mc.getBackgroundColor());
            	 
            	
            	
            ruleElements.add(mc);
           
           
       
            ins.close();

            }
            catch (Exception e) {

      
            e.printStackTrace();
            }

        }

        
//        for (RuleElement re : ruleElements)
//        System.out.println(re.getBackgroundColor());
        
        return ruleElements;
    }
    
    public void update(Vector<RuleElement> ruleElements){
    	
    	//boolean status = false ;
    	
    	int id=-1;
    	byte[] data=null;
    	Connection conn = null;
    	PreparedStatement ps=null;
        String sql=null;

    	
		try {
			conn = db.getConnection();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
        
        for(RuleElement re : ruleElements){
        	
        	
        	
        	try {
        		
				data = serialize(re);// getting the the objects serialized
				
				id = re.getId();
				sql="update rules set Value = ? where ID = '"+id+"'";
				ps=conn.prepareStatement(sql);
				ps.setObject(1, data);
				ps.execute();
        	
        	}
        catch(SQLException e){
        		e.printStackTrace();
        	}
    	
        }
    }
    
    
    public byte[] serialize(Object re){
    	
    	byte[] data = null;
    	
    	try{
    		
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);

        oos.writeObject(re);
        oos.flush();
        oos.close();
        bos.close();
        
        data = bos.toByteArray();
        
    	}
    	catch(Exception e){
    		JOptionPane.showMessageDialog(null, "Methna");
    		e.printStackTrace();
    		
    	}

        
    	
		return data;
    	
    }
    
    
    public boolean delete(int id){
    	
    	
    	boolean status = false;
    	
    	try{
    	Connection conn= db.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        String sql=null;

        sql="delete  from rules where id = '"+id+"'";

        ps=conn.prepareStatement(sql);

        ps.execute();
        
        status = true;
        
    	}
    	catch(Exception e){
    		
    		e.printStackTrace();
    		
    	}
    	
    	return status;
    	
    } 
    
    
}
