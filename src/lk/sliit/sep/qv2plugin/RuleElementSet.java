package lk.sliit.sep.qv2plugin;


public class RuleElementSet {
	private int id;
	private String name;
	
	private int [] RuleElementIdArray;
	private String [] RuleElementNameArray;
	
	private RuleElement [] ruleElements;
	
	private final String [] ruleElementTypes = new String [] {"Button", 
			"Label", "TextField"};
	

	public RuleElementSet(int id, String name, RuleElement[] ruleElements) {
		super();
		this.id = id;
		this.name = name;
		this.ruleElements = ruleElements;
		this.ruleElementIdFacilitator();
		this.ruleElementNameFacilitator();
	}
	
	public RuleElementSet() {
		super();
		this.id = -1;
		this.name = "";
		this.ruleElements = null;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[] getRuleElementIdArray() {
		return RuleElementIdArray;
	}

	public void setRuleElementIdArray() {
		ruleElementIdFacilitator();;
	}

	public String[] getRuleElementNameArray() {
		return RuleElementNameArray;
	}

	public void setRuleElementNameArray() {
		ruleElementNameFacilitator();
	}

	public RuleElement[] getRuleElements() {
		return ruleElements;
	}

	public void setRuleElements(RuleElement[] ruleElements) {
		this.ruleElements = ruleElements;
	}

	public String[] getRuleElementTypes() {
		return ruleElementTypes;
	}

	private void ruleElementIdFacilitator(){		
		if(this.ruleElements.length > 0){
			RuleElementIdArray = new int[ruleElements.length];
			for(int i=0; i<ruleElements.length; i++){
				this.RuleElementIdArray[i] = this.ruleElements[i].getId();
			}
		} else {
			//ruleElements is Empty
		}
	}
	
	private void ruleElementNameFacilitator(){
		if(this.ruleElements.length > 0){ 
			RuleElementNameArray = new String[ruleElements.length];
			for(int i=0; i<ruleElements.length; i++){
				this.RuleElementNameArray[i] = this.ruleElements[i].getName();
			}
		} else {
			//ruleElements is Empty
		}
	}
}
