package lk.sliit.sep.qv2plugin;

/*
 * this class was added just in case we need to 
 * serialize ONLY a list of rules. DO NOT DELETE
 */

import java.util.List;
import java.util.ArrayList;

public class RuleElementListForSerialization {

	private List<RuleElement> list;
	
	public RuleElementListForSerialization(){
		list = new ArrayList<RuleElement>();
	}
	
	public void add(RuleElement ruleElement){
		list.add(ruleElement);
	}
}
