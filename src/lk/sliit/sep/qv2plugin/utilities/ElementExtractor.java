package lk.sliit.sep.qv2plugin.utilities;

import java.util.List;
import java.util.Vector;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ElementExtractor {

	public static Vector<List<WebElement>> extractElements(String url, WebDriver driver){    	       
    	Vector<List<WebElement>> elementsList = new Vector<List<WebElement>>();
    	
        driver.get(url);

        List <WebElement> buttons = driver.findElements(By.tagName("button"));
        List <WebElement> inputs = driver.findElements(By.tagName("input"));
        List <WebElement> hyperlinks = driver.findElements(By.tagName("a"));
        List <WebElement> images = driver.findElements(By.tagName("img"));
        List <WebElement> spans = driver.findElements(By.tagName("span"));
        List <WebElement> tables = driver.findElements(By.tagName("table"));
        List <WebElement> divs = driver.findElements(By.tagName("div"));

        elementsList.clear();
        elementsList.add(buttons);
        elementsList.add(inputs);
        elementsList.add(hyperlinks);
        elementsList.add(images);
        elementsList.add(spans);
        elementsList.add(tables);
        elementsList.add(divs);
        
        return elementsList;
	}
}
