package lk.sliit.sep.qv2plugin.utilities;

import java.util.Vector;

import lk.sliit.sep.qv2plugin.RuleElement;

public class NaturalLanguageProcessor {

	public RuleElement createRuleElement(String inputString){	
		RuleElement ruleElement = new RuleElement();
		
		String[] wordsArray = inputString.split(" ");
		
		if(stringArraySearchByString("kind", wordsArray)){
			int indexOfKind = getIndexOfWord("kind", wordsArray);
			int indexOfPossibleType = indexOfKind + 1;
			//development suspended.
		}
		
		return ruleElement;
	}
	
	private boolean stringArraySearchByString(String query, String[] wordsArray){
		boolean result = false;
		
		for(String word:wordsArray){
			result = word.equalsIgnoreCase(query) ? true : false;
		}
		
		return result;
	}
	
	private int getIndexOfWord(String query, String[] wordsArray){
		int result = -1;
		
		for(int i=0; i<wordsArray.length; i++){
			if(wordsArray[i].equalsIgnoreCase(query)){
				result = i;
			}
		}
		
		return result;
	}
}
