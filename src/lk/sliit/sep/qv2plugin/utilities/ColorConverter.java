package lk.sliit.sep.qv2plugin.utilities;

public class ColorConverter {
	public static String convertFromStringToHex(String colorName){
		String colorHex = "";
		
		if(colorName.equalsIgnoreCase("AliceBlue")){
			colorHex = "#F0F8FF";
		}
		else if(colorName.equalsIgnoreCase("AntiqueWhite")){
			colorHex = "#FAEBD7";
		}
		else if(colorName.equalsIgnoreCase("Aqua")){
			colorHex = "#00FFFF";
		}
		else if(colorName.equalsIgnoreCase("Aquamarine")){
			colorHex = "#7FFFD4";
		}
		else if(colorName.equalsIgnoreCase("Azure")){
			colorHex = "#F0FFFF";
		}
		else if(colorName.equalsIgnoreCase("Beige")){
			colorHex = "#F5F5DC";
		}
		else if(colorName.equalsIgnoreCase("Bisque")){
			colorHex = "#FFE4C4";
		}
		else if(colorName.equalsIgnoreCase("Black")){
			colorHex = "#000000";
		}
		else if(colorName.equalsIgnoreCase("BlanchedAlmond")){
			colorHex = "#FFEBCD";
		}
		else if(colorName.equalsIgnoreCase("Blue")){
			colorHex = "#0000FF";
		}
		else if(colorName.equalsIgnoreCase("BlueViolet")){
			colorHex = "#8A2BE2";
		}
		else if(colorName.equalsIgnoreCase("Brown")){
			colorHex = "#A52A2A";
		}
		else if(colorName.equalsIgnoreCase("BurlyWood")){
			colorHex = "#DEB887";
		}
		else if(colorName.equalsIgnoreCase("CadetBlue")){
			colorHex = "#5F9EA0";
		}
		else if(colorName.equalsIgnoreCase("Chartreuse")){
			colorHex = "#7FFF00";
		}
		else if(colorName.equalsIgnoreCase("Chocolate")){
			colorHex = "#D2691E";
		}
		else if(colorName.equalsIgnoreCase("Coral")){
			colorHex = "#FF7F50";
		}
		else if(colorName.equalsIgnoreCase("CornflowerBlue")){
			colorHex = "#6495ED";
		}
		else if(colorName.equalsIgnoreCase("Cornsilk")){
			colorHex = "#FFF8DC";
		}
		else if(colorName.equalsIgnoreCase("Crimson")){
			colorHex = "#DC143C";
		}
		else if(colorName.equalsIgnoreCase("Cyan")){
			colorHex = "#00FFFF";
		}
		else if(colorName.equalsIgnoreCase("DarkBlue")){
			colorHex = "#00008B";
		}
		else if(colorName.equalsIgnoreCase("DarkCyan")){
			colorHex = "#008B8B";
		}
		else if(colorName.equalsIgnoreCase("DarkGoldenRod")){
			colorHex = "#B8860B";
		}
		else if(colorName.equalsIgnoreCase("DarkGray")){
			colorHex = "#A9A9A9";
		}
		else if(colorName.equalsIgnoreCase("DarkGreen")){
			colorHex = "#006400";
		}
		else if(colorName.equalsIgnoreCase("DarkKhaki")){
			colorHex = "#BDB76B";
		}
		else if(colorName.equalsIgnoreCase("DarkMagenta")){
			colorHex = "#8B008B";
		}
		else if(colorName.equalsIgnoreCase("DarkOliveGreen")){
			colorHex = "#556B2F";
		}
		else if(colorName.equalsIgnoreCase("DarkOrange")){
			colorHex = "#FF8C00";
		}
		else if(colorName.equalsIgnoreCase("DarkOrchid")){
			colorHex = "#9932CC";
		}
		else if(colorName.equalsIgnoreCase("DarkRed")){
			colorHex = "#8B0000";
		}
		else if(colorName.equalsIgnoreCase("DarkSalmon")){
			colorHex = "#E9967A";
		}
		else if(colorName.equalsIgnoreCase("DarkSeaGreen")){
			colorHex = "#8FBC8F";
		}
		else if(colorName.equalsIgnoreCase("DarkSlateBlue")){
			colorHex = "#483D8B";
		}
		else if(colorName.equalsIgnoreCase("DarkSlateGray")){
			colorHex = "#2F4F4F";
		}
		else if(colorName.equalsIgnoreCase("DarkTurquoise")){
			colorHex = "#00CED1";
		}
		else if(colorName.equalsIgnoreCase("DarkViolet")){
			colorHex = "#9400D3";
		}
		else if(colorName.equalsIgnoreCase("DeepPink")){
			colorHex = "#FF1493";
		}
		else if(colorName.equalsIgnoreCase("DeepSkyBlue")){
			colorHex = "#00BFFF";
		}
		else if(colorName.equalsIgnoreCase("DimGray")){
			colorHex = "#696969";
		}
		else if(colorName.equalsIgnoreCase("DodgerBlue")){
			colorHex = "#1E90FF";
		}
		else if(colorName.equalsIgnoreCase("FireBrick")){
			colorHex = "#B22222";
		}
		else if(colorName.equalsIgnoreCase("FloralWhite")){
			colorHex = "#FFFAF0";
		}
		else if(colorName.equalsIgnoreCase("ForestGreen")){
			colorHex = "#228B22";
		}
		else if(colorName.equalsIgnoreCase("Fuchsia")){
			colorHex = "#FF00FF";
		}
		else if(colorName.equalsIgnoreCase("Gainsboro")){
			colorHex = "#DCDCDC";
		}
		else if(colorName.equalsIgnoreCase("GhostWhite")){
			colorHex = "#F8F8FF";
		}
		else if(colorName.equalsIgnoreCase("Gold")){
			colorHex = "#FFD700";
		}
		else if(colorName.equalsIgnoreCase("GoldenRod")){
			colorHex = "#DAA520";
		}
		else if(colorName.equalsIgnoreCase("Gray")){
			colorHex = "#808080";
		}
		else if(colorName.equalsIgnoreCase("Green")){
			colorHex = "#008000";
		}
		else if(colorName.equalsIgnoreCase("GreenYellow")){
			colorHex = "#ADFF2F";
		}
		else if(colorName.equalsIgnoreCase("HoneyDew")){
			colorHex = "#F0FFF0";
		}
		else if(colorName.equalsIgnoreCase("HotPink")){
			colorHex = "#FF69B4";
		}
		else if(colorName.equalsIgnoreCase("IndianRed ")){
			colorHex = "#CD5C5C";
		}
		else if(colorName.equalsIgnoreCase("Indigo ")){
			colorHex = "#4B0082";
		}
		else if(colorName.equalsIgnoreCase("Ivory")){
			colorHex = "#FFFFF0";
		}
		else if(colorName.equalsIgnoreCase("Khaki")){
			colorHex = "#F0E68C";
		}
		else if(colorName.equalsIgnoreCase("Lavender")){
			colorHex = "#E6E6FA";
		}
		else if(colorName.equalsIgnoreCase("LavenderBlush")){
			colorHex = "#FFF0F5";
		}
		else if(colorName.equalsIgnoreCase("LawnGreen")){
			colorHex = "#7CFC00";
		}
		else if(colorName.equalsIgnoreCase("LemonChiffon")){
			colorHex = "#FFFACD";
		}
		else if(colorName.equalsIgnoreCase("LightBlue")){
			colorHex = "#ADD8E6";
		}
		else if(colorName.equalsIgnoreCase("LightCoral")){
			colorHex = "#F08080";
		}
		else if(colorName.equalsIgnoreCase("LightCyan")){
			colorHex = "#E0FFFF";
		}
		else if(colorName.equalsIgnoreCase("LightGoldenRodYellow")){
			colorHex = "#FAFAD2";
		}
		else if(colorName.equalsIgnoreCase("LightGray")){
			colorHex = "#D3D3D3";
		}
		else if(colorName.equalsIgnoreCase("LightGreen")){
			colorHex = "#90EE90";
		}
		else if(colorName.equalsIgnoreCase("LightPink")){
			colorHex = "#FFB6C1";
		}
		else if(colorName.equalsIgnoreCase("LightSalmon")){
			colorHex = "#FFA07A";
		}
		else if(colorName.equalsIgnoreCase("LightSeaGreen")){
			colorHex = "#20B2AA";
		}
		else if(colorName.equalsIgnoreCase("LightSkyBlue")){
			colorHex = "#87CEFA";
		}
		else if(colorName.equalsIgnoreCase("LightSlateGray")){
			colorHex = "#778899";
		}
		else if(colorName.equalsIgnoreCase("LightSteelBlue")){
			colorHex = "#B0C4DE";
		}
		else if(colorName.equalsIgnoreCase("LightYellow")){
			colorHex = "#FFFFE0";
		}
		else if(colorName.equalsIgnoreCase("Lime")){
			colorHex = "#00FF00";
		}
		else if(colorName.equalsIgnoreCase("LimeGreen")){
			colorHex = "#32CD32";
		}
		else if(colorName.equalsIgnoreCase("Linen")){
			colorHex = "#FAF0E6";
		}
		else if(colorName.equalsIgnoreCase("Magenta")){
			colorHex = "#FF00FF";
		}
		else if(colorName.equalsIgnoreCase("Maroon")){
			colorHex = "#800000";
		}
		else if(colorName.equalsIgnoreCase("MediumAquaMarine")){
			colorHex = "#66CDAA";
		}
		else if(colorName.equalsIgnoreCase("MediumBlue")){
			colorHex = "#0000CD";
		}
		else if(colorName.equalsIgnoreCase("MediumOrchid")){
			colorHex = "#BA55D3";
		}
		else if(colorName.equalsIgnoreCase("MediumPurple")){
			colorHex = "#9370DB";
		}
		else if(colorName.equalsIgnoreCase("MediumSeaGreen")){
			colorHex = "#3CB371";
		}
		else if(colorName.equalsIgnoreCase("MediumSlateBlue")){
			colorHex = "#7B68EE";
		}
		else if(colorName.equalsIgnoreCase("MediumSpringGreen")){
			colorHex = "#00FA9A";
		}
		else if(colorName.equalsIgnoreCase("MediumTurquoise")){
			colorHex = "#48D1CC";
		}
		else if(colorName.equalsIgnoreCase("MediumVioletRed")){
			colorHex = "#C71585";
		}
		else if(colorName.equalsIgnoreCase("MidnightBlue")){
			colorHex = "#191970";
		}
		else if(colorName.equalsIgnoreCase("MintCream")){
			colorHex = "#F5FFFA";
		}
		else if(colorName.equalsIgnoreCase("MistyRose")){
			colorHex = "#FFE4E1";
		}
		else if(colorName.equalsIgnoreCase("Moccasin")){
			colorHex = "#FFE4B5";
		}
		else if(colorName.equalsIgnoreCase("NavajoWhite")){
			colorHex = "#FFDEAD";
		}
		else if(colorName.equalsIgnoreCase("Navy")){
			colorHex = "#000080";
		}
		else if(colorName.equalsIgnoreCase("OldLace")){
			colorHex = "#FDF5E6";
		}
		else if(colorName.equalsIgnoreCase("Olive")){
			colorHex = "#808000";
		}
		else if(colorName.equalsIgnoreCase("OliveDrab")){
			colorHex = "#6B8E23";
		}
		else if(colorName.equalsIgnoreCase("Orange")){
			colorHex = "#FFA500";
		}
		else if(colorName.equalsIgnoreCase("OrangeRed")){
			colorHex = "#FF4500";
		}
		else if(colorName.equalsIgnoreCase("Orchid")){
			colorHex = "#DA70D6";
		}
		else if(colorName.equalsIgnoreCase("PaleGoldenRod")){
			colorHex = "#EEE8AA";
		}
		else if(colorName.equalsIgnoreCase("PaleGreen")){
			colorHex = "#98FB98";
		}
		else if(colorName.equalsIgnoreCase("PaleTurquoise")){
			colorHex = "#AFEEEE";
		}
		else if(colorName.equalsIgnoreCase("PaleVioletRed")){
			colorHex = "#DB7093";
		}
		else if(colorName.equalsIgnoreCase("PapayaWhip")){
			colorHex = "#FFEFD5";
		}
		else if(colorName.equalsIgnoreCase("PeachPuff")){
			colorHex = "#FFDAB9";
		}
		else if(colorName.equalsIgnoreCase("Peru")){
			colorHex = "#CD853F";
		}
		else if(colorName.equalsIgnoreCase("Pink")){
			colorHex = "#FFC0CB";
		}
		else if(colorName.equalsIgnoreCase("Plum")){
			colorHex = "#DDA0DD";
		}
		else if(colorName.equalsIgnoreCase("PowderBlue")){
			colorHex = "#B0E0E6";
		}
		else if(colorName.equalsIgnoreCase("Purple")){
			colorHex = "#800080";
		}
		else if(colorName.equalsIgnoreCase("Red")){
			colorHex = "#FF0000";
		}
		else if(colorName.equalsIgnoreCase("RosyBrown")){
			colorHex = "#BC8F8F";
		}
		else if(colorName.equalsIgnoreCase("RoyalBlue")){
			colorHex = "#4169E1";
		}
		else if(colorName.equalsIgnoreCase("SaddleBrown")){
			colorHex = "#8B4513";
		}
		else if(colorName.equalsIgnoreCase("Salmon")){
			colorHex = "#FA8072";
		}
		else if(colorName.equalsIgnoreCase("SandyBrown")){
			colorHex = "#F4A460";
		}
		else if(colorName.equalsIgnoreCase("SeaGreen")){
			colorHex = "#2E8B57";
		}
		else if(colorName.equalsIgnoreCase("SeaShell")){
			colorHex = "#FFF5EE";
		}
		else if(colorName.equalsIgnoreCase("Sienna")){
			colorHex = "#A0522D";
		}
		else if(colorName.equalsIgnoreCase("Silver")){
			colorHex = "#C0C0C0";
		}
		else if(colorName.equalsIgnoreCase("SkyBlue")){
			colorHex = "#87CEEB";
		}
		else if(colorName.equalsIgnoreCase("SlateBlue")){
			colorHex = "#6A5ACD";
		}
		else if(colorName.equalsIgnoreCase("SlateGray")){
			colorHex = "#708090";
		}
		else if(colorName.equalsIgnoreCase("Snow")){
			colorHex = "#FFFAFA";
		}
		else if(colorName.equalsIgnoreCase("SpringGreen")){
			colorHex = "#00FF7F";
		}
		else if(colorName.equalsIgnoreCase("SteelBlue")){
			colorHex = "#4682B4";
		}
		else if(colorName.equalsIgnoreCase("Tan")){
			colorHex = "#D2B48C";
		}
		else if(colorName.equalsIgnoreCase("Teal")){
			colorHex = "#008080";
		}
		else if(colorName.equalsIgnoreCase("Thistle")){
			colorHex = "#D8BFD8";
		}
		else if(colorName.equalsIgnoreCase("Tomato")){
			colorHex = "#FF6347";
		}
		else if(colorName.equalsIgnoreCase("Turquoise")){
			colorHex = "#40E0D0";
		}
		else if(colorName.equalsIgnoreCase("Violet")){
			colorHex = "#EE82EE";
		}
		else if(colorName.equalsIgnoreCase("Wheat")){
			colorHex = "#F5DEB3";
		}
		else if(colorName.equalsIgnoreCase("White")){
			colorHex = "#FFFFFF";
		}
		else if(colorName.equalsIgnoreCase("WhiteSmoke")){
			colorHex = "#F5F5F5";
		}
		else if(colorName.equalsIgnoreCase("Yellow")){
			colorHex = "#FFFF00";
		}
		else if(colorName.equalsIgnoreCase("YellowGreen")){
			colorHex = "#9ACD32";
		}
		else{
			System.out.println("Unidentified Color String");
		}
		
		return colorHex;
	}
}
