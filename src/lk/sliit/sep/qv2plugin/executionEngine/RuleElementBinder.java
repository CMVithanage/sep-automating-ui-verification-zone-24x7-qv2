package lk.sliit.sep.qv2plugin.executionEngine;

import java.util.HashMap;
import java.util.Vector;

import lk.sliit.sep.qv2plugin.RuleElement;

public class RuleElementBinder {
	private HashMap<RuleElement, Vector<String>> elementMap;
	
	public RuleElementBinder(HashMap<RuleElement, Vector<String>> elementMap){
		this.elementMap = elementMap;
	}
	
	public RuleElementBinder(){
		this.elementMap = new HashMap<RuleElement, Vector<String>>();
	}
	
	public void addNewRuleElement(RuleElement ruleElement){
		elementMap.put(ruleElement, new Vector<String>());
	}
	
	public void addNewXpathForRuleElement(RuleElement ruleElement, String xPath){
		if(elementMap.containsKey(ruleElement)){
			Vector<String> xPathStringVector = elementMap.get(ruleElement);
			xPathStringVector.add(xPath);
			elementMap.put(ruleElement, xPathStringVector);
		} else {
			Vector<String> xPathStringVector = new Vector<String>();
			xPathStringVector.add(xPath);
			elementMap.put(ruleElement, xPathStringVector);
		}
	}
	
	public boolean clearElementMap(){
		boolean success = false;
		try{
			elementMap.clear();
			success = true;
		} catch (Exception e) {
			System.out.println("Failed with Exception: " + e);
		}
		return success;
	}

	public HashMap<RuleElement, Vector<String>> getElementMap() {
		return elementMap;
	}

	public void setElementMap(HashMap<RuleElement, Vector<String>> elementMap) {
		this.elementMap = elementMap;
	}
}
