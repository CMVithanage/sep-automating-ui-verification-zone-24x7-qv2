package lk.sliit.sep.qv2plugin.executionEngine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import lk.sliit.sep.qv2plugin.RuleElement;
import lk.sliit.sep.qv2plugin.verification.BackgroundVerification;
import lk.sliit.sep.qv2plugin.verification.ExecuteVerifications;

public class MultiElementExecutor {
	
	public static HashMap<String, String> excecute(HashMap<RuleElement, Vector<String>> elementMap, WebDriver driver, String webpage){
		HashMap<String, String> resultMap = new HashMap<String, String>();
		
		//WebDriver wb;
		//System.setProperty("webdriver.chrome.driver", "E:\\chromedriver_win32\\chromedriver.exe");		//CHROMEDRIVER PATH EDIT
		//wb = new ChromeDriver();
		//wb.get(webpage);
		
		if (!elementMap.isEmpty()) {			
			Iterator<Entry<RuleElement, Vector<String>>> iterator = elementMap.entrySet()
					.iterator();
			while (iterator.hasNext()) {
				Map.Entry<RuleElement, Vector<String>> elementPair = (Map.Entry<RuleElement, Vector<String>>) iterator
						.next();
				RuleElement ruleElement = elementPair.getKey();
				Vector<String> xPathVector = elementPair.getValue();
				
				Iterator<String> vectorIterator = xPathVector.iterator();
				
				while(vectorIterator.hasNext()){
					System.out.println("shmurr");
					String xPath = vectorIterator.next();
					
					ExecuteVerification ev = new ExecuteVerification(ruleElement, xPath, driver);
					HashMap<String, String> tmp = ev.execute();
					
					System.out.println("before for: "+xPath);
					for (Map.Entry<String, String> entry : tmp.entrySet())
					{
					    String key = entry.getKey()+":"+xPath;
					    String value = entry.getValue();
					    resultMap.put(key, value);
					    System.out.println("key: "+key);
					    System.out.println("value: "+value);
					}

				}
			}
		} else {
			//ElementMap is empty!
		}
		System.out.println("size: "+resultMap.size());
		return resultMap;
	}
}
