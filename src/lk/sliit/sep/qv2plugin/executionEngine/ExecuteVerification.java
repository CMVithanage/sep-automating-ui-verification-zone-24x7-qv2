package lk.sliit.sep.qv2plugin.executionEngine;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import lk.sliit.sep.qv2plugin.verification.*;
import lk.sliit.sep.qv2plugin.exceptions.*;
import lk.sliit.sep.qv2plugin.*;

public class ExecuteVerification {

	private RuleElement ruleElement;
	private String xpath;
	private String webpage;
	private WebDriver wb;

	public ExecuteVerification(RuleElement ruleElement, String xpath,
			WebDriver wb) {

		this.ruleElement = ruleElement;
		this.xpath = xpath;
		this.wb = wb;

	}

	public HashMap<String, String> execute() {

		// WebDriver wb;
		// System.setProperty("webdriver.chrome.driver",
		// "E:\\chromedriver_win32\\chromedriver.exe"); //CHROMEDRIVER PATH EDIT
		// wb = new ChromeDriver();
		// wb.get(webpage);

		BackgroundVerification bck = new BackgroundVerification(xpath, wb,
				ruleElement);
		BorderVerification brdr = new BorderVerification(xpath, wb, ruleElement);
		ClassificationAndPositionVerification classAndPos = new ClassificationAndPositionVerification(
				xpath, wb, ruleElement);
		DimensionVerification dimeVerification = new DimensionVerification(
				xpath, wb, ruleElement);
		FontVerification fv = new FontVerification(xpath, wb, ruleElement);
		ListVerification lv = new ListVerification(xpath, wb, ruleElement);
		MarginVerification mv = new MarginVerification(xpath, wb, ruleElement);
		OutlineVerification ov = new OutlineVerification(xpath, wb, ruleElement);
		PaddingVerification pv = new PaddingVerification(xpath, wb, ruleElement);
		TableVerification tv = new TableVerification(xpath, wb, ruleElement);
		TextVerification txtV = new TextVerification(xpath, wb, ruleElement);

		HashMap<String, String> resultMap = new HashMap<String, String>();

		if (ruleElement.getBackgroundColor() != null
				&& !ruleElement.getBackgroundColor().isEmpty()) {

			try {
				String res = bck.verifyBackgroundColor();
				resultMap.put("Verify Background Color", res);
			} catch (Exception exception) {
				resultMap.put("Verify Background Color", exception.getMessage());
			}
		}

		if (ruleElement.getBackgroundImage() != null
				&& !ruleElement.getBackgroundImage().isEmpty()) {

			try {
				String res = bck.verifyBackgroundImage();
				resultMap.put("Verify Background Image", res);
			} catch (Exception e) {
				resultMap.put("Verify Background Image", e.getMessage());
			}
		}

		if (ruleElement.getBackgroundAttachment() != null
				&& !ruleElement.getBackgroundAttachment().isEmpty()) {
			try {
				String res = bck.verifyBackgrounAttachment();
				resultMap.put("Verify Background Attachment", res);
			} catch (Exception e) {
				resultMap.put("Verify Background Attachment", e.getMessage());
			}
		}

		if (ruleElement.getBackgroundPosition() != null
				&& !ruleElement.getBackgroundPosition().isEmpty()) {
			try {
				String res = bck.verifyBackgroundPosition();
				resultMap.put("Verify Background Position", res);
			} catch (Exception e) {
				resultMap.put("Verify Background Position", e.getMessage());
			}
		}

		if (ruleElement.getBackgroundRepeat() != null
				&& !ruleElement.getBackgroundRepeat().isEmpty()) {
			try {
				String res = bck.verifyBackgroundRepeat();
				resultMap.put("Verify Background Repeat", res);
			} catch (Exception e) {
				resultMap.put("Verify Background Repeat", e.getMessage());
			}
		}

		if (ruleElement.getBorderColor() != null
				&& !ruleElement.getBorderColor().isEmpty()) {
			try {
				String res = brdr.verifyWholeBorderColor();
				resultMap.put("Verify Whole Border", res);
			} catch (Exception e) {
				resultMap.put("Verify Whole Border", e.getMessage());
			}
		}

		if (ruleElement.getBorderTopColor() != null
				&& !ruleElement.getBorderTopColor().isEmpty()) {
			try {
				String res = brdr.verifyTopBorderColor();
				resultMap.put("Verify Top Border Color", res);
			} catch (Exception e) {
				resultMap.put("Verify Top Border Color", e.getMessage());
			}
		}

		if (ruleElement.getBorderLeftColor() != null
				&& !ruleElement.getBorderLeftColor().isEmpty()) {
			try {
				String res = brdr.verifyLeftBorderColor();
				resultMap.put("Verify Left Border Color", res);
			} catch (Exception e) {
				resultMap.put("Verify Left Border Color", e.getMessage());
			}
		}

		if (ruleElement.getBorderRightColor() != null
				&& !ruleElement.getBorderRightColor().isEmpty()) {
			try {
				String res = brdr.verifyRightBorderColor();
				resultMap.put("Verify Right Border Color", res);
			} catch (Exception e) {
				resultMap.put("Verify Right Border Color", e.getMessage());
			}
		}

		if (ruleElement.getBorderBottomColor() != null
				&& !ruleElement.getBorderBottomColor().isEmpty()) {
			try {
				String res = brdr.verifyBottomBorderColor();
				resultMap.put("Verify Bottom Border Color", res);
			} catch (Exception e) {
				resultMap.put("Verify Bottom Border Color", e.getMessage());
			}
		}

		if (ruleElement.getBorderWidth() != null
				&& !ruleElement.getBorderBottomWidth().isEmpty()) {
			try {
				String res = brdr.verifyBottomBorderWidth();
				resultMap.put("Verify Bottom Border Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Bottom Border Width", e.getMessage());
			}
		}

		if (ruleElement.getBorderTopWidth() != null
				&& !ruleElement.getBorderTopWidth().isEmpty()) {
			try {
				String res = brdr.verifyTopBorderWidth();
				resultMap.put("Verify Top Border Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Top Border Width", e.getMessage());
			}
		}

		if (ruleElement.getBorderRightWidth() != null
				&& !ruleElement.getBorderRightWidth().isEmpty()) {
			try {
				String res = brdr.verifyRightBorderWidth();
				resultMap.put("Verify Right Border Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Right Border Width", e.getMessage());
			}
		}

		if (ruleElement.getBorderLeftWidth() != null
				&& !ruleElement.getBorderLeftWidth().isEmpty()) {
			try {
				String res = brdr.verifyLeftBorderWidth();
				resultMap.put("Verify Left Border Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Left Border Width", e.getMessage());
			}
		}

		if (ruleElement.getBorderBottomWidth() != null
				&& !ruleElement.getBorderBottomWidth().isEmpty()) {
			try {
				String res = brdr.verifyBottomBorderWidth();
				resultMap.put("Verify Bottom Border Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Bottom Border Width", e.getMessage());
			}
		}

		if (ruleElement.getBorderStyle() != null
				&& !ruleElement.getBorderStyle().isEmpty()) {
			try {
				String res = brdr.verifyWholeBorderStyle();
				resultMap.put("Verify Whole Border Style", res);
			} catch (Exception e) {
				resultMap.put("Verify Whole Border Style", e.getMessage());
			}
		}

		if (ruleElement.getBorderTopStyle() != null
				&& !ruleElement.getBorderTopStyle().isEmpty()) {
			try {
				String res = brdr.verifyTopBorderStyle();
				resultMap.put("Verify Top Border Style", res);
			} catch (Exception e) {
				resultMap.put("Verify Top Border Style", e.getMessage());
			}
		}

		if (ruleElement.getBorderRightStyle() != null
				&& !ruleElement.getBorderRightStyle().isEmpty()) {
			try {
				String res = brdr.verifyRightBorderStyle();
				resultMap.put("Verify Right Border Style", res);
			} catch (Exception e) {
				resultMap.put("Verify Right Border Style", e.getMessage());
			}
		}

		if (ruleElement.getBorderLeftStyle() != null
				&& !ruleElement.getBorderLeftStyle().isEmpty()) {
			try {
				String res = brdr.verifyLeftBorderStyle();
				resultMap.put("Verify Left Border Style", res);
			} catch (Exception e) {
				resultMap.put("Verify Left Border Style", e.getMessage());
			}
		}

		if (ruleElement.getBorderBottomStyle() != null
				&& !ruleElement.getBorderBottomStyle().isEmpty()) {
			try {
				String res = brdr.verifyBottomBorderStyle();
				resultMap.put("Verify Bottom Border Style", res);
			} catch (Exception e) {
				resultMap.put("Verify Bottom Border Style", e.getMessage());
			}
		}

		if (ruleElement.getClear() != null && !ruleElement.getClear().isEmpty()) {
			try {
				String res = classAndPos.verifyClarity();
				resultMap.put("Verify Clarity", res);
			} catch (Exception e) {
				resultMap.put("Verify Clarity", e.getMessage());
			}
		}

		if (ruleElement.getCursor() != null
				&& !ruleElement.getCursor().isEmpty()) {
			try {
				String res = classAndPos.verifyCursor();
				resultMap.put("Verify Cursor", res);
			} catch (Exception e) {
				resultMap.put("Verify Cursor", e.getMessage());
			}

		}

		if (ruleElement.getDisplay() != null
				&& !ruleElement.getDisplay().isEmpty()) {
			try {
				String res = classAndPos.verifyDisplay();
				resultMap.put("Verify Display", res);
			} catch (Exception e) {
				resultMap.put("Verify Display", e.getMessage());
			}
		}

		if (ruleElement.getVisibility() != null
				&& !ruleElement.getVisibility().isEmpty()) {
			try {
				String res = classAndPos.verifyVisibility();
				resultMap.put("Verify Visibility", res);
			} catch (Exception e) {
				resultMap.put("Verify Visibility", e.getMessage());
			}
		}

		if (ruleElement.getTop() != null && !ruleElement.getTop().isEmpty()) {
			try {
				String res = classAndPos.verifyTop();
				resultMap.put("Verify Top", res);
			} catch (Exception e) {
				resultMap.put("Verify Top", e.getMessage());
			}
		}

		if (ruleElement.getRight() != null && !ruleElement.getRight().isEmpty()) {
			try {
				String res = classAndPos.verifyRight();
				resultMap.put("Verify Right", res);
			} catch (Exception e) {
				resultMap.put("Verify Right", e.getMessage());
			}
		}

		if (ruleElement.getLeft() != null && !ruleElement.getLeft().isEmpty()) {
			try {
				String res = classAndPos.verifyLeft();
				resultMap.put("Verify Left", res);
			} catch (Exception e) {
				resultMap.put("Verify Left", e.getMessage());
			}
		}

		if (ruleElement.getBottom() != null
				&& !ruleElement.getBottom().isEmpty()) {
			try {
				String res = classAndPos.verifyBottom();
				resultMap.put("Verify Bottom", res);
			} catch (Exception e) {
				resultMap.put("Verify Bottom", e.getMessage());
			}
		}

		if (ruleElement.getPosition() != null
				&& !ruleElement.getPosition().isEmpty()) {
			try {
				String res = classAndPos.verifyPosition();
				resultMap.put("Verify Position", res);
			} catch (Exception e) {
				resultMap.put("Verify Position", e.getMessage());
			}
		}

		if (ruleElement.getClip() != null && !ruleElement.getClip().isEmpty()) {
			try {
				String res = classAndPos.verifyClip();
				resultMap.put("Verify Clip", res);
			} catch (Exception e) {
				resultMap.put("Verify Clip", e.getMessage());
			}
		}

		if (ruleElement.getOverflow() != null
				&& !ruleElement.getOverflow().isEmpty()) {
			try {
				String res = classAndPos.verifyOverflow();
				resultMap.put("Verify Overflow", res);
			} catch (Exception e) {
				resultMap.put("Verify Overflow", e.getMessage());
			}
		}

		if (ruleElement.getVerticalAlign() != null
				&& !ruleElement.getVerticalAlign().isEmpty()) {
			try {
				String res = classAndPos.verifyVerticalAlignment();
				resultMap.put("Verify Vertical Alignment", res);
			} catch (Exception e) {
				resultMap.put("Verify Vertical Alignment", e.getMessage());
			}

		}

		if (ruleElement.getzIndex() != null
				&& !ruleElement.getzIndex().isEmpty()) {
			try {
				String res = classAndPos.verifyZindex();
				resultMap.put("Verify Zindex", res);
			} catch (Exception e) {
				resultMap.put("Verify Zindex", e.getMessage());
			}
		}

		if (ruleElement.getHeight() != null
				&& !ruleElement.getHeight().isEmpty()) {
			try {
				String res = dimeVerification.verifyHeight();
				resultMap.put("Verify Height", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Height", e.getMessage());
			}
		}

		if (ruleElement.getWidth() != null && !ruleElement.getWidth().isEmpty()) {
			try {
				String res = dimeVerification.verifyWidth();
				resultMap.put("Verify Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Width", e.getMessage());
			}
		}

		if (ruleElement.getMinHeight() != null
				&& !ruleElement.getMinHeight().isEmpty()) {
			try {
				String res = dimeVerification.verifyMinHeight();
				resultMap.put("Verify Minimum Height", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Minimum Height", e.getMessage());
			}
		}

		if (ruleElement.getMaxHeight() != null
				&& !ruleElement.getMaxHeight().isEmpty()) {
			try {
				String res = dimeVerification.verifyMaxHeight();
				resultMap.put("Verify Maximum Height", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Maximum Height", e.getMessage());
			}
		}

		if (ruleElement.getMinWidth() != null
				&& !ruleElement.getMinWidth().isEmpty()) {
			try {
				String res = dimeVerification.verifyMinWidth();
				resultMap.put("Verify Minimum Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Minimum Width", e.getMessage());
			}
		}

		if (ruleElement.getMaxWidth() != null
				&& !ruleElement.getMaxWidth().isEmpty()) {
			try {
				String res = dimeVerification.verifyMaxWidth();
				resultMap.put("Verify Maximum Width", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Maximum Width", e.getMessage());
			}
		}

		if (ruleElement.getFontFamily() != null
				&& !ruleElement.getFontFamily().isEmpty()) {
			try {
				String res = fv.verifyFont();
				resultMap.put("Verify Font", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Font", e.getMessage());
			}
		}

		if (ruleElement.getFontStyle() != null
				&& !ruleElement.getFontStyle().isEmpty()) {
			try {
				String res = fv.verifyFontStyle();
				resultMap.put("Verify Font Style", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Font Style", e.getMessage());
			}
		}

		if (ruleElement.getFontWeight() != null
				&& !ruleElement.getFontWeight().isEmpty()) {
			try {
				String res = fv.verifyFontWeight();
				resultMap.put("Vserify Font Weight", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Vserify Font Weight", e.getMessage());
			}
		}

		if (ruleElement.getFontSize() != null
				&& !ruleElement.getFontSize().isEmpty()) {
			try {
				String res = fv.verifyFontSize();
				resultMap.put("Verify Font Size", res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Font Size", e.getMessage());
			}
		}

		if (ruleElement.getFontVariant() != null
				&& !ruleElement.getFontVariant().isEmpty()) {
			try {
				String res = fv.verifyFontVariant();
				resultMap.put("Verify Font Variant", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Font Variant", e.getMessage());
			}
		}

		// if (ruleElement.getColor() != null &&
		// !ruleElement.getColor().isEmpty()){
		// fv.verifyFontColor();
		// }

		if (ruleElement.getListStyleType() != null
				&& !ruleElement.getListStyleType().isEmpty()) {
			try {
				String res = lv.verifyListStyleType();
				resultMap.put("Verify List Style Type", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify List Style Type", e.getMessage());
			}
		}

		if (ruleElement.getListStylePosition() != null
				&& !ruleElement.getListStylePosition().isEmpty()) {
			try {
				String res = lv.listStylePosition();
				resultMap.put("List Style Position", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("List Style Position", e.getMessage());
			}
		}

		if (ruleElement.getMarkerOffset() != null
				&& !ruleElement.getMarkerOffset().isEmpty()) {
			try {
				String res = lv.verifyMarkerOffset();
				resultMap.put("Verify Marker Offset", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Marker Offset", e.getMessage());
			}
		}

		if (ruleElement.getMarginTop() != null
				&& !ruleElement.getMarginTop().isEmpty()) {
			try {
				String res = mv.verifyMarginTop();
				resultMap.put("Verify Margin Top", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Margin Top", e.getMessage());
			}
		}

		if (ruleElement.getMarginRight() != null
				&& !ruleElement.getMarginRight().isEmpty()) {
			String res;
			try {
				res = mv.verifyMarginRight();
				resultMap.put("Verify Margin Right", res);
			} catch (UndefinedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Margin Right", e.getMessage());
			}

		}

		if (ruleElement.getMarginBottom() != null
				&& !ruleElement.getMarginBottom().isEmpty()) {
			try {
				String res = mv.verifyMarginBottom();
				resultMap.put("Verify Margin Bottom", res);
			} catch (UndefinedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Margin Bottom", e.getMessage());
			}
		}

		if (ruleElement.getMarginLeft() != null
				&& !ruleElement.getMarginLeft().isEmpty()) {
			try {
				String res = mv.verifyMarginLeft();
				resultMap.put("Verify Margin Left", res);
			} catch (UndefinedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Margin Left", e.getMessage());
			}
		}

		if (ruleElement.getOutlineColor() != null
				&& !ruleElement.getOutlineColor().isEmpty()) {
			try {
				String res = ov.verifyOutlineColor();
				resultMap.put("Verify Outline Color", res);
			} catch (UndefinedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultMap.put("Verify Outline Color", e.getMessage());
			}
		}

		if (ruleElement.getOutlineStyle() != null
				&& !ruleElement.getOutlineStyle().isEmpty()) {
			try {
				String res = ov.verifyOutlineStyle();
				resultMap.put("Verify Outline Style", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Outline Style", e.getMessage());
			}
		}

		if (ruleElement.getOutlineWidth() != null
				&& !ruleElement.getOutlineWidth().isEmpty()) {
			try {
				String res = ov.verifyOutlineWidth();
				resultMap.put("Verify Outline Width", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Outline Width", e.getMessage());
			}
		}

		if (ruleElement.getPaddingTop() != null
				&& !ruleElement.getPaddingTop().isEmpty()
				&& ruleElement.getPaddingRight() != null
				&& !ruleElement.getPaddingRight().isEmpty()
				&& ruleElement.getPaddingBottom() != null
				&& !ruleElement.getPaddingBottom().isEmpty()
				&& ruleElement.getPaddingLeft() != null
				&& !ruleElement.getPaddingLeft().isEmpty()) {

			try {
				String res = pv.verifyPadding();
				resultMap.put("Verify Padding", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Padding", e.getMessage());
			}
		}

		if (ruleElement.getPaddingBottom() != null
				&& !ruleElement.getPaddingBottom().isEmpty()) {
			try {
				String res = pv.verifyPaddingBottom();
				resultMap.put("Verify Padding Bottom", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Padding Bottom", e.getMessage());
			}
		}

		if (ruleElement.getPaddingLeft() != null
				&& !ruleElement.getPaddingLeft().isEmpty()) {
			try {
				String res = pv.verifyPaddingLeft();
				resultMap.put("Verify Padding Left", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Padding Left", e.getMessage());
			}
		}

		if (ruleElement.getPaddingRight() != null
				&& !ruleElement.getPaddingRight().isEmpty()) {
			try {
				String res = pv.verifyPaddingRight();
				resultMap.put("Verify Padding Right", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Padding Right", e.getMessage());
			}
		}

		if (ruleElement.getPaddingTop() != null
				&& !ruleElement.getPaddingTop().isEmpty()) {
			try {
				String res = pv.verifyPaddingTop();
				resultMap.put("Verify Padding Top", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Padding Top", e.getMessage());
			}
		}

		if (ruleElement.getBorderCollapse() != null
				&& !ruleElement.getBorderCollapse().isEmpty()) {
			try {
				String res = tv.verifyTableBorderCollapse();
				resultMap.put("Verify Table Border Collapse", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Table Border Collapse", e.getMessage());
			}
		}

		if (ruleElement.getBorderSpacing() != null
				&& !ruleElement.getBorderSpacing().isEmpty()) {
			try {
				String res = tv.verifyTableBorderSpacingOneVal();
				resultMap.put("Verify Table Border Spacing", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Table Border Spacing", e.getMessage());
			}
		}

		if (ruleElement.getCaptionSide() != null
				&& !ruleElement.getCaptionSide().isEmpty()) {
			try {
				String res = tv.verifyTableCaption();
				resultMap.put("Verify Table Caption", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Table Caption", e.getMessage());
			}
		}

		if (ruleElement.getEmptyCells() != null
				&& !ruleElement.getEmptyCells().isEmpty()) {
			try {
				String res = tv.verifyTableEmptyCells();
				resultMap.put("Verify Table Empty Cells", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Table Empty Cells", e.getMessage());
			}
		}

		if (ruleElement.getTableLayout() != null
				&& !ruleElement.getTableLayout().isEmpty()) {
			try {
				String res = tv.verifyTableLayout();
				resultMap.put("Verify Table Layout", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Table Layout", e.getMessage());
			}
		}

		if (ruleElement.getColor() != null && !ruleElement.getColor().isEmpty()) {
			try {
				String res = txtV.verifyColor();
				resultMap.put("Verify Color", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Color", e.getMessage());
			}
		}

		if (ruleElement.getDirection() != null
				&& !ruleElement.getDirection().isEmpty()) {
			try {
				String res = txtV.verifyTextDirection();
				resultMap.put("Verify Text Direction", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text Direction", e.getMessage());
			}
		}

		if (ruleElement.getLineHeight() != null
				&& !ruleElement.getLineHeight().isEmpty()) {
			try {
				String res = txtV.verifyLineHeight();
				resultMap.put("Verify Line Height", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Line Height", e.getMessage());
			}
		}

		if (ruleElement.getLetterSpacing() != null
				&& !ruleElement.getLetterSpacing().isEmpty()) {
			try {
				String res = txtV.verifyLetterSpacing();
				resultMap.put("Verify Letter Spacing", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Letter Spacing", e.getMessage());
			}
		}

		if (ruleElement.getTextAlign() != null
				&& !ruleElement.getTextAlign().isEmpty()) {
			try {
				String res = txtV.verifyTextAlign();
				resultMap.put("Verify Text Align", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text Align", e.getMessage());
			}
		}

		if (ruleElement.getTextDecoration() != null
				&& !ruleElement.getTextDecoration().isEmpty()) {
			try {
				String res = txtV.verifyTextDecoration();
				resultMap.put("Verify Text Decoration", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text Decoration", e.getMessage());
			}
		}

		if (ruleElement.getTextIndent() != null
				&& !ruleElement.getTextIndent().isEmpty()) {
			try {
				String res = txtV.verifyTextIndent();
				resultMap.put("Verify Text Indent", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text Indent", e.getMessage());
			}
		}

		if (ruleElement.getWordSpacing() != null
				&& !ruleElement.getWordSpacing().isEmpty()) {
			try {
				String res = txtV.verifyTextSpacing();
				resultMap.put("Verify Text Spacing", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text Spacing", e.getMessage());
			}
		}

		if (ruleElement.getWhiteSpace() != null
				&& !ruleElement.getWhiteSpace().isEmpty()) {
			try {
				String res = txtV.verifyTextWhiteSpace();
				resultMap.put("Verify Text White Space", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text White Space", e.getMessage());
			}
		}

		if (ruleElement.getTextTransform() != null
				&& !ruleElement.getTextTransform().isEmpty()) {
			try {
				String res = txtV.verifyTextTransform();
				resultMap.put("Verify Text Transform", res);
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("Verify Text Transform", e.getMessage());
			}
		}

		return resultMap;

	}

}
