package lk.sliit.sep.qv2plugin.executionEngine;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HighlightAndScreenShot {
	
	public byte[] highlightAndTakeScreenShot(String xpath, WebDriver driver){
		WebElement element = driver.findElement(By.xpath(xpath));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].style.border='3px solid red'", element);
		byte[] imgArray = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		return imgArray;
		
	}
	
	public void highlight(String xpath, WebDriver driver){
		WebElement element = driver.findElement(By.xpath(xpath));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].style.border='3px solid red'", element);
	}
	
	public void removeHighlight(String xpath, WebDriver driver){
		WebElement element = driver.findElement(By.xpath(xpath));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].style.border='none'", element);
	}

}
