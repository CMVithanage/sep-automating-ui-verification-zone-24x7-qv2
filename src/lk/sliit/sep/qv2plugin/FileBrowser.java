package lk.sliit.sep.qv2plugin;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class FileBrowser {

	public File selectFile() {
		JFrame frame = new JFrame();

		String filename = File.separator + "tmp";
		JFileChooser fc = new JFileChooser(new File(filename));

		// Show open dialog; this method does not return until the dialog is
		// closed
		fc.showOpenDialog(frame);
		File selFile = fc.getSelectedFile();
		return selFile;
	}

	public File saveFile() {
		JFrame frame = new JFrame();

		String filename = File.separator + "tmp";
		JFileChooser fc = new JFileChooser(new File(filename));

		// Show save dialog; this method does not return until the dialog is
		// closed
		fc.showSaveDialog(frame);
		File selFile = fc.getSelectedFile();
		return selFile;
	}
}
